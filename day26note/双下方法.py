class A(object):
    def __init__(self):
        print('in __init__')

    def __new__(cls, *args, **kwargs):
        object1 = object.__new__(cls)
        print('in __new__',cls)
        return object1

obj = A()

