class A:
    static_field = '静态属性'
    def __init__(self,name ,age):
        self.name = name
        self .age = age

    def func(self):
        print('in Afunc')
obj = A('MC骚Q',18)
print(hasattr(obj,'name'))#检查obj里有没有name属性
print(getattr(obj,'name',None))#返回obj里的name属性
setattr(obj,'hobby','玩')#给obj加一个属性hobby,值为玩.或替换,跟obj.hobby ='玩'一样的效果
print(getattr(obj,'hobby'))
print(obj.hobby)
delattr(obj,'name')
print(hasattr(obj,'name'))
if hasattr(obj,'static_field'):
    print(getattr(obj,'static_field'))
if hasattr(obj,'func'):
    print(getattr(obj,'func'))
    getattr(obj,'func')()
# if hasattr
