# 补充内容
# 循环jq对象的数组:
# 普通的for循环不能作用在jq的数组里.
# for (let i =0;i<li_list.length;i++){
#console.log(i)}   length属性

# 内容回顾
    # jquery引入
    # 官网找到download,找到未压缩版,复制到本地文件.
    # <script src = '路径'></script>
# jquery是一个对js进行了高度封装的库,js主要操作dom对象,jq主要对dom对象的功能进行了封装
# 查找标签
    # 选择器
        # 基础选择器  id #,类名.,标签名,通用*  div.c1交集, div,p并集
        # 层级选择器  空格(后代) >子代, +毗邻紧挨着 ,~同级的弟弟
        # 属性选择器  [属性名] [属性名= '属性值'] [属性名^="开头"][属性名$='结尾']
            # [属性名!="不等于"] [属性名*="包含"]
    # 筛选器
        # 基础筛选器 first last eq[index] even(偶数) odd(奇数) gt大于(index)
            # lt小于(index) not不等于('选择器') has('选择器')如果有后代满足条件,就把当代选出来
            #
        # 表单筛选器:
            # text password radio  checkbox  file  reset submit button
            # checked selected disabled  enabled
    # 筛选器方法
        # 找兄弟 .siblings
        # 找弟弟 next,nextAll,nextUntil('选择器')
        # 找哥哥 prev(),prevAll(),prevUntil('选择器')
        # 找祖辈 parents()找爹, parents(找祖宗), parentsUntil('选择器')
        # 找儿子 children()

        # 筛选:first() last() eq(index)
            # not('选择器'也可以放jq对象或dom对象)
            # filter('结果再筛选')
            # find('后代') 当代的后代.
            # has('拥有后代的当代')  has和find 要区分好.
# 操作标签
    # 操作标签的属性和文本
# jQuery的本质就是一个类,简写是$符.
# 如何创建一个jquery对象:
    # $('选择器选择到标签')
    # $(dom对象)
# 对象.方法名
# 对象.属性
# jq->dom
    # jq[index]
# dom->jq
    # $(dom对象)
