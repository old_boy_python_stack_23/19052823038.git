%%OSI七层               TCP/Ip五层

应用层

表示层`	`			应用层

会话层

传输层

网络层

数据链路层

物理层



socket  套接字  位于应用层和传输层中的一个抽象层,一个接口.



C/S  模型  B/S  模型 

百度的服务器(socket服务端)

1. 创建socket对象
2. 绑定(ip 端口)
3. 监听
4. 接收数据
5. 发送数据
6. 断开连接

浏览器 (socket客户端)

1. 创建一个socket对象
2. 链接百度
3. 发送数据
4. 接收数据
5. 断开连接

### Http协议

http://www.cnblogs.com/maple-shaw/articles/9060408.html

HTTP  规定请求和响应的标准

请求方法  8种

GET POST HEAD PUT DELETE TRACE OPTIONS CONNECT 

### get

get是获取请求,没有请求体.可以通过?后面的键值对来交互,但可见,不安全,用于不需保密的内容

django中获取的request.GET这里获取的是?后面键值对的内容,这个内容各种请求都有.所以post请求也有这个功能

并不是GET请求独有.

而request.POST里获取的,是POST请求的请求体里面的内容.是POST独有的.且数据隐藏.

```
get  获取一个页面 
没有请求体
?k1=v1&k2=v2
django 中获取   request.GET   request.GET['k1']  request.GET.get('k1','xxxx')
post 提交数据   数据隐藏
django 中获取   request.POST request.POST['k1']  request.POST .get('k1','xxxx')
```

#### 状态码

1xx   请求接收了 还需要进一步处理

2xx  成功   最正常的,例如200 ok

3xx  重定向,本服务器不能解决请求,但返回一个可能能解决请求的地址.

4xx   请求错误  403权限不够  404 服务器上没有

5xx   服务器的错误

#### url   网络地址的构成

```
https://www.sogou.com/web?query=%E5%A4%9A%E5%9C%B0%E8%B0%83%E6%95%B4%E5%B7%A5%E8%B5%84%E6%A0%87%E5%87%86&_asf=www.sogou.com&_ast=&w=01015002&p=40040108&ie=utf8&from=index-nologin&s_from=index&oq=&ri=0&sourceid=sugg&suguuid=&sut=0&sst0=1566371270500&lkt=0%2C0%2C0&sugsuv=00B70D22DDDAD4905CF78DE27621C931&sugtime=1566371270500&pid=sogou-wsse-af5baf594e9197b4-0001
以上是搜狗搜索网站搜索:'多地调整工资标准'这个内容显示的url地址.
```

协议    https协议,跟http区别为加了安全证书      ://这个是固定格式.

域名  IP   www.sogou.com,这个是域名,通过dns地址解析,解析成ip地址

端口   http  80   https 443   默认隐藏,

URL路径   web这个是URL路径

 查询参数   ?query=  这段内容是搜索内容,上面的代码没有解码

?k1=v1&k2=v2   这是具体参数,以键值对的形式存在.

#### 请求和响应

请求（浏览器发给服务器的数据   request）

 	格式：

​		"请求方法 路径 协议版本\r\n

​		k1 : v1\r\n

​		k2 : v2\r\n

​		\r\n
​		请求数据"          get  请求没有请求体

响应（服务器返回给浏览器的数据  response）

​	格式：

​		"协议版本 状态码 状态描述\r\n

​		k1 : v1\r\n

​		k2 :v2\r'n

​		\r\n

​		响应数据（响应体）"  html文本

### 浏览器发送请求接收响应的流程

1. 在浏览器的地址栏中输入地址，回车。发送一个GET请求。
2. 按照HTTP协议的格式发送数据
3. 服务器接收到数据，拿到url路径，根据url的路径执行对应的函数，得到返回的内容
4. 服务器把响应的内容按照http的响应格式发送给浏览器
5. 浏览器接收到数据，断开连接。解析数据。

### web框架的功能:

1. 本质是使用socket收发消息

   ```
   socket收发消息   django里,由wsgi模块和服务器,wsgiref,来做这一步.部署时用uwsgi
   ```

2. 根据不同路径返回不同的内容

3. 返回动态的数据 (字符串的替换   模板的渲染)

### 分类:

django  2 3 

flask 2

tornado 1 2 3 

### django

#### 下载安装:

1. 命令行

   pip3 install django==1.11.23 -i https://pypi.tuna.tsinghua.edu.cn/simple   清华地址元

2.  pycharm

#### 创建项目:

 1. 命令行

    django-admin startproject 项目名称

	2. pycharm

    flie _   new_project  _   django   _ 项目路径  选解释器  

#### 启动项目:

 1. 命令行

    python  manage.py  runserver  #   127.0.0.1:8000

    python  manage.py  runserver  80  #   127.0.0.1:80

    python  manage.py  runserver  0.0.0.0:80  #   0.0.0.0:80

	2. pycharm

    点绿三角   dj    不要右键运行文件
    
 2. 在settings.py配置文件中修改ALLOWED_HOSTS = []

    这个列表是允许访问的地址.  添加'*'这样的元素,就是允许所有人访问.

#### 简单使用:

urls.py  写url 和函数的对应关系  写函数

```
from django.shortcuts import HttpResponse, render

def index(request):
    return HttpResponse('欢迎进入红浪漫!')  #   返回字符串


def home(request):
    return render(request, 'home.html')  # 返回html页面


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^index/', index),   # 路径和函数的对应关系
    url(r'^home/', home),
]
```

### 1.静态文件的配置

```python
在settings文件中的最后,找到这个配置.
STATIC_URL = '/static/'  # 别名
添加下面的内容,调用时引入路径以/static/开头,即可找到根目录文件中的下面列表内内容.
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static')
]
添加多个文件夹,查找顺序按列表顺序
STATICFILES_DIRS = [   #按照列表的顺序进行查找
    os.path.join(BASE_DIR, 'x1'),
    os.path.join(BASE_DIR, 'static'),
    os.path.join(BASE_DIR, 'x2')
]
```

### 2.登陆的实例

form表单  在form里加入novalidate是在前端不做任何校验.

1. action 提交的地址(不填默认本地)  method 改为post(不指定默认get)  
2. input 需要有name   有些需要有value默认值
3. submit  提交的按钮或者input

目前提交post请求，注释一个csrf 中间件 ,这个是保护机制,

```python
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
```

前端模板 [http://www.jq22.com](http://www.jq22.com/)

### 3.app 

##### 新建APP

python manage.py   startapp  app名称

##### 注册APP

在settings中

```python
INSTALLED_APPS = [
    'app01.apps.App01Config',  # 推荐写法
]
找到这个配置,添加新建的app文件.
```

##### 目录

​	init文件中,可以装需要加载的内容.

​	admin.py   django admin  管理后台工具,可以在里面写代码,对数据做操作

​	apps.py  app的信息,app的名字等

​	models.py    模型  model  跟数据库和om相关,

​	tests.py  这个是测试用的,暂时不用

​	views.py   视图效果,这里就是写函数用的.

```
在views.py文件中,放入函数,并导入响应模块.
from django.conf.urls import url
from django.contrib import admin
from django.shortcuts import HttpResponse,render
在rul文件中引入views
from app01 import views
并以views.的方式调用views中的函数.
 url(r'^admin/', admin.site.urls),
 url(r'^index/', views.index),
 url(r'^home/', views.home),
```

### 4.orm

#### 使用mysql数据库的流程

1. 创建一个mysql数据库 ;

2. 在settings中配置数据库：

   ```python
   DATABASES = {
       'default': {
           'ENGINE': 'django.db.backends.mysql',   # 引擎
           'NAME': 'day53',			# 数据库名称
           'HOST': '127.0.0.1',		# IP
           'PORT': 3306,				# 端口号
           'USER': 'root',			# 用户名
           'PASSWORD': '123'			# 密码
       }
   }
   ```

3. 使用pymysql模块连接mysql数据库。

   写在与settings同级目录下的init.py中

   ```python
   import pymysql
   pymysql.install_as_MySQLdb()
   ```

4. 写对应关系，在app下的models.py中写类。

   ```python
   class User(models.Model):
       pid = models.AutoField(primar_key=True)
       username = models.CharField(max_length=32)  # username  varchar(32)
       password = models.CharField(max_length=32)  # password  varchar(32)
   ```

5. 执行数据库迁移的命令

   python manage.py  makemigrations    # 记录下models.py的变更记录  

   python manage.py migrate   # 变更记录同步到数据库



#### orm 操作

```
在有函数的文件中(views.py),调用
from app01 import models 从app文件夹调用models文件,这个文件是数据库表和对象对应的关系.
类对应表
行数据对应对象
坐标数值对应属性.
ret = models.User.objects.get(username=user, ) # 找不到就报错  找到多个也报错
ret = models.User.objects.filter(username=user, )  # 对象列表
```

### 出版社管理实例

### 展示

```python
# 查询所有的数据
all_publishers = models.Publisher.objects.all()   #  对象列表

render(request,'publisher_list.html',{'all_publishers':all_publishers})

```

模板的语法：

​	{{  all_publishers }}    变量

​	{% for  i in  all_publishers  %}

​		{{ forloop.counter }}    {{  i }}

​	{% endfor %}

### 新增

```python
# 方式一
ret = models.Publisher.objects.create(name=pub_name)
# 方式二
obj = models.Publisher(name=pub_name)
obj.save()
```

### 删除

```python
pk = request.GET.get('pk')
query = models.Publisher.objects.filter(pk=pk)  # 对象列表
query.delete()  # 通过queryset 删除
query[0].delete()  # 通过单独的对象 删除
```

### 编辑

```python
obj = models.Publisher.objects.filter(pk=pk).first()  # 对象列表中第一个对象
obj.name = pub_name  # 内存中修改
obj.save()			# 提交
```

## 内容回顾

1. django的命令

   1. 下载

      pip3 install django==1.11.23 -i

   2. 创建项目

      切入路径，python django-admin startproject 项目名

   3. 启动项目

      cd 到项目的根目录

      python manage.py runserver 0.0.0.0:80 

   4. 创建app

      python manage.py startapp app名称

   5. 数据库迁移

      python manage.py makemigrations #扫描变化并创建迁移文件

      python manage.py migrate

2. django配置

   1. 注册app

      直接写app的文件名

      app01.apps.App01Config

   2. DATABASES

      1. 引擎 mysql
      2. name:数据库名
      3. HOST:127.0.0.1
      4. PORT:3306
      5. USER:root
      6. PASSWORD:123

   3. 静态文件

      1. STATIC_URL='/static/'

      2. STATICFILES_DIRS=[

         os.path.join(BASE_DIR,'static')]

   4. 中间件

      注释掉csrf,提交POST请求时,不用校验了.

   5. 模板

      TEMPLATES

      DIRS=[os.path.join(BASE_DIR,templates)

3. django使用mysql数据库的流程

   1. init文件中,通过pymysql指定数据库

   2. 在数据库中建库,

   3. 通过上面的settings文件的DATABASE,name指定新库.

   4. models.py中写类

      class publisher(models.Model):

      ​	name=models.charfile(max_length=32)

   5. 迁移命令

      python manage.py makemigrations

      python manage.py migrate

4. get和post区别

   ​	默认发送的请求都是get

   ​	a标签

5. orm

   面向对象和关系型数据库的一种映射关系

   对应关系

   ​		类 ---表

   ​		对象---行数据

   ​		属性---字段

   查询:

   from app01 import models导入.

   models.类.objects.get 有且唯一,不然报错

   models.类.objects.filter 查询满足条件的所有内容 对象列表queryset

   .all() 查询所有数据.

   新增:

   ​	.create(name=xxxx) 返回值是新建的对像

   ​	obj= models.publisher(name='xxxx')

   删除

   ​	models.Publisher.objects.filter(pk=1).delete()

   ​	obj = models.Publisher.objecs.filter(pk=1).first() obj.delete()

   修改:

   ​	obj=models.publisher.objects.filter(pk=1).first()

   ​	obj.name='xxx'

   ​	obj.save()

6. 模板的语法

   render(request,'页面地址',{'k1':v1}

   1. 变量

      {{  k1 }}

      for 循环 

      {%  for i in  k1 %}

      ​	{{  forloop.counter }}

      ​	{{ i }}

      {% endfor %}

      

      外键

      一对多 

      图书管理系统

      出版社

      书

      外键的设计

      ```python
      class Book(models.Model):
          title = models.CharField(max_length=32)
          pid = models.ForeignKey('Publisher', on_delete=models.CASCADE) # 外键 
          #  on_delete  2.0 必填 
         
      ```

   查询：

   ```
   all_books = models.Book.objects.all()
   print(all_books)
   for book in all_books:
       print(book)
       print(book.pk)
       print(book.title)
       print(book.pub,type(book.pub))  # 所关联的对象 
       print(book.pub_id,type(book.pub_id))  # 所关联的对象的pk
       print('*' * 32)
   ```

   新增

   ```
   models.Book.objects.create(title=title,pub=models.Publisher.objects.get(pk=pub_id))
   models.Book.objects.create(title=title, pub_id=pub_id)
   
   ```

   删除：

   ```
   pk = request.GET.get('pk')
   models.Book.objects.filter(pk=pk).delete()
   ```

   编辑：

   ```
   book_obj.title = title
   book_obj.pub_id = pub_id
   # book_obj.pub = models.Publisher.objects.get(pk=pub_id)
   book_obj.save()
   ```

   ## 内容回顾

   django

   ### 1.django处理请求的一个流程

   1. 在浏览器上输入地址 回车，发送一个get请求
   2. wsgi模块接收请求，把请求相关的内容封装成request对象
   3. 根据url地址，找到对应函数。
   4. 执行函数，得到返回值。wsgi模块将httpresponse对象按照http响应的格式发送给浏览器。

   ### 2.发请求的途径

   1. 在浏览器上输入地址 回车 get
   2. form表    get/post
   3. a标签    get

   ### 3.函数

   ​    request

   ​	request.GET       url上携带的参数   ？k1=v1&k2=v2

   ​	request.POST    POST请求提交的数据

   ​	request.method   请求方式  GET POST  

   ​    返回值

   ​	HttpResponse('xxxxxx')       返回字符串

   ​	render(request,'模板的文件名',{  })     返回一个完整的页面

   ​	redirect(要跳转到的路径)       重定向   本质  Location：/publisher_list/

   ### 4.外键

   描述一对多的关系

   publisher      book   

   ```python
   class Publisher(models.Model):
       pid = models.AutoField(primary_key=True)
       name = models.CharField(max_length=32, unique=True)
   
       def __str__(self):
           return "{} {}".format(self.pid, self.name)
   
       __repr__ = __str__
   
   
   class Book(models.Model):
       title = models.CharField(max_length=32)
       pub = models.ForeignKey('Publisher', on_delete=models.CASCADE)
   ```

   查询：

   ```python
   books = Book.objects.all()  # 查所有
   for book in books :
       print(book)   #  一个book对象
       print(book.pk)   print(book.id)   #  主键
       print(book.title)    # 标题
       print(book.pub_id)   # 关联数据的id  
       print(book.pub)      # 关联对象
   ```

   新增

   ```python
   Book.objects.create(title='xxxx',pub=pub_obj)
   Book.objects.create(title='xxxx',pub_id=pub_obj.pk)
   
   obj = Book(title='xxxx',pub=pub_obj)
   obj.save()
   
   ```

   删除

   Book.objects.filter(pk=1).delete()

   Book.objects.get(pk=1).delete()

   编辑

   obj = Book.objects.filter(pk=1).first()

   obj.title='xxxx'  

   obj.pub=  pub_obj  

   obj.pub_id=  id

   obj.save()

   ### 5.模板

   {{  k1 }}  

   for 

   {%  for  i in list  %}

   ​	{{   forloop.counter  }}

   ​	{{  i }}	

   {% endfor %}

   if 

   {% if  条件  %}

   ​	xxxx

   {% else %}

   ​	qqqq

   {% endif  %}

   ## 今日内容

   多对多

   ### 表结构的设计

   ```python
   class Book(models.Model):
       title = models.CharField(max_length=32)
       pub = models.ForeignKey('Publisher', on_delete=models.CASCADE)
   
   
   class Author(models.Model):
       name = models.CharField(max_length=32)
       books = models.ManyToManyField('Book')  # 描述多对多的关系  不生成字段  生成关系表
       
       
   
   ```

   

   ### 查询

   ```python
   all_authors = models.Author.objects.all().order_by('id')
   
   for author in all_authors:
       print(author)
       print(author.name)
       print(author.pk)
       print(author.books)  # 关系管理对象
       print(author.books.all())  # 所关联的所有对象
       print('*' * 32)
   ```

   ### 新增

   ```python
   books = request.POST.getlist('books')   # 获取多个元素
   
   # 新建作者
   author_obj = models.Author.objects.create(name=name)
   # 给作者和书籍绑定关系
   author_obj.books.set(books) # 【id,id】
   
   ```

   ### 删除

   ```python
   pk=request.GET.get('pk') #拿到pk
   if not pk:
   return HttpResponse('要删除的内容不存在')
   else:
   models.author.objects.get(pk=pk).delete()
   return redirect('/author_list/')
   ```

   

   ### 编辑

   ```python
   def author_edit(request):
       error=''
       pk=request.GET.get('pk')
       author=models.author.objects.filter(pk=pk).first()
       all_books=models.book.objects.all()
       if request.method == 'POST':
           name = request.POST.get('name')
           books=request.POST.getlist('books')   #这里直接获取多项选择的结果列表
           if not name:
               error='不能为空'
           else:
               author.name=name
               author.save()
               author.books.set(books)   #这里使用列表直接写入关系表.
               return redirect('/author_list/')
   
       return render(request,'author_edit.html',{'author':author,'all_books':all_books,'error':error})
   ```

   

   ### 创建多对多的表的方法

   1. django通过ManyToManyField自动创建第三张表

   ```python
   class Book(models.Model):
       title = models.CharField(max_length=32)
       pub = models.ForeignKey('Publisher', on_delete=models.CASCADE)
       # authors = models.ManyToManyField('Author')  # 描述多对多的关系   不生成字段  生成关系表
   
       def __repr__(self):
           return self.title
   
       __str__ = __repr__
   
   class Author(models.Model):
       name = models.CharField(max_length=32)
       books = models.ManyToManyField('Book')  # 描述多对多的关系   不生成字段  生成关系表
   ```

   2. 自己手动创建

   ```python
   class Book(models.Model):
       title = models.CharField(max_length=32)
   
   
   class Author(models.Model):
       name = models.CharField(max_length=32)
   
   
   class Book_Author(models.Model):
       book = models.ForeignKey(Book, on_delete=models.CASCADE)
       author = models.ForeignKey(Author, on_delete=models.CASCADE)
       date = models.DateField()
   ```

   3. 自己创建 + ManyToManyField

   ```python
   class Book(models.Model):
       title = models.CharField(max_length=32)
   
   
   class Author(models.Model):
       name = models.CharField(max_length=32)
       books = models.ManyToManyField(Book, through='Book_Author')
   
   
   class Book_Author(models.Model):
       book = models.ForeignKey(Book, on_delete=models.CASCADE)
       author = models.ForeignKey(Author, on_delete=models.CASCADE)
       date = models.DateField()
   ```

## django 第六天 内容回顾

### 1.django所有的命令

下载安装：

pip install django==1.11.23  -i  源

创建项目

django-admin startproject 项目名

启动项目

切换到项目的根目录

python manage.py  runserver   #   127.0.0.1:8000

python manage.py  runserver   80  #   127.0.0.1:80

python manage.py  runserver   0.0.0.0:80  #    0.0.0.0:80

创建APP

python manage.py startapp  app名称

数据库迁移的命令

python manage.py makemigrations   # 记录下所有App下的models的变更记录 

python manage.py migrate   # 同步迁移的记录

### 2.配置

TEMPLATES 模板

​	DIRS []

静态文件

​	STATIC_URL = '/static/'   # 静态文件的别名

​	STATICFILES_DIRS = [

​		os.path.join(BASE_DIR,'static'),

​	]

数据库 DATABASES

​	ENGINE :  mysql

​	NAME:  数据库名

​	USER: 用户名

​	PASSWORD: 密码

​	HOST : 127.0.0.1

​	PORT：端口 

中间件
	注释掉 csrf   提交post请求

APP  

​	INSTALLED_APPS = [

​		‘app01’  或者  ‘app01.apps.App01Config’

​	]  

### 3.django使用mysql数据库的流程

1. 创建一个mysql数据库

2. 在settings中配置数据库的配置

   ​	ENGINE :  mysql

   ​	NAME:  数据库名

   ​	USER: 用户名

   ​	PASSWORD: 密码

   ​	HOST : 127.0.0.1

   ​	PORT：端口 

3. ### 告诉django使用pymysql连接数据库

   写在与settings同级目录下的`__init__.py`中：

   import  pymysql

   pymysql.install_as_MySQLdb()

4. 在app下的models中写类：

   ```python
   class Publisher(models.Model):
       pid = models.AutoField(primary_key=True)
       name = models.CharField(max_length=32)  # varchar(32)
   
   
   class Book(models.Model):
       name = models.CharField(max_length=32)
       pub = models.ForeignKey('Publisher', on_delete=models.CASCADE)
   
   
   class Author(models.Model):
       name = models.CharField(max_length=32)
       books = models.ManyToManyField('Book')
   ```

5. 执行数据库迁移的命令

   python manage.py makemigrations   # 记录下所有App下的models的变更记录 

   python manage.py migrate   # 同步迁移的记录

### 4.request

​	request.GET   url上携带的参数   {}         ?k1=v1&k2=v2

​	request.POST  form表单提交POST请求的数据   {}     get()     getlist()

​	request.method  请求的方法   GET  POST PUT

### 5.response

​	HttpResponse('字符串')    返回字符串

​	render(request，‘模板的文件名’，{})   返回一个完整的页面

​	redirect(‘URL的路径’)  重定向     响应头  Location：url  

### 6.orm

对应关系

类   ——》   数据表

对象   ——》   数据行（记录）

属性  ——》    字段

查询：

```python
from app01 import models 
models.Publisher.objects.get(name='xxx')   # 获取一个满足条件的对象  找不到或者是多个就报错
models.Publisher.objects.filter(name='xxx')  # 获取满足条件的所有的对象  queryset 对象列表
models.Publisher.objects.all()   # 获取所有的数据


pub_obj.name # 出版社的名字
pub_obj.pk  pub_obj.pid # 出版社的主键

book_obj.pk
book_obj.name 
book_obj.pub      #  所关联的出版社对象
book_obj.pub_id    #  所关联的出版社对象id

author_obj.books  # 关系管理对象
author_obj.books.all()  # 所关联的所有的书籍对象
```

新增：

```python
pub_obj =models.Publisher.objects.create(name='xxxx')
pub_obj = models.Publisher(name='xxxx')
pub_obj.save()

models.Book.objects.create(name='xxxx',pub=pub_obj)
models.Book.objects.create(name='xxxx',pub_id=pub_obj.pk)


author_obj = models.Author.objects.create(name='xxxx')
author_obj.books.set([1,2])   # 设置多对多的关系
```

删除：

```python
models.Publisher.objects.filter(pk=pk).delete()
pub_obj.delete()
```

编辑：

```python
pub_obj.name='qqqq'
pub_obj.save()

book_obj.name='xxxx'
book_obj.pub=pub_obj
book_obj.pub_id=pub_obj.pk
book_obj.save()

author_obj.name ='xxx'
author_obj.save()
author_obj.books.set([id,id])    # 设置

```

### 7.模板

render(request,'模板的文件名'，{k1：v1})

{{ k1  }}   v1

if 

{% if  条件 %}

​	x1

{% else%}

​	x2

{% endif %}

for 

{%  for i in list %}

​	{{  forloop.counter }}

​	{{ i }}

{% endfor %}

## 今日内容

### MVC 和 MTV

MVC:

​	M: model   模型  操作数据库

​	V： view  视图  展示页面  HTML

​	C: controller  控制器   调度  业务逻辑

MTV:

​	M : model   模型  操作数据库  orm

​	T :  template  模板  HTML  

​	V: view  视图 业务逻辑  

### 变量

{{  变量名 }}

{{  变量名.  }}

.索引   .key   .属性   .方法

### 过滤器

#### 内置的过滤器

修改变量的显示结果

语法：

render(request,'模板文件名'，{'k1':v1})

{{  k1 }} 

{{  name_list.0  }}

{{  dic.key  }}

{{  dic.keys  }}

{{  dic.values  }}

{{  dic.items}}

{{  p1.name  }}

{{  p1.talk  }}

 {{ value|filter_name }}   {{ value|filter_name:参数 }}

default： 提供默认值

 {{ value|default:'xxxx'}}

传过来的变量不存在或者为空 使用默认值

{{ name_list|slice:'-1::-1' }}  切片

add + 

数字的加法 字符串的拼接 列表的合并

日期格式化

```python
{{ now|date:'Y-m-d H:i:s' }}
```

settings的配置：

```
USE_L10N = False
DATETIME_FORMAT = 'Y-m-d H:i:s'
```

safe   告诉django不需要转义

```
{{ js|safe }}
```

py文件中

```
from django.utils.safestring import mark_safe

'a':mark_safe('<a href="http://www.baidu.com">跳转</a>')

{{ a }}
```

#### 自定义过滤器

##### 定义

1. 在app下创建一个名为templatetags的python包

2. 在包内创建py文件 —— 》名字自定义   my_yags.py

3. 在py文件中写入：

   ```python
   from django import template
   register = template.Library()  # register不能变
   ```

4. 定义函数 + 加装饰

   ```python 
   @register.filter
   def new_upper(value, arg=None):  #arg参数可有可无,通常设默认值为None.
       print(arg)
       return value.upper()
   
   @register.filter(name='xxxx')
   def add_arg(value,arg):
      return  "{}_{}".format(value,arg)
   ```

##### 在模板中使用：

```python
{% load my_tags %}   # 在模板中申明了这个,才能使用自定义过滤器.
{{ 'asd'|new_upper:dic }}
{{ 'alex'|add_arg:'dsb' }}  'alex_dsb'
{{ 'alex'|xxxx:'dsb' }}  'alex_dsb'
```

### tag {%   %}

#### for

```
{%  for i in list %}
{{ forloop.counter }}
{{ i }}
{% endfor %}
forloop
{{ forloop.counter }}        当前循环的从1开始的计数
{{ forloop.counter0 }}      当前循环的从0开始的计数
{{ forloop.revcounter }}    当前循环的倒叙计数（到1结束）
{{ forloop.revcounter0 }}    当前循环的倒叙计数（到0结束）
{{ forloop.first}}    当前循环是否是第一次循环  布尔值
{{ forloop.last}}    当前循环是否是最后一次循环  布尔值
{{ forloop.parentloop }}    当前循环父级循环的forloop
{% for name in name_list %}
    {{ name }}
{% empty %}
    空的数据
{% endfor %}
```

### if判断

```
{% if p1.age < 18 %}
    他还是个宝宝
{% elif p1.age == 18 %}
    刚成年 可以出家
{% else %}
    是个骚老头子 坏得很
{% endif %}
# 不支持算术运算 不支持连续判断
```

#### 变量名重新定义

```
{% with alex=person_list.1.name age=person_list.1.age   %}
    {{ alex }} {{ age }}
    {{ alex }}
    {{ alex }}
    {{ alex }}
{% endwith %}
```

#### POST请求的校验

```
{% csrf_token %}  # 这个标签放在form表单内,会自动生成校验标签.  提交时,中间件的csrf功能就会起作用
# 标签放在form标签中，form表单中有一个隐藏的input标签 name  ='csrfmiddlewaretoken'   
```

### 母版的继承

#### 母版

母板：

1. html页面  提取多个页面的公共部分

2. 定义多个block块，需要让子页面覆盖填写

   ```
   {% block content%}  #这里的content是block块的名字.
   这里可以放内容也可以不放
   {% endblock%}    这就是一个block块
   ```

继承：

1. {% extends  ‘母板文件名’ %}

2. 重写block块 ，写在block内部

   ```
   {% extends  ‘母板文件名’ %}  开头先写继承的母版文件名
   {% block content%}  #这里要填写继承的母版中对应的block块的内容.
   {% endbolck%}闭合.    母板块中没有被继承的block依然显示原来的内容,如果为空则不显示.
   ```

   

注意：

1. {% extends 'base.html' %}    带上引号   不带的话会当做变量
2. {% extends 'base.html' %} 上面不要写其他内容
3. 要显示的内容写在block块中
4. 母板定义多个block   定义  css js

组件 

html 页面    包含  一小段代码   _>  nav.html

使用：

​	{%  include  ‘nav.hmtl ’ %}

### 静态文件相关

引入静态文件：

```
{% load static %}
<link rel="stylesheet" href="{% static 'css/dashboard.css' %}">
{% get_static_prefix %}   '/s1/'  #这个标签是直接获取s1.并拼接到文件前面.
# 在seting文件中,static配置时,/static/这个别名在更换时,就可以不在html页面中更换了.
```

### 定义  filter  simple_tag   inclusion_tag

1. 在app下创建templatetags的python包

2. 在包内创建py文件   my_tags.py

3. 在py文件中写代码：

   ```python
   from django import  template
   register = template.Library()
   ```

4. 定义函数  +  加装饰器

   ```python
   @register.filter
   def add_arg(value,arg):
      return  "{}_{}".format(value,arg)   	# 这是和上面一样的过滤器
   @register.simple_tag
   def join_str(*args, **kwargs):  		# 这个是自定义函数,可接受任意数量参数的.
       return '_'.join(args) + "*".join(kwargs.values())
   
   @register.inclusion_tag('page.html')   #page.html是另一个页面.
   def page(num):			#这里是输入参数
       return {'num':range(1,num+1)}    #这里将参数传到page.html里,并有page执行返回结果. 显示在原应用页面里 .    对比母版,这个更像是子板.    从母版处继承外套,自己填进去.    从子板拿过来,放在自己肚子里. 
   page.html 
   ```

