# ⼆、数据库操作（共25分）
# 请根据描述完成下列题⽬
# 表结构
# ⽤户(user)表：⽤户id(user_id),⽤户名(user_name),密码(password)
# 订单(orders)表：订单编号(order_id)，下单时间(order_time)
# ⽀付⾦额(payment)，⽤户id(uid)
# 商品(goods)表：商品编号(goods_id)，商品名称(gname),商品单价
# (price)，库存(g_num)
# 商品_订单表(goods_orders)：编号(id)，商品编号(g_id)，订单编号
# (o_id)，购买个数(buy_num)
# ddl语句：（5分）
# 1）创建表，并根据描述给表中的字段设置合理的数据类型，并设计合理
# 的表与表之间关系；
#建表语句:数据库当中执行
# create database shoping;
# use shoping;
#创建用户表
#create table user(user_id int primary key auto_increment,
#user_name char(12) not null unique,password char (12) not null);
# 创建订单表
# create table orders(order_id int primary key auto_increment,
# order_time datetime,payment float(9,2),uid int,
# foreign key(uid) references user(user_id) on update cascade);
#创建商品表
# create table goods(goods_id int primary key auto_increment,
# gname char(12),price float(8,2),g_num int)
# 创建商品_订单表
# create table goods_orders(id int primary key auto_increment,
# g_id int,o_id int,buy_num int)

# dml语句 :（20分）
# 1）使⽤python，读⽂件，并通过pymysql模块将⽂本内容写⼊数据库
# （4分）

import pymysql
conn = pymysql.Connection(host='127.0.0.1', user='root', password="123456",
                 database='shoping')
cur = conn.cursor()
with open('user',encoding='utf-8') as f:
    try:
        for line in f:
            line = line.strip()
            lst = line.split(',')
            sql = 'insert into user(user_id,user_name,password) values (%s,%s,%s);'
            cur.execute(sql,lst)
    except:
        conn.rollback()
conn.commit()
cur.close()
conn.close()

import pymysql
conn = pymysql.Connection(host='127.0.0.1', user='root', password="123456",
                 database='shoping')
cur = conn.cursor()
with open('orders',encoding='utf-8') as f:
    try:
        for line in f:
            line = line.strip()
            lst = line.split(',')
            sql = 'insert into orders(order_id,order_time,payment,uid) values (%s,%s,%s,%s);'
            cur.execute(sql,lst)
    except:
        conn.rollback()
conn.commit()
cur.close()
conn.close()

import pymysql
conn = pymysql.Connection(host='127.0.0.1', user='root', password="123456",
                 database='shoping')
cur = conn.cursor()
with open('goods',encoding='utf-8') as f:
    try:
        for line in f:
            line = line.strip()
            lst = line.split(',')
            sql = 'insert into goods(goods_id,gname,price,g_num) values (%s,%s,%s,%s);'
            cur.execute(sql,lst)
    except:
        conn.rollback()
conn.commit()
cur.close()
conn.close()

import pymysql
conn = pymysql.Connection(host='127.0.0.1', user='root', password="123456",
                 database='shoping')
cur = conn.cursor()
with open('goods_orders',encoding='utf-8') as f:

    for line in f:
        line = line.strip()
        lst = line.split(',')
        sql = 'insert into goods_orders(id,g_id,o_id,buy_num) values (%s,%s,%s,%s);'
        cur.execute(sql,lst)

conn.commit()
cur.close()
conn.close()

# 2）求出价格最贵的商品id（2分）
# select goods_id,price from goods order by price desc limit 1;
# 运行结果
# +----------+---------+
# | goods_id | price   |
# +----------+---------+
# |       14 | 5000.00 |
# +----------+---------+

# 3）求⽀付⾦额最⾼的订单（2分）
# select order_id,payment from orders order by payment desc limit 1;
# +----------+---------+
# | order_id | payment |
# +----------+---------+
# |       18 | 5000.00 |
# +----------+---------+

# 4）求各商品的商品名和总销量（2分）
# select gname,sum(buy_num) from goods g1 inner join goods_orders g2 on g1.goods_id = g2.g_id
#  group by g2.g_id
# +-----------+--------------+
# | gname     | sum(buy_num) |
# +-----------+--------------+
# | 假发      |            6 |
# | 雨伞      |            2 |
# | 键盘      |            7 |
# | 耳机      |            3 |
# | 钢笔      |           10 |
# | 啤酒      |            6 |
# | 白酒      |            6 |
# | 烟        |            3 |
# | 钢笔      |            1 |
# | 香水      |            5 |
# | 假牙      |            2 |
# | 扑克牌    |          100 |
# | 泡面      |           20 |
# | 手机      |            1 |
# +-----------+--------------+
# 5）求个⼈消费总⾦额⼤于1000元的⽤户(2分)
# select user_name,payment from user as i inner join orders as o on i.user_id = o.uid
# where payment >1000;
#结果
# +-----------+---------+
# | user_name | payment |
# +-----------+---------+
# | alex      | 2000.00 |
# | wusir     | 5000.00 |
# +-----------+---------+
# 6）求’taibai’购买的所有订单，并按照订单总额从⾼到低排序（2分）
# select user_name,order_id,payment from user as i inner join orders as o on i.user_id = o.uid
# where user_name = 'taibai' order by payment desc;
# +-----------+----------+---------+
# | user_name | order_id | payment |
# +-----------+----------+---------+
# | taibai    |        4 |  500.00 |
# | taibai    |        6 |  400.00 |
# | taibai    |        3 |  300.00 |
# | taibai    |        2 |  200.00 |
# | taibai    |        5 |  150.00 |
# | taibai    |        1 |  100.00 |
# +-----------+----------+---------+


# 7）求出销量最⾼的商品名（2分）
# select gname,sum(buy_num) from goods g1 inner join goods_orders g2 on g1.goods_id = g2.g_id
#  group by g2.g_id order by sum(buy_num) desc limit 1;
#结果
# +-----------+--------------+
# | gname     | sum(buy_num) |
# +-----------+--------------+
# | 扑克牌    |          100 |
# +-----------+--------------+

# 8）求出每个⽉下单的⽤户⼈数（执⾏select month(time) from orders
# 来学习month的⽤法，并完成本题）（2分）
# select month(order_time) as month_,count(*) as count_user from
# orders group by month(order_time);
# +--------+------------+
# | month_ | count_user |
# +--------+------------+
# |      1 |          6 |
# |      2 |          6 |
# |      3 |          5 |
# |      4 |          1 |
# +--------+------------+
# 9）求出本月各商品的销售总⾦额和个数，按照个数排序（2分）
# select * from goods as g inner join goods_orders as g1
# select gname,sum(buy_num)*price,sum(buy_num) as sun_buy_num from goods as g inner join goods_orders as g1
# on g.goods_id = g1.g_id group by g1.g_id having g2.o_id in (select order_id from orders where
# month(order_time) = month(now()));

# +----------+---------------------+---------+------+
# | order_id | order_time          | payment | uid  |
# +----------+---------------------+---------+------+
# |        1 | 2019-01-01 11:12:00 |  100.00 |    1 |
# |        2 | 2019-01-02 11:12:00 |  200.00 |    1 |
# |        3 | 2019-01-05 11:12:00 |  300.00 |    1 |
# 带时间的订单表里没有详细商品内容.
