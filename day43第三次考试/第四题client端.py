# 4、手写⼀个udp协议的cs端，要求实现在客户端输⼊时间格式，server端根据
# 时间格式把当前时间返回给客户端(5分)
# 例如：
# 客户端输⼊ %Y-%m-%d %H:%M:%S,得到结果 2019-6-13 21:51:40
# 客户端输⼊ %m/%d/%Y %H:%M:%S,得到结果 6/13/2019 21:51:40
import socket
udp_client = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
while 1:
    to_server_data =input('>>>').strip()
    udp_client.sendto(to_server_data.encode('utf-8'),('127.0.0.1',9000))
    from_server_data = udp_client.recvfrom(1024)
    print(f'来自{from_server_data[1]}的消息:{from_server_data[0].decode("utf-8")}')