# 3、开启⼀个有20个线程的线程池，提交200个任务
# 1）要获取这200个任务的返回值（5分）
# 2）要等待这200个任务结束前后，在log⽂件中记录开始和结束事件（5分）
from concurrent.futures import ThreadPoolExecutor
import time
import logging
logger = logging.getLogger()
fh = logging.FileHandler('吃包子.log',encoding = 'utf-8')
formatter = logging.Formatter( '%(asctime)s  %(filename)s  [line:%(lineno)d] %(levelname)s  %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)
logger.setLevel(10)
fh.setLevel(10)
def eat(i):
    print(' 吃包子%s'%(i))
    time.sleep(1)
    return i+250
if __name__ == '__main__':
    t = ThreadPoolExecutor(20)
    ret_l = []
    print('开始吃包子')
    logging.info('开始吃包子')
    for i in range(1,201):
        ret = t.submit(eat,i)
        ret_l.append(ret)
    t.shutdown()
    print('包子吃完了')
    logging.info('包子吃完了')




