# 4、手写⼀个udp协议的cs端，要求实现在客户端输⼊时间格式，server端根据
# 时间格式把当前时间返回给客户端(5分)
# 例如：
# 客户端输⼊ %Y-%m-%d %H:%M:%S,得到结果 2019-6-13 21:51:40
# 客户端输⼊ %m/%d/%Y %H:%M:%S,得到结果 6/13/2019 21:51:40
import socket
import time
udp_server = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
udp_server.bind(('127.0.0.1',9000))

while 1:
    from_client_data = udp_server.recvfrom(1024)
    print(f'来自{from_client_data[1]}的消息:{from_client_data[0].decode("utf-8")}')
    f1 = from_client_data[0].decode("utf-8")
    to_client_data = time.strftime(f1)
    udp_server.sendto(to_client_data.encode('utf-8'),from_client_data[1])

