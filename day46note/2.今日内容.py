# javascript  简称js
    # 简单介绍
        # 能够处理逻辑
        # 可以和浏览器交互
        # 不够严谨
    # javascript 包含:
        # ECMAscript 是js的一种标准化规范
            # 我们目前用的是ES5
            # 标出了一些基础的js语法.
        # DOM document objct model
                # 文本对象模型.
                # 主要操作文档中的标签
        # BOM borwser object model 主要操作浏览器
    # js引入和script标签
        # 方式1
            # <script>
            #alert('hello,world')
            # </script>
        # 方式2
            # <script src='first.jx'>
            # </script>
        # PS: 引入css的时候,
            # <img src ="图片地址">
            # 是<a href="www.baidu.com">笑话网</a>
    # js里的结束符
        # 跟python一样,可以用;分号结束,也可以用回车结束. 但由于js通常需要网络传输
        # 所以最好加上;分号来结束.
    # 单行注释 //后面的行内内容被注释. 多行注释/*  中间可换行   */
    # 变量
        # 数字\字母\下划线\$
        # 创建变量的关键字  var a=1
        # 创建变量的时候可以不指定数据类型.弱类型语言.
        # var a; a声明出来但未赋值,是未定义,不报错.
        #
    #输入输出
        # alert 输出弹框
        # prompt 带输入的弹框
        # 控制台输出

    # 基础的数据类型
    # 流程控制
        # 条件判断
        # 循环
    # 函数
a= 'a,b,c,d'
b = a.split(',',2)
print(b)