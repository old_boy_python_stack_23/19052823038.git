# 内容回顾
    # 选择器的优先级
        # 行内>id>类>标签>继承
    # 颜色
        # rgb
        # rgba
        # 井号加6位16进制数
        # 单词
    # 字体
        # font-family 字体"黑体"
        # font-size # 字号尺寸,默认16px
        # font-weight # 粗细  bold加粗,
    # 文本
        # text-align 对齐方式,默认left,可设right ,center居中
        # text-decoration none去掉下划线等样式.   under line下划线,overline上划线,line-through中划线
        # line-height 设置行范围,字体会自动在行范围内垂直居中.
        # text-indent 缩进em单位,按字体数.
    # 背景
        # background-color: 背景色
        # background-image: url(""图片地址)
            # background-repeat:no-repeat  重复铺设
            # background-position :
                # 水平位置  left center right  left 和right 可以带参数
                # 垂直位置 top center bottom
            # background-attachment : fixed
            # background-size : cover;  裁剪全铺.
        # background: 颜色 背景图 重复铺设 位置;
    # 边框
        # border-style:solid 实线
        # border-color:颜色
        # border - width:10px; 设置边框宽度
        # border -top - style:dotted 上,右,下,左
        # border solid red 5px
        # border - radius: 边框圆角
    # display   类型转换
        # 块级元素:block
        # 行内元素:inline
        # 行内块:inline-block
        # none 不显示,不占位.  先隐藏,特殊情况触发
    # 盒模型
        # 盒内
            # content 内容 width height
            # padding 内容和边框之间的剧里
                # paddint-top
            # border 边框
        # 盒外
            # margin:外边距
                # margin- left,距左边.
        # 两个盒子都设置距离,以大的覆盖小的为准. 这种现象叫塌陷.
        # 不设置border的父子盒子,也会塌陷.
        # 更多描述兄弟之间的关系,如果是父子之间的关系用padding来描述
    # 浮动
        # float: left right
        # 浮动起来的盒子会脱离原来的图层,且不再独占一行.
        # 子盒不再撑起父盒
            # 清楚浮动: clear:both
            # 伪元素选择器,做清除. clearfix:after{
                # content:'';
                # clear:both;
                # display:block}
            # 加div块标签
            # overflow: hidden scroll auto
    # overflow
        # 溢出部分
        # visible 默认溢出依然显示
        # hidden 默认溢出不显示
        # auto scroll 溢出滚动条.
    # 定位
        # position:
            # relative 相对 相对自己原来的位置,原来的位置还占.
            # absolute 绝对 相对于整个网页的位置.不占原来的位置
            # fixed 固定 相对于窗口位置固定.
            #
    # z-index
        # 表示图层顺序
        # 值越大,越靠前
        # 只有定位重合的盒子,设置才有意义
    # 透明度opacity
        # opacity: 0.5
        # 是整个标签的透明度














