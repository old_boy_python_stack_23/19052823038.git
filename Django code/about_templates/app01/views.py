from django.shortcuts import render
import datetime
# Create your views here.

def index(request):
    num = 1
    string='下九流'
    boo=True
    name_list=['师爷','衙差','升秤','媒婆','走卒','时妖','盗']

    dic = {
        '一流':'佛祖',
        '二流':'仙',
        '三流':'皇帝',
        '四流':'官',
        '五流':'烧锅',
        '六流':'当',
        '七流':'商',
        '八流':'客',
        '九流':'庄田',
    }
    num_list=[1,2,3,4,5,6,7,8,9,0]
    now=datetime.datetime.now()
    context={
        'num':num,
        'string':string,
        'boo':boo,
        'name_list':name_list,
        'dic':dic,
        'num_list':num_list,
        'now':now,
    }
    return render(request,'index.html',context)