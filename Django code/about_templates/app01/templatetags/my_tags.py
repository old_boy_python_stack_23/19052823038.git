from django import template
from django.utils.safestring import mark_safe
register=template.Library()

@register.filter
def multiplication(value,multiplier):
    return value*multiplier

@register.filter
def division(value,divisor):
    return value/divisor

@register.filter
def show_a(value,show):
    return '<a href="http://{}">"{}"</a>'.format(value,show)

@register.filter
def show_a2(value,show):
    return mark_safe('<a href="http://{}">"{}"</a>'.format(value,show))

@register.inclusion_tag('count.html')
def sqr_list(num):
    return {'num':range(0,num+1)}