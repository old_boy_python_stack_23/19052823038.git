# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2019-08-24 07:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='class_manage',
            fields=[
                ('cid', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=32, unique=True)),
                ('thacher', models.CharField(max_length=12)),
                ('people_num', models.IntegerField(null=True)),
            ],
        ),
    ]
