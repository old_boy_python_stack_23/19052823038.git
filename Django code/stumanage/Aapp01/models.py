from django.db import models

class class_manage(models.Model):
    cid = models.AutoField(primary_key=True)
    name=models.CharField(max_length=32,unique=True)
    teacher=models.CharField(max_length=12)
    people_num=models.IntegerField(null=True)

    def __str__(self):
        return '{} {} {} {}'.format(self.cid,self.name,self.teacher,self.people_num)