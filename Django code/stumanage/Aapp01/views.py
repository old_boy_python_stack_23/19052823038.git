from django.shortcuts import render,HttpResponse,redirect
from Aapp01 import models

def class_list(request):
    ret=models.class_manage.objects.all()
    return render(request,'class_list.html',{'ret':ret})

def class_add(request):
    value=['','','']
    error=''
    if request.method=='POST':
        class_name=request.POST.get('class_name')
        teacher=request.POST.get('teacher')
        people_num=request.POST.get('people_num')
        if not class_name:
            error='班级不能为空'
        else:
            if models.class_manage.objects.filter(name=class_name):
                error='班级已存在'
            else:
                if not people_num.isalnum():
                    people_num =None

                models.class_manage.objects.create(name=class_name,
                                                   teacher=teacher,people_num=people_num)
                return redirect('/class_list/')


    return render(request,'class_add.html',{ 'error':error})

def class_del(request):
    pk=request.GET.get('pk')
    query = models.class_manage.objects.filter(pk=pk)
    if not query:
        return HttpResponse('想要删除的数据不存在')
    query.delete()
    return redirect('/class_list/')

def class_edit(request):
    value=['','','']
    error=''
    pk=request.GET.get('pk')
    obj = models.class_manage.objects.filter(pk=pk).first()
    if not obj:
        return HttpResponse('想要删除的数据不存在')
    value[0],value[1],value[2]=obj.name,obj.teacher,obj.people_num
    if request.method=='POST':
        class_name = request.POST.get('class_name')
        teacher = request.POST.get('teacher')
        people_num = request.POST.get('people_num')
        if not class_name:
            error = '班级不能为空'
        else:
            if models.class_manage.objects.filter(name=class_name) and obj.name!=class_name:
                error='班级已存在'
            else:
                if obj.name != class_name:
                    obj.name=class_name
                if not people_num.isalnum():
                    people_num = None
                else:obj.people_num=people_num
                obj.teacher=teacher
                obj.save()
                return redirect('/class_list/')

    return render(request,'class_edit.html',{
        'value0': value[0],
        'value1': value[1],
        'value2': value[2],
        'error':error
    })
