from django.db import models

class bookmanage(models.Model):
    pid = models.AutoField(primary_key=True)
    name=models.CharField(max_length=32,unique=True)

    def __str__(self):
        return '{} {}'.format(self.pid,self.name)


class book(models.Model):
    bid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32)
    pub = models.ForeignKey('bookmanage',on_delete=models.CASCADE)

    def __str__(self):
        return '{} {} {}'.format(self.bid,self.name,self.pub_id)

class author(models.Model):
    name=models.CharField(max_length=32)
    books = models.ManyToManyField(book)
