from django.shortcuts import render,HttpResponse,redirect
from app01 import models
from django.views import View
def publisher_list(request):
    all_publishers=models.bookmanage.objects.all().order_by('pid')
    return render(request,'publisher_list.html',{'all_publishers':all_publishers})
def publisher_add(request):
    error=''
    value=''
    if request.method =='POST':
        pub_name = request.POST.get('publisher_name')
        if pub_name:
            if models.bookmanage.objects.filter(name=pub_name):
                error='提交的内容重复'
                value=pub_name
            else:
                obj = models.bookmanage.objects.create(name=pub_name)
                return redirect('/publisher_list/')
        else:
            error='不能为空'
    return render(request,'publisher_add.html',{'error':error,'value':value})
def publisher_del(request):
    error=''
    # models.bookmanage.objects.
    pk_book = request.GET.get('pk')
    query = models.bookmanage.objects.filter(pid=pk_book)
    if query:
        query.delete()
        return redirect('/publisher_list/')
    else:return HttpResponse('要删除的内容不存在')
def publisher_edit(request):
    pk=request.GET.get('pk')
    obj = models.bookmanage.objects.filter(pk=pk).first()
    if not obj:
        return HttpResponse('要编辑的对象不存在')
    error=''
    if request.method=='POST':
        pub_name = request.POST.get('pub_name')
        if not pub_name:
            error = '不能为空'
        else:
            if not models.bookmanage.objects.filter(name=pub_name):
                obj.name = pub_name
                obj.save()
                return redirect('/publisher_list/')
            elif obj.name==pub_name:
                error='没有做修改'
            else:error='这个出版社已存在'
    return render(request, 'publisher_edit.html', {'obj': obj,'error':error})
def home_page(request):
    return render(request,'home_page.html')
def book_list(request):
    all_book=models.book.objects.all()
    return render(request,'book_list.html',{'all_book':all_book})
def book_add(request):
    error=''
    if request.method=='POST':
        book_name = request.POST.get('book_name')
        pub_id = request.POST.get('pub_id')
        if not book_name:
            error = '不能为空'
        else:
            ret=models.book.objects.create(name=book_name,pub_id=pub_id)
            return redirect('/book_list/')
    all_publishers=models.bookmanage.objects.all().order_by('pid')
    return render(request,'book_add.html',{'all_publishers':all_publishers,'error':error})
def book_del(request):
    pk=request.GET.get('pk')
    ret = models.book.objects.filter(pk=pk)
    if not ret:
        return HttpResponse('要删除的内容不存在')
    ret.delete()
    return redirect('/book_list/')
def book_edit(request):
    error=''
    book_id=request.GET.get('pk')
    query = models.book.objects.filter(pk=book_id).first()
    value=query.name
    pub_id=query.pub_id
    all_publishers=models.bookmanage.objects.all()
    if request.method=='POST':
        book_name = request.POST.get('book_name')
        if book_name:
            pub_id = request.POST.get('pub_id')
            query.name=book_name
            query.pub_id=pub_id
            query.save()
            return redirect('/book_list/')
        else:error='不能为空'

    return render(request,'book_edit.html',{'value':value,'error':error,'all_publishers':all_publishers,'pub_id':pub_id})
def author_list(request):
    all_author = models.author.objects.all()
    return render(request,'author_list.html',{'all_author':all_author})
def author_add(request):
    all_books=models.book.objects.all()
    error=''
    if request.method=="POST":
        name = request.POST.get('name')
        books=request.POST.getlist('books')
        if not name:
            error='不能为空'
        else:
            author_obj=models.author.objects.create(name=name)
            author_obj.books.set(books)
            return redirect('/author_list/')

    return render(request,'author_add.html',{'all':all_books,'error':error})
def author_del(request):
    pk=request.GET.get('pk')
    if not pk:
        return HttpResponse('要删除的内容不存在')
    else:
        models.author.objects.get(pk=pk).delete()
    return redirect('/author_list/')
# def author_edit(request):
#     error=''
#     pk=request.GET.get('pk')
#     author=models.author.objects.filter(pk=pk).first()
#     all_books=models.book.objects.all()
#     if request.method == 'POST':
#         name = request.POST.get('name')
#         books=request.POST.getlist('books')
#         if not name:
#             error='不能为空'
#         else:
#             author.name=name
#             author.save()
#             author.books.set(books)
#             return redirect('/author_list/')
#
#     return render(request,'author_edit.html',{'author':author,'all_books':all_books,'error':error})

class author_edit(View):
    def get(self,request):
        pk = request.GET.get('pk')
        author = models.author.objects.filter(pk=pk).first()
        all_books = models.book.objects.all()
        return render(request,'author_edit.html',{'author':author,'all_books':all_books})
    def post(self,request):
        pk = request.GET.get('pk')
        author = models.author.objects.filter(pk=pk).first()
        all_books = models.book.objects.all()
        name = request.POST.get('name')
        books = request.POST.getlist('books')
        if not name:
            error = '不能为空'
        else:
            author.name = name
            author.save()
            author.books.set(books)
            return redirect('/author_list/')
        return render(request, 'author_edit.html', {'author': author, 'all_books': all_books,'error':error})
