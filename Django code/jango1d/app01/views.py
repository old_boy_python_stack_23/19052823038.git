from django.conf.urls import url
from django.contrib import admin
from django.shortcuts import HttpResponse,render,redirect

# Create your views here.
def index(request):
    return HttpResponse('欢迎进入红浪漫!')
def home(request):
    return render(request,'欢迎.html')
def tab(request):
    return render(request,'tab切换.html')
def load(request):
    return render(request,'load.html')
# def cv_df(request):
#     print(request.method)
#     if request.method=='GET':
#         return render(request,'cv大法.html')
#     elif request.method=='POST':
#         print(request.POST,type(request.POST))
#         username=request.POST.get('username')
#         pwd=request.POST.get('pwd')
#         if username=='zzz' and pwd =='123':
#             return redirect('/home/')
#         else:
#             return render(request,'cv大法.html')
# 以上注掉的是写死的登录验证
from app01 import models
def cv_df(request):
    print(request.method)
    if request.method=='GET':
        return render(request,'cv大法.html')
    elif request.method=='POST':
        print(request.POST,type(request.POST))
        username=request.POST.get('username')
        pwd=request.POST.get('pwd')
        ret = models.User.objects.filter(username=username, password=pwd)
        if ret:
            return redirect('/home/')
        else:
            return render(request,'cv大法.html')

def load_12306(request):
    return render(request,'12306.html')