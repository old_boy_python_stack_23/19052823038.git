'''8.userinfo.txt 文件中存放以下结构:(总分10分)
```
alex:alex3714
wusir:123456
meet:meet123
```
1.让用户选择:
```
1.注册
2.登录
```
2.用户选择注册就将账号和密码添加到userinfo.txt中,如果用户名存在就提示用户名存在,不存在就进行添加(3分)
3.用户选择登录,就验证用户的账号和密码是否与userinfo.txt一致,如果一致终止循环提示登录成功(3分)
4.让用户登录三次,三次错误提示用户名已锁定,并打印错误次数(使用字符串格式化)(4分)
'''
f = open("userinfo.txt",mode='r+',encoding="utf-8")
while 1:
    use_input = input("请输入登陆选择登陆,或输入注册选择注册:")
    if use_input =="登陆":
        break
    elif use_input =="注册":
        break
while use_input =="注册":
    use_name1 = input("请输入您想注册的帐号:")
    if use_name1 in f.read():
        f.seek(0,0)
        print("您注册的账号已存在,请重新输入")
        continue
    else:
        use_pass_word1 = input("请注册您的密码")
        f.seek(0,2)
        f.write('\n'+use_name1+':'+use_pass_word1)
        break
f.seek(0,0)
lis_locks= []
for i in f:
    lis_locks.append(i.strip().split(':'))
count = 1
while use_input == "登陆":
    use_name2 = input("请输入用户名")
    use_pass_word2 = input("请输入密码")
    for i in lis_locks:
        if use_name2 == i[0]:
            if use_pass_word2 == i[1]:
                print('登陆成功,欢迎您,亲爱的%s'%(use_name2))
                break
            else:
                print("您的密码有误,请重新输入,您的错误次数为%d,还剩%d次机会"%(count,3-count))
                count += 1
                if count>3:
                    print("您的错误次数已经3次,错误太多,用户名已锁定")
                    break
    print('您输入的用户名错误,请重新尝试')
f.close()
