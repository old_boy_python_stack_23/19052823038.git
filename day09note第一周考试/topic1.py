#__author:wolfs
#date: 2019/6/14
#题目1
'''
1. lis = [['哇',['how',{'good':['am',100,'99']},'大帅哥'],'I']] （总分2分）
   - 列表lis中的'am'全部变成大写。(1分)
   - 列表中的100通过数字相加在转换成字符串的方式变成'10086'。(1分)
'''

# lis = [['哇',['how',{'good':['am',100,'99']},'大帅哥'],'l']]
# lis[0][1][1]['good'][0] = 'AM'#1问
# lis[0][1][1]['good'][1] = lis[0][1][1]['good'][1] +9986
# lis[0][1][1]['good'][1] = str(lis[0][1][1]['good'][1])#2问
# print(lis)
#题目2
'''
2. dic = {'k1':'v1','k2':['alex','sb'],(1,2,3,):{'k3':['2',100,'wer']}}  （总3分）
   - 将'k3'对应的值的最后面添加一个元素[1,2,3]。(1分)
   - 将'k2'对应的值的第2个位置前插入元素{'a'}。(1分)
   - 将(1,2,3,)对应的值添加一个键值对key:(1,)。(1分)
'''

# dic = {'k1':'v1','k2':['alex','sb'],(1,2,3,):{'k3':['2',100,'wer']}}
# dic[(1,2,3)]['k3'].append([1,2,3])#1问
# dic['k2'].insert(0,'a')#2问
# dic[(1,2,3)]['key'] =(1,)#3问
# print(dic)

'''
3. 实现一个整数加法计算器（多个数相加）：（5分）
 如：content = input("请输入内容:") 用户输入：5+9+6 +12    +  13，
 去除所有的空白,然后进行分割再进行计算。
'''
# use_input = input('请输入内容:')
# li = use_input.replace(' ','').split('+')
# _sum = 0
# for i in li:
#     if i.isdecimal():
#         _sum += int(i)
# print(_sum)

'''
  4.使用for循环计算1-3+5-7+9-11+13...99的结果(5分)
  
'''
# sum_sum = 0
# for i in range(1,100):
#     if i%4 == 3:
#         sum_sum -= i
#     elif i%2 == 1:
#         sum_sum += i
# print(sum_sum)


'''5.有文件t1.txt里面的内容为:（5分）

```
1,alex,22,13651054608,IT
2,wusir,23,13304320533,Tearcher
3,taibai,18,1333235322,IT
利用文件操作，将其构造成如下数据类型。
[{'id':'1','name':'alex','age':'22','phone':'13651054608','job':'IT'},
 {'id':'2','name':'wusir','age':'23','phone':'13304320533','job':'Tearcher'},
 {'id':'3','name':'taibai','age':'18','phone':'1333235322','job':'IT'},]
```
'''
# f = open('t1.txt',mode='r',encoding='utf-8')
# li1 = []
# for i in f:
#     if i[0].isdecimal():
#         li = i.strip().split(',')
#         dic = {'id':li[0],'name':li[1],'age':li[2],'phone':li[3],'job':li[4]}
#         li1.append(dic)
#     else:
#         continue
# f.close()
# print(li1)

'''6.有如下车牌和车辆归属地,形成一个新的字典,显示每个归属地的车辆共有多少：(5分)
cars = ['鲁A32444','鲁B12333','京B8989M','⿊C49678','⿊C46555','沪B25044','冀G11111']
locals = {'冀':'河北', '⿊':'⿊⻰江', '鲁':'⼭东', '鄂':'湖北', '湘':'湖南','京':'北京'}
结果: {'⿊⻰江':2, '⼭东': 2, '北京': 1,'河北':1}
'''
# cars = ['鲁A32444','鲁B12333','京B8989M','⿊C49678','⿊C46555','沪B25044','冀G11111']
# locals = {'冀':'河北', '⿊':'⿊⻰江', '鲁':'⼭东', '鄂':'湖北', '湘':'湖南','京':'北京'}
# # dic1 = {}
# li = []
# locals['沪']= '上海'
# for i in cars:
#     li.append(locals[i[0]])
#     dic1= dict.fromkeys(li,0)
# for i in cars:
#     dic1[locals[i[0]]] += 1
# del dic1['上海']#要不要上海搞不清楚,按题目前两行的意思是要上海.最后的结果却又说不要.用户需求模糊.
# print(dic1)

'''7.有如下值li= ["a","b",11,22,33,44,55,"66","77","88","99"]，
将所有的数字保存至字典的第一个key中，将所有的字符串保存至第二个key的值中。(5分)
'''
# li= ["a","b",11,22,33,44,55,"66","77","88","99"]
# li1 = []
# li2 = []
# for i in li:
#     if type(i) == int:
#         li1.append(i)
#     else:
#         li2.append(i)
# dic = {'key1':li1,'key2':li2}
# print(dic)

'''8.userinfo.txt 文件中存放以下结构:(总分10分)
```
alex:alex3714
wusir:123456
meet:meet123
```
1.让用户选择:
```
1.注册
2.登录
```
2.用户选择注册就将账号和密码添加到userinfo.txt中,如果用户名存在就提示用户名存在,不存在就进行添加(3分)
3.用户选择登录,就验证用户的账号和密码是否与userinfo.txt一致,如果一致终止循环提示登录成功(3分)
4.让用户登录三次,三次错误提示用户名已锁定,并打印错误次数(使用字符串格式化)(4分)
'''
f = open("userinfo.txt",mode='r+',encoding="utf-8")
while 1:
    use_input = input("请输入登陆选择登陆,或输入注册选择注册:")
    if use_input =="登陆":
        break
    elif use_input =="注册":
        break
while use_input =="注册":
    use_name1 = input("请输入您想注册的帐号:")
    if use_name1 in f.read():
        f.seek(0,0)
        print("您注册的账号已存在,请重新输入")
        continue
    else:
        use_pass_word1 = input("请注册您的密码")
        f.seek(0,2)
        f.write('\n'+use_name1+':'+use_pass_word1)
        break
f.seek(0,0)
lis_locks= []
for i in f:
    lis_locks.append(i.strip().split(':'))
f.close()
count = 1
while use_input == "登陆":
    use_name2 = input("请输入用户名")
    use_pass_word2 = input("请输入密码")
    for i in lis_locks:
        if use_name2 == i[0]:
            if use_pass_word2 == i[1]:
                print('登陆成功,欢迎您,亲爱的%s'%(use_name2))
                break
            else:
                print("您的密码有误,请重新输入,您的错误次数为%d,还剩%d次机会"%(count,3-count))
                count += 1
                if count>3:
                    print("您的错误次数已经3次,错误太多,用户名已锁定")
                    exit()



