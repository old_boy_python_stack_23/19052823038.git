'''7.有如下值li= ["a","b",11,22,33,44,55,"66","77","88","99"]，
将所有的数字保存至字典的第一个key中，将所有的字符串保存至第二个key的值中。(5分)
'''
li= ["a","b",11,22,33,44,55,"66","77","88","99"]
li1 = []
li2 = []
for i in li:
    if type(i) == int:
        li1.append(i)
    else:
        li2.append(i)
dic = {'key1':li1,'key2':li2}
print(dic)