'''
3. 实现一个整数加法计算器（多个数相加）：（5分）
 如：content = input("请输入内容:") 用户输入：5+9+6 +12    +  13，
 去除所有的空白,然后进行分割再进行计算。
'''
use_input = input('请输入内容:')
li = use_input.replace(' ','').split('+')
_sum = 0
for i in li:
    if i.isdecimal():
        _sum += int(i)
print(_sum)