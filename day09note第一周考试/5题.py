'''5.有文件t1.txt里面的内容为:（5分）

```
1,alex,22,13651054608,IT
2,wusir,23,13304320533,Tearcher
3,taibai,18,1333235322,IT
利用文件操作，将其构造成如下数据类型。
[{'id':'1','name':'alex','age':'22','phone':'13651054608','job':'IT'},
 {'id':'2','name':'wusir','age':'23','phone':'13304320533','job':'Tearcher'},
 {'id':'3','name':'taibai','age':'18','phone':'1333235322','job':'IT'},]
```
'''
f = open('t1.txt',mode='r',encoding='utf-8')
li1 = []
for i in f:
    if i[0].isdecimal():
        li = i.strip().split(',')
        dic = {'id':li[0],'name':li[1],'age':li[2],'phone':li[3],'job':li[4]}
        li1.append(dic)
    else:
        continue
f.close()
print(li1)