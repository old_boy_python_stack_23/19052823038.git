'''
  4.使用for循环计算1-3+5-7+9-11+13...99的结果(5分)
'''
sum_sum = 0
for i in range(1,100):
    if i%4 == 3:
        sum_sum -= i
    elif i%2 == 1:
        sum_sum += i
print(sum_sum)