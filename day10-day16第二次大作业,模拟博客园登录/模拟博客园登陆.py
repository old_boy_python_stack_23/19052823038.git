''''''
'''
作业：用代码模拟博客园系统
项目分析：
一．首先程序启动，页面显示下面内容供用户选择：
1.请登录
2.请注册
3.进入文章页面
4.进入评论页面
5.进入日记页面
6.进入收藏页面
7.注销账号
8.退出整个程序
二．必须实现的功能：
1.注册功能要求：
a.用户名、密码要记录在文件中。
b.用户名要求：只能含有字母或者数字不能含有特殊字符并且确保用户名唯一。
c.密码要求：长度要在6~14个字符之间。
d.超过三次登录还未成功，则退出整个程序。
2.登录功能要求：
a.用户输入用户名、密码进行登录验证。
b.登录成功之后，才可以访问37选项，如果没有登录或者登录不成功时访问37选项，不允许访问，让其先登录。（装饰器）
3.进入文章页面要求：
a.提示欢迎xx进入文章页面。
b.此时用户可以选择：直接写入内容，还是导入md文件。
①如果选择直接写内容：让学生直接写文件名|文件内容......最后创建一个文章。
②如果选择导入md文件：让用户输入已经准备好的md文件的文件路径
（相对路径即可：比如函数的进阶.md），然后将此md文件的全部内容写入文章（函数的进阶.text）中。
4.进入评论页面要求：
提示欢迎xx进入评论页面。
5.进入日记页面要求：
提示欢迎xx进入日记页面。
6.进入收藏页面要求：
提示欢迎xx进入收藏页面。
7.注销账号要求：
不是退出整个程序，而是将已经登录的状态变成未登录状态（访问3~7选项时需要重新登录）。
8.退出整个程序要求：
就是结束整个程序。
三．选做功能：
1.评论页面要求：
a.提示欢迎xx进入评论页面。
b.让用户选择要评论的文章。
这个需要借助于os模块实现此功能。将所有的文章文件单独放置在一个目录中，利用os模块listdir功能,可以将一个目录下所有的文件名以字符串的形式存在一个列表中并返回。
例如：
代码：
import os
print(os.listdir(r'D:\teaching_show\article'))
['01 函数的初识.text', '02 函数的进阶.text']
c.选择要评论的文章之后，先要将原文章内容全部读一遍，然后输入的你的评论，评论要过滤掉这些敏感字符："苍老师", "东京热", "武藤兰", "波多野结衣"，替换成等长度的"*"之后，写在文章的评论区最下面。
文章的结构：
文章具体内容
.......
评论区：
      (用户名)xx:
      评论内容
      (用户名)oo:
      评论内容
原文章最下面如果没有以下两行：
"""
评论区：
"""
就加上这两行在写入评论，如果有这两行则直接在下面顺延写上：
(用户名)xx:
评论内容
'''
WELCOME = '''
1.请登录
2.请注册
3.进入文章页面
4.进入评论页面
5.进入日记页面
6.进入收藏页面
7.注销账号
8.退出整个程序'''
import json
import os
import hashlib
Login_status = False
usename = ['匿名']
def register():
    if os.path.isfile('use_name.json')==False:
        dic = {'': ''}
        with open('use_name.json', encoding='utf-8', mode='w') as f:
            json.dump(dic, f)
    with open('use_name.json',encoding='utf-8',mode='r+') as f:
        dic = json.load(f)
        while 1:
            use_name = input('请输入用户名,只能由字母或数字组成').strip().replace(' ','')
            if use_name.isalnum():
                if len(ascii(use_name))-2 != len(use_name):
                    print('您输入的内容包含特殊字符,请重新输入')
                    continue
            else:
                print('您输入的内容包含特殊字符,请重新输入')
                continue
            if use_name in dic:
                print('用户名已存在,请重新输入')
                continue
            else:
                break
        while 1:
            use_password = input('请输入密码,6到14位').strip().replace(' ','')
            if 6 < len(use_password) < 14:
                use_password1 = input('请在次输入密码以便确认,6到14位').strip().replace(' ','')
                if use_password == use_password1:
                    break
                else:
                    print('两次密码不一致,请重试')
            else:
                print('您输入的密码长度不对,请重试')
        ret = hashlib.md5()
        ret.update(use_password.encode('utf-8'))
        s1 = ret.hexdigest()
        dic[use_name]=s1
        f.seek(0)
        json.dump(dic, f)
# register()注册函数
def use_load():
    count = 1
    while count < 4:
        use_load_name = input('请输入您的用户名')
        use_load_psd = input('请输入您的密码')
        ret = hashlib.md5()
        ret.update(use_load_psd.encode('utf-8'))
        s1 = ret.hexdigest()
        with open('use_name.json', encoding='utf-8', mode='r') as f:
            dic = json.load(f)
            if use_load_name in dic:
                if dic[use_load_name] == s1:
                    print('登录成功,欢迎您进入博客园')
                    global Login_status
                    Login_status= True
                    usename.append(use_load_name)
                    break
                else:
                    print('您输入的用户名或密码错误,请重试')
                    count += 1
            else:
                print('您输入的用户名或密码错误,请重试')
                count += 1
    else:
        print('您输入的错误次数超过三次,程序自动退出.')
# use_load()#登录函数
def wrapper(f):
    def inner():
        if Login_status:
            f()
        else:
            use_load()
    return inner
#登录验证装饰器
@wrapper
def Article():
    print(f'欢迎{usename[-1]}进入文章页面')
    while 1:
        use_choice = input('''
        请选择
        1写入内容
        2导入md文件
        ''')
        if use_choice == '1'or use_choice == '2':
            break
        print('您的选择出错,请重新选择')
    if use_choice == '1':
        use_write = input('请写入内容,文件名和文件内容用|分开.格式为:'
                          '文件名|内容').strip().split('|')
        with open(f'{use_write[0]}.text',mode='w',encoding='utf-8') as f:
            f.write(use_write[1])
    if use_choice == '2':
        while 1:
            use_file = input('请输入您要导入文件的路径')
            if os.path.isfile(use_file):
                with open(use_file,mode='r',encoding='utf-8') as f,\
                open('linshi.text',mode='w',encoding='utf-8')as f1:
                    for i in f.readlines():
                        f1.write(i)
                os.remove(use_file)
                os.rename('linshi.text',use_file.split('.')[0]+'.text')
                break
            print('您输入的路径不存在,请检查之后重新输入')
# Article()#文章页面函数,未加装饰器.
def comment():
    print(f'欢迎{usename[-1]}进入评论页面')
def diary():
    print(f'欢迎{usename[-1]}进入日记页面')
def Collection():
    print(f'欢迎{usename[-1]}进入日记页面')
@wrapper
def Cancellation():
    global Login_status
    Login_status = False
#注销,需要登录验证
def exit_load():
    exit()
dict = {1:use_load,2:register,3:Article,4:comment,5:diary,6:Collection,7:Cancellation,8:exit_load}

def main_begin():
    while 1:
        use_begin = input(WELCOME).strip()
        # try:
        dict[int(use_begin)]()
        # except Exception:
        #     print('您输入的内容有误')
main_begin()


#__author:wolfs
#date: 2019/7/9