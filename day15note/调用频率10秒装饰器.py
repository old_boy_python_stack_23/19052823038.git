import time
def wrapper(func1):
    li = [1]
    def inner(*args,**kwargs):
        start_time = time.time()
        li.append(start_time)
        if li[-1]-li[-2] > 10:
            ret = func1(*args,**kwargs)
            return ret
        else:
            print( '您调用的太频繁了,请等待10秒再次调用')
    return inner