def wrapper_out(n):
    def wrapper(func):
        def inner(*args,**kwargs):
            # nonlocal n
            f = open(f'{n}',mode='r',encoding='utf-8')
            print(f.read(6))
            ret = func(*args,**kwargs)
            return ret
        return inner
    return wrapper

@wrapper_out('qq')
def qq():
    print('成功登陆QQ')

qq()


@wrapper_out('douyin')
def douyin():
    print('成功登陆抖音')

douyin()