''''''
'''
请实现一个装饰器，限制该函数被调用的频率，如10秒一次（借助于time模块，time.time()）（面试题,有点难度，可先做其他）
'''
# import time
# def wrapper(func1):
#     li = [1]
#     def inner(*args,**kwargs):
#         start_time = time.time()
#         li.append(start_time)
#         if li[-1]-li[-2] > 10:
#             ret = func1(*args,**kwargs)
#             return ret
#         else:
#             print( '您调用的太频繁了,请等待10秒再次调用')
#     return inner
# @wrapper
# def func_user():
#     print('我是被装饰的函数')
# func_user()
# time.sleep(10)
# func_user()
'''
请写出下列代码片段的输出结果：
def say_hi(func):
    def wrapper(*args,**kwargs):
        print("HI")
        ret=func(*args,**kwargs)
        print("BYE")
        return ret
    return wrapper
def say_yo(func):
    def wrapper(*args,**kwargs):
        print("Yo")
        return func(*args,**kwargs)
    return wrapper
@say_hi
@say_yo
def func():
    print("ROCK&ROLL")
func()
'''
# def say_hi(func):
#     def wrapper(*args,**kwargs):
#         print("HI")
#         ret=func(*args,**kwargs)
#         print("BYE")
#         return ret
#     return wrapper
# def say_yo(func):
#     def wrapper(*args,**kwargs):
#         print("Yo")
#         return func(*args,**kwargs)
#     return wrapper
# @say_hi
# @say_yo
# def func():
#     print("ROCK&ROLL")
# func()
# HI
# Yo
# ROCK&ROLL
# BYE

'''编写装饰器完成下列需求:
用户有两套账号密码,一套为京东账号密码，一套为淘宝账号密码分别保存在两个文件中。
设置四个函数，分别代表 京东首页，京东超市，淘宝首页，淘宝超市。
启动程序后,呈现用户的选项为:
​	1,京东首页
​	2,京东超市
​	3,淘宝首页
​	4,淘宝超市
​	5,退出程序
四个函数都加上认证功能，用户可任意选择,用户选择京东超市或者京东首页,只要输入一次京东账号和密码并成功,则这两个函数都可以任意访问;用户选择淘宝超市或者淘宝首页,只要输入一次淘宝账号和密码并成功,则这两个函数都可以任意访问.
相关提示：用带参数的装饰器。装饰器内部加入判断，验证不同的账户密码。
'''
# dic_land = {'jd':True,'tb':True}
# def wrapper_out(n):
#     def wrapper(func):
#         global dic_land
#         def inner(*args,**kwargs):
#             while dic_land[f'{n}']:
#                 with open(f'{n}',mode='r',encoding='utf-8') as f:
#                     use_name = input(f'请输入您的{n}账号')
#                     use_pwd = input(f'请输入您的{n}密码')
#                     for i in f.readlines():
#                         if use_name == i.strip().split('|')[0] and use_pwd.strip() == i.strip().split('|')[1]:
#                             dic_land[f'{n}'] = False
#                             ret = func(*args,**kwargs)
#                             return ret
#             else:
#                 ret = func(*args,**kwargs)
#                 return ret
#         return inner
#     return wrapper
#
# @wrapper_out('jd')
# def jd_home_page_land():
#     print('欢迎登陆京东首页')
#
# @wrapper_out('jd')
# def jd_shop_page_land():
#     print('欢迎登陆京东超市')
#
# @wrapper_out('tb')
# def tb_home_page_land():
#     print('欢迎登陆淘宝首页')
#
# @wrapper_out('tb')
# def tb_shop_page_land():
#     print('欢迎登陆淘宝超市')
#
# def land():
#     use_choice_input=input(   '''
#         1,京东首页
#     ​	2,京东超市
#     ​	3,淘宝首页
#     ​	4,淘宝超市
#     ​	5,退出程序''')
#     if use_choice_input.strip()   == '1':
#         jd_home_page_land()
#     elif use_choice_input.strip() == '2':
#         jd_shop_page_land()
#     elif use_choice_input.strip() == '3':
#         tb_home_page_land()
#     elif use_choice_input.strip() == '4':
#         tb_shop_page_land()
#     elif use_choice_input.strip() == '5':
#         exit()
#     else:
#         print('您输入的内容有误,请重试')
#         exit()
#     land()
# land()
'''
用递归函数完成斐波那契数列（面试题）：
斐波那契数列：1，1，2，3，5，8，13，21..........(第三个数为前两个数的和，但是最开始的1，1是特殊情况，可以单独讨论)
用户输入序号获取对应的斐波那契数字：比如输入6，返回的结果为8.
'''
# def Fibonacci(n):
#     if n == 1:
#         return 1
#     elif n == 2:
#         return 1
#     else:
#         return Fibonacci(n-1)+Fibonacci(n-2)
# print(Fibonacci(10))

'''
给l1 = [1,1,2,2,3,3,6,6,5,5,2,2] 去重，不能使用set集合（面试题）。
'''
# import copy
# l1 = [1,1,2,2,3,3,6,6,5,5,2,2]
# # l2 = copy.deepcopy(l1)
# l2 = []
# for i in l1:
#     if i not in l2:
#         l2.append(i)
# l1 = copy.deepcopy(l2)
# print(l1)
