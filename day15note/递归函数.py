# import sys
# print(sys.setrecursionlimit(100000)) 递归次数设定上限为10万.实际执行到4054次中断, 受电脑配置影响.
# count = 1
# def func():
#     global count
#     print(count)
#     count += 1
#     func()
# func()  #默认递归深度是1000,但执行起来是998次.

li = [1,2,3,[4,5,6,[7,8,9,[10,11,12,[13,14,15,[16,17,18,[19,20,21],22,[23,24,25],26,27],28],29],30],31],32]
def func(n):
    for i in n:
        if type(i)==list:
            func(i)
        else:
            print(i)
func(li)