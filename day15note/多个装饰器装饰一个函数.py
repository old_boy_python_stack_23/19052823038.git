def wrapper1(func):
    def inner1(*args,**kwargs):
        print('我是装饰器1函数执行前加的内容')
        ret1 = func()
        print('我是装饰器1函数执行后加的内容')
        return ret1
    return inner1

def wrapper2(func):
    def inner2(*args,**kwargs):
        print('我是装饰器2函数执行前加的内容')
        ret2 = func()
        print('我是装饰器2函数执行后加的内容')
        return ret2
    return inner2

@wrapper1
@wrapper2
def func1():
    print('我是被装饰的函数')
func1()

