''''''
'''
简答题：

面向对象的三大特性是什么？

答:封装,继承,多态

什么是面向对象的新式类？什么是经典类？

答:新式类和经典类区别主要在一对多继承并多重继承时,的调用顺序.
经典类调用顺序遵循深度优先原则,父类由左至右,查到头像上级返回.
新式类调用顺序符合rom调用规则.

面向对象为什么要有继承？继承的好处是什么？

答:一个类可以直接沿用另一个类的所有功能,并从这些功能和属性中挑选使用.非常方便.
好处:节约代码,增加耦合性

面向对象中super的作用。
super的作用是,在子类与父类存在相同方法时,直接加载自己的方法,
并通过super来加载父类方法.达到一起使用的效果

代码题(通过具体代码完成下列要求)：

class A:
    def func(self):
        print('in A')

class B:
    def func(self):
        print('in B')

class C(A,B):
    def func(self):
        print('in C')
​	可以改动上上面代码，完成下列需求：对C类实例化一个对象产生一个c1，然后c1.func()

​	1. 让其执行C类中的func

​	2. 让其执行A类中的func

​	3. 让其执行B类中的func

​	4. 让其既执行C类中的func，又执行A类中的func

​	5. 让让其既执行C类中的func，又执行B类中的func
'''
# class A:
#     def func1(self):
#         print('in A')
# class B:
#     def func2(self):
#         print('in B')
# class C(A,B):
#     def func3(self):
#         print('in C')
#     def func4(self):
#         self.func3()
#         super().func1()
#     def func5(self):
#         self.func3()
#         super().func2()
#
# c1 = C()
# # ​1. 让其执行C类中的func
# c1.func3()
# #2. 让其执行A类中的func
# c1.func1()
# # ​	3. 让其执行B类中的func
# c1.func2()
# # ​	4. 让其既执行C类中的func，又执行A类中的func
# c1.func4()
# # ​	5. 让让其既执行C类中的func，又执行B类中的func
# c1.func5()
'''
下面代码执行结果是什么？为什么？
class Parent:
    def func(self):
        print('in Parent func')
    def __init__(self):
        self.func()
class Son(Parent):
    def func(self):
        print('in Son func')
son1 = Son()
# in Son func

class A:
    name = []
p1 = A()
p2 = A()
p1.name.append(1)
# p1.name，p2.name，A.name 分别是什么？
p1.age = 12
# p1.age，p2.age，A.age 分别又是什么？为什么？
'''
#
# class A:
#     name = []
# p1 = A()
# p2 = A()
# p1.name.append(1)
# # p1.name，p2.name，A.name 分别是什么？
# print(p1.name)
# print(p2.name)
# print(A.name)
# # [1] [1] [1]
# p1.age = 12
# # p1.age，p2.age，A.age 分别又是什么？为什么？
# print(p1.age)
# print(p2.age)
# print(A.age)
# 12
# 报错
# 报错
'''
写出下列代码执行结果：
class Base1:
    def f1(self):
        print(**'base1.f1'**)
    def f2(self):
        print(**'base1.f2'**)
    def f3(self):
        print(**'base1.f3'**)
        self.f1()
class Base2:
    def f1(self):
        print(**'base2.f1'**)
class Foo(Base1, Base2):
    def f0(self):
        print(**'foo.f0'**)
        self.f3()
obj = Foo()
obj.f0()
'''
# class Base1:
#     def f1(self):
#         print('base1.f1')
#     def f2(self):
#         print('base1.f2')
#     def f3(self):
#         print('base1.f3')
#         self.f1()
# class Base2:
#     def f1(self):
#         print('base2.f1')
# class Foo(Base1, Base2):
#     def f0(self):
#         print('foo.f0')
#         self.f3()
# obj = Foo()
# obj.f0()
#foo.f0
# base1.f3
# base1.f1
#
'''
看代码写结果：
class Parent:
    x = 1
class Child1 (Parent):
	pass
class Child2(Parent):
	pass
print(Parent.x,Child1.x,Child2.x)
Child2.x = 2
print(Parent.x,Child1.x,Child2.x) 
Child1.x = 3
print(Parent.x,Child1.x,Child2.x)
'''
# class Parent:
#     x = 1
# class Child1 (Parent):
#     pass
# class Child2(Parent):
#     pass
# print(Parent.x,Child1.x,Child2.x) #1 1 1
# Child2.x = 2
# print(Parent.x,Child1.x,Child2.x)#1 1 2
# Child1.x = 3
# print(Parent.x,Child1.x,Child2.x)#1 3 2


'''
有如下类：
class A:
    pass
class B(A):
    pass
class C(A):
    pass
class D(A):
    pass
class E(B,C):
    pass
class F(C,D):
    pass
class G(D):
    pass
class H(E,F):
    pass
class I(F,G):
    pass
class K(H,I):
    pass
如果这是经典类，请写出他的继承顺序。
如果这是新式类，请写出他的继承顺序，并写出具体过程。
'''
#答:经典类排序:K,H,E,B,A,C,F,D,I,G
#新式类排序K,H,E,B,I,F,C,G,D,A
#具体过程参考图片流程图,按照拓扑排序,找入点为0的,从左至右开始切并排序.
