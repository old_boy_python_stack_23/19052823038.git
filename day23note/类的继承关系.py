class Animal:
    def __init__(self, name, age, sex):
        self.name = name
        self.age = age
        self.sex = sex
    def func1(self):
        print(f'{self.name}调用了Animal类的func1')
    s1 = '我是Animal的属性1'
    def eat(self):
        print('人类不要吃屎')
class Person(Animal):
    def __init__(self,name,age,sex):
        # Animal.__init__(self,name, age, sex)
        # super(Person, self).__init__(name,age,sex)
        super().__init__(name,age,sex)
        self.name = name
        # self.self = self
    s1 = '我是Person里的属性1'
    def eat(self):
        print('人类需要吃饭')
        super().eat()
    pass
class Dog:
    def __init__(self,name,age,sex):
        self.name = name
        self.age = age
        self.sex = sex
class Cat:
    def __init__(self,name,age,sex):
        self.name = name
        self.age = age
        self.sex = sex
zmm = Person('zm',4,'男')   #一定会执行init,本类没有,找父类,父类没有就找object
print(zmm.__dict__)
zmm.func1()
print(zmm.s1)
zmm.eat()

