# import requests
# from concurrent.futures import  ProcessPoolExecutor
# import os
# import time
# import random
# time_start = time.time()
# def get(url):
#     response = requests.get(url)
#     print(f'{os.getpid()}正在爬取:{url}')
#     # time.sleep(random.randint(1,3))
#     if response.status_code == 200:
#         return response.text
#
# def parse(text):
#     print(f'{os.getpid()} 分析结果:{len(text)}')
#
# if __name__ == '__main__':
#     url_list = [
#         'http://www.taobao.com',
#         'http://www.JD.com',
#         'http://www.JD.com',
#         'http://www.JD.com',
#         'http://www.baidu.com',
#         'https://www.cnblogs.com/jin-xin/articles/11232151.html',
#         'https://www.cnblogs.com/jin-xin/articles/10078845.html',
#         'http://www.sina.com.cn',
#         'https://www.sohu.com',
#         'https://www.youku.com',]
#     pool = ProcessPoolExecutor(4)
#     obj_list = []
#     for url in url_list:
#         obj = pool.submit(get,url)
#         obj_list.append(obj)
#
#     pool.shutdown(wait=True)
#
#     for obj in obj_list:
#         parse(obj.result())
# print(time.time()-time_start)   #1.256


# import requests
# from concurrent.futures import  ProcessPoolExecutor
# import os
# import time
# import random
# time_start = time.time()
# def get(url):
#     response = requests.get(url)
#     print(f'{os.getpid()}正在爬取:{url}')
#     # time.sleep(random.randint(1,3))
#     if response.status_code == 200:
#         parse(response.text)
#
# def parse(text):
#     print(f'{os.getpid()} 分析结果:{len(text)}')
#
# if __name__ == '__main__':
#     url_list = [
#         'http://www.taobao.com',
#         'http://www.JD.com',
#         'http://www.JD.com',
#         'http://www.JD.com',
#         'http://www.baidu.com',
#         'https://www.cnblogs.com/jin-xin/articles/11232151.html',
#         'https://www.cnblogs.com/jin-xin/articles/10078845.html',
#         'http://www.sina.com.cn',
#         'https://www.sohu.com',
#         'https://www.youku.com',]
#     pool = ProcessPoolExecutor(4)
#     # obj_list = []
#     for url in url_list:
#         obj = pool.submit(get,url)
#         # obj_list.append(obj)
#
#     pool.shutdown(wait=True)
#
#     # for obj in obj_list:
#     #     parse(obj.result())
# print(time.time()-time_start)   #2.477

import requests
from concurrent.futures import ProcessPoolExecutor
import os
import time
import random
time_start = time.time()
def get(url):
    response = requests.get(url)
    print(f'{os.getpid()}正在爬取:{url}')
    # time.sleep(random.randint(1,3))
    if response.status_code == 200:
        return response.text

def parse(obj):
    print(f'{os.getpid()} 分析结果:{len(obj.result())}')

if __name__ == '__main__':
    url_list = [
        'http://www.taobao.com',
        'http://www.JD.com',
        'http://www.JD.com',
        'http://www.JD.com',
        'http://www.baidu.com',
        'https://www.cnblogs.com/jin-xin/articles/11232151.html',
        'https://www.cnblogs.com/jin-xin/articles/10078845.html',
        'http://www.sina.com.cn',
        'https://www.sohu.com',
        'https://www.youku.com',]
    pool = ProcessPoolExecutor(4)
    # obj_list = []
    for url in url_list:
        obj = pool.submit(get,url)
        # obj_list.append(obj)
        obj.add_done_callback(parse)

    pool.shutdown(wait=True)

    # for obj in obj_list:
    #     parse(obj.result())
print(time.time()-time_start)   #2.477

