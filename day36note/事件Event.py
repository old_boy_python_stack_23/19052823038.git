from threading import Thread
from threading import current_thread
from threading import Event
import time
event = Event()

def task():
    print(f'{current_thread().name}')
    time.sleep(1)
    event.set()
    print(f'检测')
def task1():
    print(f'{current_thread().name}正在尝试连接服务器')
    event.wait(1) #循环检测event是否为True,当为True继续
    print(f'{current_thread().name}连接成功')
if __name__ == '__main__':
    t1 = Thread(target=task1,)
    t2 = Thread(target=task1,)
    t3 = Thread(target=task1,)

    t = Thread(target=task)
    t.start()
    t1.start()
    t2.start()
    t3.start()