# # 1 FIFO queue   先进先出
# import queue
#
# q = queue.Queue(3)
# q.put(1)
# q.put(2)
# q.put('太白')
# print(q.get())
# print(q.get())
# print(q.get())
# q.put(666)
# print(q.get())
#
# # LIFO 堆栈, 先进后出
# q = queue.LifoQueue()
# q.put(1)
# q.put(2)
# q.put('太白')
# print(q.get())
# print(q.get())
# print(q.get())

#优先级队列   从小到大,
import queue
q = queue.PriorityQueue(3)
q.put((10,'垃圾消息'))   #放进去的时候以元组形式,第一个元素是优先级顺序.
q.put((-9,'紧急消息'))   #第二个元素是数据.  顺序越低越优先
q.put((3,'一般消息'))
print(q.get())
print(q.get())
print(q.get())

