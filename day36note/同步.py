from concurrent.futures import ProcessPoolExecutor
import os
import time
import random
def task():
    print(f'{os.getpid()} is running')
    time.sleep(1)
    return  f'{os.getpid()} is finish'

if __name__ == '__main__':
    p = ProcessPoolExecutor(4)

    for i in range(10):
        obj = p.submit(task,)
        print(obj.result())

