from concurrent.futures import ProcessPoolExecutor
import os
import time
import random
def task():
    print(f'{os.getpid()} is running')
    time.sleep(random.randint(0,2))
    return  f'{os.getpid()} is finish'

if __name__ == '__main__':
    p = ProcessPoolExecutor(4)
    obj_l1 = []
    for i in range(10):
        obj = p.submit(task,)
        obj_l1.append(obj)

    p.shutdown(wait=True)

    for i in obj_l1:
        print(i.result())