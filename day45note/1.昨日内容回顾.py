# 内容回顾
    # 块级标签
        # p上面一行空白 # div普通块 #hr分割线
    # 列表
        # ul  li
            # type: # disc square circle none
        # ol
            # type : a  A  I  1  i   start:开始位置
        # dl
            # dt标题  dd内容
    # 表格
        #table
            # thead 表头,可不要. tr 表示行  th 表示cell列
            # tbody : tr 表示行  td表示cell内容.
                # border 边框的线宽 cellpadding和内容的剧里
                # cellspacing和外边框的剧里
                # colspan 合并单元格,占几列
                # rowspan 合并单元格,占几行
    # 表单
         # form action='url' 提交目的地
            # input
                # type:# text,password,radio,checkbox,submit,button,reset,
                # hidden,file,date
                # name\value
                # radic,checkbox : 菜单式勾选选项.checked,默认
                # text,password:readonly,可修改不可提交
                # disabled,不能修改也不能提交
                # text:placeholder 提示语句
            # button 按钮,放input里默认是提交.
            # selected   下拉是选条
                # 默认单选 size 设置多选 multiple
                # option 应该有name属性和value属性
            # lable : 和input text/password 设置了id,for =""鼠标点击光标跳转
            # textarea: 文本框  rows  cols设置大小
# css
    # css 的引入方式
        # 行内样式  style= "color:red;"
        # 内接样式  <style>
            # div{color:red}</style>
        # 外接样式
            # 外连式
                # <link href = 'xxx.css'>
            # 导入式
                #<style>
                # @import url('地址')
                #</style>
    # 基础样式
        # width 宽
        # height 高
        # background - color 背景色
        # color 填充色
    # 选择器
        # 标签选择器: a div p span
        # 类选择器: .class名
        # id选择器,对应#id名
        # 通用选择器: *
    # 高级选择器
        # 后代选择器 div p a表示div标签下的所有的p标签下的所有的a标签.
        # 子代选择器 div>p 表示div标签下的所有第一层的p
        # 毗邻选择器 div+p  表示div之后的下一个p
        # 弟弟选择器 div~p  找div以下的所有p弟弟.
        # 属性选择器 div[title]  div[title = '']含有title属性的div.前者优先.
        # or并集选择器 div,ul,li
        # and交集选择器 div.class.class1

        # 伪类选择器
            # a:active 鼠标按下,link连接未访问的样式,visited访问后样式
            # input:focus 输入框获得焦点时的样式
            # hover 通用鼠标悬浮
        # 伪元素选择器
            # first-letter 第一个字母的样式
            # before 前缀 content
            # after 后缀
        #