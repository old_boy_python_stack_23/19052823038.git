# css 选择器优先级
    # !important,提高优先级到最高;
    # 作用范围越小,优先级越高.
    # 同优先级,后覆盖前.
    # 行内 > id > 类 > 标签 > 继承
    # 1000   100  10    1       0
    # 同级别内,可累加优先度
# 颜色表示
    # rgb : red green blue 光谱三原色
        # rgb(255,255,255) = 白色
        # rgb(0,0,0) = 黑色
        # rgba(0,0,0,0-1)  第四位是0到1.0完全透明,1不透明.0.5半透明.
    # 16进制,颜色符号.
        # 每两位一个16进制数,上方三原色发转换 前两位红色,中两位绿色,后两位蓝色
        # #000000白色 #FFFFFF黑色
        # #000-#FFF简写
# 字体
    # font-family "宋体"
    # font - weight: 100-900范围;
    # font - weight: lighter变细?,bold加粗
    # font - size: 20px; (默认16px)
# 文本
    # text - align:center(左右居中),right(右对齐),lift(左对齐)
    # text - decoration: 划线设置
        #line-through(中划线)
        # overlinge(上划线)
        # underline(下划线)
        # none(没有线)
    # text - indent
        # text-indent:32xp
        # text-indent:2em(1em是一个字体的大小)
        # 首行缩进 text-align:
        # line-height 行间距
            # 可以通过这个实现上下居中.
        # text-shadow 阴影,
        # 文字溢出效果 : 省略号
# 背景图片
    # 参考背景图片html
# 边框的设置
    # 参考边框html
# 块和行内的概念\转换
    # display: block块  inline行内  inline-boock行内块   none无属性 inline行内
    # display: block;
        # width: 100px
        # height: 100px
        # background - color : red
        # line-height:100px
        # text-aligh: center;
    # display:block;   独占一行并且可以设置宽高
    # display:inline -block
# 盒模型
# 浮动
# 定位
# z-index
# opacity