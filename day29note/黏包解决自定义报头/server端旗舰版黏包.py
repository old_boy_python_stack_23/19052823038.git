import socket
import subprocess
import struct
sk = socket.socket()
sk.bind(('127.0.0.1',8080))
sk.listen()

conn,addr = sk.accept()
print(addr)
while 1:
    ret_addr = conn.recv(1024)
    obj = subprocess.Popen(ret_addr.decode('gbk'),
                           shell=True,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
    result = obj.stdout.read()+obj.stderr.read()
    print(result.decode('gbk'))
    len_result = len(result)
    send_size = struct.pack('i',len_result)
    conn.send(send_size)
    conn.send(result)
conn.close()
sk.close()

