import socket
import struct
#怎么交给操作系统.
#socket是内置模块,负责将pycharm里写好的东西交给Socket.又它交给操作系统.
phone = socket.socket()
#1创建socket对象,可以默认不写

phone.connect(('127.0.0.1',8848))  #连接服务器ip地址与端口
#2:绑定ip地址和端口(办卡)
while 1:
    tu_server = input('>>>').strip()
    phone.send(tu_server.encode('gbk'))
    #3发送消息
    head_bytes = phone.recv(4)
    print(struct.unpack('i',head_bytes))
    total_size = struct.unpack('i',head_bytes)[0]
    total_data = b''
    #接受消息
    while len(total_data)<total_size:
        total_data += phone.recv(1024)


    print(f'来自服务端的消息:{total_data.decode("gbk")}')

#4. 等电话,接受连接

phone.close()
