#并发
#进程
    #特点:
        #开销大 数据隔离 数据不安全 可以利用多核
        # 操作系统级别 资源分配的最小的单位
    #线程
        # 特点:
            #开销小 数据共享 数据不安全 可以利用多核
            #操作系统级别 能被CPU调度的最小单位
    # 协程
        # 特点:
            # 开销小 数据共享 数据安全 不能利用多核 用户级别

# a = 1
# def func():
#携程切换
#     global a
#     a =    进程和线程可能在这种位置切换,所以不安全.a+1
# import dis
# dis.dis(func)
#上面代码运行结果,下面的内容都是CPU指令.
# 14           0 LOAD_GLOBAL              0 (a)
#               2 LOAD_CONST               1 (1)
#               4 BINARY_ADD
#               6 STORE_GLOBAL             0 (a)
#               8 LOAD_CONST               0 (None)
#              10 RETURN_VALUE

#开多进程 + 多线程 +协程

#线程
    # GIL 锁 Cpython解释旗下 导致了多个线程不能同时利用多核
    # 计算密集型的场景中,线程的效率会大打折扣.
    # 大多数情况,都是IO密集型.很少有计算密集型. 多线程和协程就够用了

# 模块
    # multiprocessing
    # threading
    # concurrent.futrues
    # gevent  基于greenlet写的,greenlet基于C. 第三方模块
    # asyncio 底层模块,协程. 内置   .基于yield关键字完成的,是基于python的原生模块.
        # sanic框架  轻量级异步框架. 跟flashk差不多,支持异步.
        # aiohttp  爬虫框架.

#锁
    # 互斥锁
    # 递归锁
    # 死锁现象
    #