# 查看哪些数据库
# mysql>show databases;

# ftp 项目 创建库和删库
# create database ftp; 创建了一个名为ftp的库
# 切换到这个库下 use ftp;
# 创建表
# create table 表名(id int,name char(12))

# 查看表结构
# desc userinfo;
# describe userinfo; 上下一样,缩写和简写的区别
# show create table userinfo;

# 删除表
# drop table userinfo;

# 修改表
# alter table userinfo(旧表名) rename user(新表名);

#查看 show tables;

# 查看
    # select * from user;表名
# 增加
    # insert into user values (1,'alex');
    # insert into user values (2,'wusir'),(3,'太亮')
# 修改
# update user set name = 'barry'; #不用
# update user set name = 'barry' where id = 1;
# update user set name = 'barry',id = 2 where id =2;

#删除
# delete from user; #清空表,少用
# delete from user where id = 3; #删除对应id的值,其他的值都不变.

# select user,host from mysql.user; #查看当前用户可以通过哪些方式登陆.

