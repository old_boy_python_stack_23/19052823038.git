# server端
# client端
# 卸载
    # 在cmd里进入mysql,如果能进入,说明server端在开启.
    # 进入之后,输入 net stop mysql通过这个命令停止这个服务的工作.从系统管理的服务也可以停用
    # mysqld remove 删除这个服务.
    # 把安装软件的文件内容删掉.
    # 删除环境变量
    # 清除注册表/ 或重启计算机
# 安装
    # 1.路径不能有中文
    # 2.路径中不能有特殊字符,\数字,\t,\b,\n等.
    # 3.修改配置文件my.ini
        # 1. 编码utf-8
        # 2. 所有的配置项后不要有特殊字符
        # 3. 修改两个路径basedir datadir
    # 检测文件的扩展名设置
    # 配置环境变量在path中添加mysql文件的bin目录.
    # 管理员身份重开cmd
        #mysqld install安装成功
    #启动mysql
        # net start mysql 启动mysql server
#启动mysql
    # net start mysql 启动mysql server启动了mysql的服务端
    # 在cmd启动mysql的客户端
        # mysql
        # 客户端和本地的mysql server相连.

#mysql server 端
    # net start mysql 启动一个mysql的服务
    # net stop mysql 停止一个服务
# mysql 启动了客户端,客户端会自动的连接本地的3306端口.

# select user()
# 启动时 mysql -uroot 表示用户root用户登陆.
# 默认密码是空,设置密码.
# set password = password('')
# mysql -uroot -p 回车 带密码登陆
    # Enter password :123 登陆成功,这里密码是加密文.
    # mysql -ruoot -p 123 这里密码是明文

#创建账号
    # 使用公司的数据库,管理员给一个账号
    # 自己的数据库,借给别人用,也可以创建一个账号
    # create user 'eva'@'192.168.13.%'百分号代表可变,这个ip的人才能登陆.identified by '123'才能登陆
    # mysql -ueva -p123

    # grant 权限的种类(增删改查)
    # grant all
    # grant select on 数据库.* to 'eva'@'192.168.13.%';
                        # 给一个已经存在的eva账号授权
    # grant select on 数据库.* to 'eva'@'192.168.13.%' identified by '123';

    # mysql>select user()

