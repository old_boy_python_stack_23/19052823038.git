# js的基础语法
    # Math
        # floor
        # ceil
        # max
        # min
        # random
        # abs
        # round
    # Date
        # 创建Date对象 new Date() 得到当前时间的对象
        # 对象.tolocalstring() '2019/8/14 8:41'
        # 对象.getFullYear() 2019 年
        # 对象.getMonth() 0-11  月
        # 对象.getDate() 月中天 1-31
        # 对象.getDay()  0-6
        # 对象.getHours() 小时
        # 对象.getMinutes() 分
        # 对象.getSeconds() 秒
        # 自己创建任意时间对象
            # new Date('
    # RegExp
# 面向对象
    # function 类名(参数){
#           this.属性名 = 值}
# 类名.prototype.方法名 = function(参数){this表示self}
#    var  obj = new 类名(参数)
#    obj.方法名(参数)
#    obj.属性名

    # DOM
        # dom树
        # 查找节点
        # 操作节点
        # 文本操作
        # 属性操作
            # class
            # value