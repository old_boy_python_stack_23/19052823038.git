'''
1.写代码，有如下列表，按照要求实现每一个功能
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
'''

#li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
#1)计算列表的长度并输出
#print(len(li))

#2)列表中追加元素"seven", 并输出添加后的列表
# li.append('seven')
# print(li)

#3)请在列表的第1个位置插入元素"Tony", 并输出添加后的列表
# li.insert(0,'Tony')
# print(li)

#4)请修改列表第2个位置的元素为"Kelly", 并输出修改后的列表
# li[1]='Kelly'
# print(li)

#5)请将列表l2 = [1, "a", 3, 4, "heart"]的每一个元素添加到列表li中，一行代码实现，不允许循环添加。
# l2 = [1, "a", 3, 4, "heart"]
# li = li + l2
# print(li)

#6)请将字符串s = "qwert"的每一个元素添加到列表li中，一行代码实现，不允许循环添加。
#li.extend("qwert")

#7)请删除列表中的元素"ritian", 并输出删除后的列表
# li.remove("ritian")
# print(li)

#8)请删除列表中的第2个元素，并输出删除的元素和删除元素后的列表
# n = li.pop(1)
# print(li)
# print(n)

#9)请删除列表中的第2至4个元素，并输出删除元素后的列表
# del(li[1:4])
# print(li)

#10)请将列表所有得元素反转，并输出反转后的列表

# count = 1
# lifan = []
# while count<= len(li):
#     lifan.append(li.pop())
# print(lifan)

#11)请计算出"alex"元素在列表li中出现的次数，并输出该次数。

# print(li.count("alex"))

'''
2.
写代码，有如下列表，利用切片实现每一个功能

li = [1, 3, 2, "a", 4, "b", 5, "c"]
1)通过对li列表的切片形成新的列表l1, l1 = [1, 3, 2]
2)通过对li列表的切片形成新的列表l2, l2 = ["a", 4, "b"]
3)通过对li列表的切片形成新的列表l3, l3 = ["1,2,4,5]
                            4)通过对li列表的切片形成新的列表l4, l4 = [3, "a", "b"]
5)通过对li列表的切片形成新的列表l5, l5 = ["c"]
6)通过对li列表的切片形成新的列表l6, l6 = ["b", "a", 3]

'''
#li = [1, 3, 2, "a", 4, "b", 5, "c"]
# 1)通过对li列表的切片形成新的列表l1, l1 = [1, 3, 2]
# l1 = li[0:3]
# print(l1)

# 2)通过对li列表的切片形成新的列表l2, l2 = ["a", 4, "b"]
# l2 = li [3:6]
# print(l2)

# 3)通过对li列表的切片形成新的列表l3, l3 = ["1,2,4,5]
# l3 = li[::2]
# print(l3)

#4)通过对li列表的切片形成新的列表l4, l4 = [3, "a", "b"]
# l4 = li[1:-2:2]
# print(l4)

#5)通过对li列表的切片形成新的列表l5, l5 = ["c"]
# l5 = [li[-1]]
# print(l5)


# 6)通过对li列表的切片形成新的列表l6, l6 = ["b", "a", 3]

# l6 = li[-3::-2]
# print(l6)


'''
3.
写代码，有如下列表，按照要求实现每一个功能。

lis = [2, 4, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
1)将列表lis中的
"tt"
变成大写（用两种方式）。

2)将列表中的数字3变成字符串
"100"（用两种方式）。

3)将列表中的字符串
"1"
变成数字101（用两种方式）。


'''
#lis = [2, 4, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]

# 1)将列表lis中的
# "tt"
# 变成大写（用两种方式）。
# lis[3][2][1][0] = 'TT'
# print(lis)


# 2)将列表中的数字3变成字符串
# "100"（用两种方式）。
# lis[3][2][1][1]='100'
# print(lis)


# 3)将列表中的字符串 "1" 变成数字101（用两种方式）。
# lis[3][2][1][2]=101
# print(lis)

'''
4.请用代码实现
li = ["alex", "wusir", "taibai"]
利用下划线将列表的每一个元素拼接成字符串
"alex_wusir_taibai"
'''
# li = ["alex", "wusir", "taibai"]
# n = ''
# for c in li:
#     n = n+c+'_'
# n = n.rstrip('_')
# print(n)

'''
5.
利用for循环和range打印出下面列表的索引
li = ["alex", "WuSir", "ritian", "barry", "wenzho"]
'''
# li = ["alex", "WuSir", "ritian", "barry", "wenzho"]
# s = range(0,5)
# for c in s :
#     print (li[c],':',c,sep="")

'''
6.利用while循环打印出下面列表的索引
li = ["alex", "WuSir", "ritian", "barry", "wenzho"]
'''
# li = ["alex", "WuSir", "ritian", "barry", "wenzho"]
# count = 0
# while count < len(li):
#     print(li[count],':',count,sep='')
#     count += 1

#7.利用for循环和range找出100以内所有的偶数并将这些偶数添加到一个新列表中。
# s = range(100)
# s1 = list(s)
# li = []
# for i in s1:
#     if i%2 == 0:
#         li.append(i)
# print(li)

#8.利用for循环和range找出50以内能被3整除的数，并将这些数插入到一个新列表中。
# s = range(50)
# s1 = list(s)
# li = []
# for i in s1:
#     if i%3 == 0:
#         li.append(i)
# print(li)

#9.利用for循环和range从100~1，倒序打印。
# s = range(1,101)
# s1 = list(s)
# for i in s1:
#     n = len(s1)-i
#     print(s1[n])

#10.利用for循环和range打印100~10，倒序将所有的偶数添加到一个新列表中，然后在对列表的元素进行筛选，将能被4整除的数留下来。
# s = range(10,101)
# s1 = list(s)
# c = range(len(s))
# c1 = list(c)
# li = []
# for i in c1:
#     n = len(c)-i-1
#     print(s1[n])
#     if s1[n]%2 ==0:
#         li.append(s1[n])
# for i in li:
#     if i%4 != 0:
#         li.remove(i)
# print (li)


#11.利用for循环和range，将1 - 30的数字依次添加到一个列表中，并循环这个列表，将能被3整除的数改成 *。
# s = range(1,31)
# li = []
# for i in s:
#     li.append(i)
# for i in li:
#     if i%3 == 0:
#         li[i-1]='*'
# print(li)

'''
12.
查找列表li中的元素，移除每个元素的空格，并找出以
"A"
或者
"a"
开头，并以
"c"
结尾的所有元素，并添加到一个新列表中, 最后循环打印这个新列表。
li = ["TaiBai ", "alexC", "AbC ", "egon", " riTiAn", "WuSir", " aqc"]
'''
# li = ["TaiBai ", "alexC", "AbC ", "egon", " riTiAn", "WuSir", " aqc"]
# s = range(len(li))
# l1 = []
# for i in s:
#     li[i] = li[i].replace(' ','')
# for i in s:
#     if li[i].startswith('A') or li[i].startswith('a') and li[i].endswith("c"):#??????or放前面，and放后面找不出来。
#         l1.append(li[i])
# for i in l1:
#     print(i)

'''
13.
开发敏感词语过滤程序，提示用户输入评论内容，如果用户输入的内容中包含特殊的字符：
敏感词列表
li = ["苍老师", "东京热", "武藤兰", "波多野结衣"]
则将用户输入的内容中的敏感词汇替换成等长度的 *（苍老师就替换 ** *），并添加到一个列表中；如果用户输入的内容没有敏感词汇，则直接添加到上述的列表中。
'''
# #苍老师是大家的启蒙老师，东京很热，武藤兰是个好同志，波多野结衣简称波姐
# use_input = input("请输入：")
# li = ["苍老师", "东京热", "武藤兰", "波多野结衣"]
# s = range(len(use_input))
# for i in li:
#     if i in use_input:
#         use_input = use_input.replace(i,"*" * len(i))
# use_input =[use_input]
# print(use_input)
# print(type(use_input))
#


'''

14.
有如下列表（选做题）
li = [1, 3, 4, "alex", [3, 7, 8, "BaoYuan"], 5, "RiTiAn"]
循环打印列表中的每个元素，遇到列表则再循环打印出它里面的元素。
我想要的结果是：
1
3
4
alex
3
7
8
baoyuan
5
ritian

'''
# li = [1, 3, 4, "alex", [3, 7, 8, "BaoYuan"], 5, "RiTiAn"]
# for i in li:
#     if type(i)==list:
#         for l in i:
#             print (l.lower()) if type(l) ==str else print (l)
#     else:
#         print(i.lower()) if type(i) ==str else print(i)
