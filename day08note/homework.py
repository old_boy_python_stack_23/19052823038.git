'''
1.有如下文件，a1.txt，里面的内容为：
老男孩是最好的学校，
全心全意为学生服务，
只为学生未来，不为牟利。
我说的都是真的。哈哈
分别完成以下的功能：
a,将原文件全部读出来并打印。
b,在原文件后面追加一行内容：信不信由你，反正我信了。
c,将原文件全部读出来，并在后面添加一行内容：信不信由你，反正我信了。
d,将原文件全部清空，换成下面的内容：
每天坚持一点，
每天努力一点，
每天多思考一点，
慢慢你会发现，
你的进步越来越大。
'''
# f = open('al.txt',mode = "w",encoding = "utf-8")
# f.write("""老男孩是最好的学校，
# 全心全意为学生服务，
# 只为学生未来，不为牟利。
# 我说的都是真的。哈哈""")
# f.close()
# f = open('al.txt',mode='r',encoding="utf-8")
# print(f.read())
# f = open('al.txt',mode='a',encoding="utf-8")
# f.write("信不信由你,反正我信了")
# f = open('al.txt',mode='r+',encoding="utf-8")
# print(f.read())
# f.write("信不信由你,反正我信了.")
# f.close()
# f = open('al.txt',mode='w',encoding="utf-8")
# f.write('''每天坚持一点，
# 每天努力一点，
# 每天多思考一点，
# 慢慢你会发现，
# 你的进步越来越大。''')
# f.close()

'''2.有如下文件，t1.txt,里面的内容为：
葫芦娃，葫芦娃，
一根藤上七个瓜
风吹雨打，都不怕，
啦啦啦啦。
我可以算命，而且算的特别准:
上面的内容你肯定是心里默唱出来的，对不对？哈哈
分别完成下面的功能：
a,以r的模式打开原文件，利用for循环遍历文件句柄。
b,以r的模式打开原文件，以readlines()方法读取出来，
并循环遍历 readlines(),并分析b,与c 有什么区别？深入理解文件句柄与 readlines()结果的区别。
c,以r模式读取‘葫芦娃，’前四个字符。
d,以r模式读取第一行内容，并去除此行前后的空格，制表符，换行符。
e,以a+模式打开文件，先追加一行：‘老男孩教育’然后在从最开始将 原内容全部读取出来。
'''
# f = open("t1.txt",mode='r',encoding="utf-8")
# for i in f:
#     print(i)
# f.close()
# f = open("t1.txt",mode='r',encoding="utf-8")
# for i in f.readlines():
#     print(i)
# f.close()
# a题for循环使用句柄,一次读一行.
# b题是readlines()先把所有内容读取,并把每一行内容作为一个字符串元素放入一个列表中.for循环遍历这个列表,读取每个元素.
# f = open("t1.txt", mode='r', encoding="utf-8")
# print(f.read(4))
# f.close()
# f = open("t1.txt", mode='r', encoding="utf-8")
# st1 = f.readline()
# st2 = st1.strip()
# print(st2)
# print()
# f = open("t1.txt", mode='a+', encoding="utf-8")
# f.write('\n老男孩教育')
# f.seek(0.0)
# print(f.read())
# f.close()

'''3.文件a.txt内容：每一行内容分别为商品名字，价钱，个数。
apple 10 3
tesla 100000 1
mac 3000 2
lenovo 30000 3
chicken 10 3
通过代码，将其构建成这种数据类型：
[{'name':'apple','price':10,'amount':3},
{'name':'tesla','price':1000000,'amount':1}......] 
并计算出总价钱。
'''
# f = open("a.txt",mode="r",encoding="utf-8")
# list_price = []
# for i in f:
#     li = i.strip().split(' ')
#     dic1 = {'name':li[0],'price':li[1],'amount':li[2]}
#     list_price.append(dic1)
# f.close()
# print(list_price)
# price_num = 0
# for i in list_price:
#     price_num += int(i['price']) * int(i['amount'])
# print('总价格是',price_num)

'''4.有如下文件：
alex是老男孩python发起人，创建人。
alex其实是人妖。
谁说alex是sb？
你们真逗，alex再牛逼，也掩饰不住资深屌丝的气质。
将文件中所有的alex都替换成大写的SB（文件的改的操作）。
'''
# f = open("alex.txt",mode="w",encoding="utf-8")
# f.write('''alex是老男孩python发起人，创建人。
# alex其实是人妖。
# 谁说alex是sb？
# 你们真逗，alex再牛逼，也掩饰不住资深屌丝的气质。''')
# with open("alex.txt",mode="r",encoding="utf-8") as f,\
# open("alex1.txt",mode="w",encoding="utf-8") as f1:
#     for i in f:
#         j = i.replace('alex','SB')
#         f1.write(j)
# import os
# os.rename('alex.txt','alexbeifen.txt')
# os.rename('alex1.txt','alex.txt')

'''5.文件a1.txt内容(升级题)
name:apple price:10 amount:3 year:2012
name:tesla price:100000 amount:1 year:2013
.......
通过代码，将其构建成这种数据类型：
[{'name':'apple','price':10,'amount':3,year:2012},
{'name':'tesla','price':1000000,'amount':1}......]
并计算出总价钱。
'''
# f = open("topic5.txt",mode="r",encoding="utf-8")
# lis_topic5 = []
# dic1 = {}
# price_sum = 0
# import copy
# for i in f:
#     l1 = i.strip().split(' ')
#     for j in l1:
#         keys,values = j.split(':')
#         dic1[keys] = values
#     dic2 = copy.deepcopy(dic1)
#     lis_topic5.append(dic2)
# f.close()
# print(lis_topic5)
# for i in lis_topic5:
#     price_sum += int(i['price']) * int(i['amount'])
# print('总价格是:',price_sum)

'''6.文件a1.txt内容(升级题)
序号 部门 人数 平均年龄 备注
1 python 30 26 单身狗
2 Linux 26 30 没对象
3 运营部 20 24 女生多
.......
通过代码，将其构建成这种数据类型：
[{'序号':'1','部门':Python,'人数':30,'平均年龄':26,'备注':'单身狗'},
......]
'''
# f = open("topic6.txt",mode="r",encoding="utf-8")
# import copy
# dic1 = {}
# li_result = []
# for i in f:
#     if i[0] == '序':
#         li = i.strip().split(' ')
#         continue
#     li1 = i.strip().split(' ')
#     for j in range(len(li)):
#         dic1[li[j]] = li1[j]
#     dic2 = copy.deepcopy(dic1)
#     li_result.append(dic2)
# f.close()
# print(li_result)



