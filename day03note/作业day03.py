#__author:wolfs
#date: 2019/6/5
'''
1.有变量name = " aleX leNb " 完成如下操作：

移除 name 变量对应的值两边的空格,并输出处理结果
将 name变量对应的值中所有的空格去除掉,并输出处理结果
判断 name 变量是否以 "al" 开头,并输出结果（用两种方式 切片+字符串方法）
判断name变量是否以"Nb"结尾,并输出结果（用两种方式 切片+字符串方法）
将 name 变量对应的值中的 所有的"l" 替换为 "p",并输出结果
将name变量对应的值中的第一个"l"替换成"p",并输出结果
将 name 变量对应的值根据 "l" 分割,并输出结果
将 name 变量对应的值全部变大写,并输出结果
将 name 变量对应的值全部变小写,并输出结果
请输出 name 变量对应的值的第 2 个字符?
请输出 name 变量对应的值的前 3 个字符?
请输出 name 变量对应的值的后 2 个字符?'''
# name = " aleX leNb "
# print(name.strip())
# print(name.replace(' ',''))
# print(name.startswith('al'))#是否al开头的字符串命令判断
# print(name[0:2]=='al')#是否是al索引方式
# print(name.endswith('Nb'))#是否以Nb结尾的字符串命令判断
# print(name[-2:]=='Nb')#是否以Nb结尾的索引方式。
# print(name.replace('l','p'))
# print(name.replace('l','p',1))
# print(name.split('l'))
# print(name.upper())
# print(name.lower())
# print(name[1])
# print(name[0:3])
# print(name[-2:])

'''
2.有字符串s = "123a4b5c"

通过对s切片形成新的字符串 "123"

通过对s切片形成新的字符串 "a4b"

通过对s切片形成字符串s5,s5 = "c"

通过对s切片形成字符串s6,s6 = "2ab"

通过对s切片形成字符串s6,s6 = "cba"
'''
# s = '123a4b5c'
# s1 = s[0:3]
# s2 = s[3:6]
# s5 = s[-1]
# s6 = s[1:7:2]
# s7 = s[-1:-6:-2]
# print(s1,s2,s5,s6,s7)

#3.使用while循环打印字符串 s="你好世界" 中每个元素。
# s = '你好世界'
# count = 0
# while count<4:
#     print(s[count])
#     count += 1

#4.使用while循环对s="321"进行循环，打印的内容依次是："倒计时3秒"，"倒计时2秒"，"倒计时1秒"，"出发！"(提示使用字符串方法中的格式化)
# s = "321"
# count = 0
# while count<3:
#     print('倒计时%s秒'%(s[count]))
#     count += 1
# print ('出发！')

#5.使用for循环对s="321"进行循环，打印的内容依次是："倒计时3秒"，"倒计时2秒"，"倒计时1秒"，"出发！"(提示使用字符串方法中的格式化)
# s = '321'
# for i in s:
#     print('倒计时%s秒'%(i))
# print('出发！')


# 6.实现一个整数加法计算器(两个数相加)：
# 如：content = input("请输入内容:") 用户输入：5+9或5+ 9或5 + 9（含空白），然后进行分割转换最终进行整数的计算得到结果。(列表也支持索引)
# content_input = input ("请输入内容:")
# content_input1 = content_input.replace(' ','')
# content_input2 = content_input1.split('+')
# sum = 0
# for i in content_input2:
#     sum += int(i)
# print(sum)


#7.计算用户输入的内容中有几个 s 字符？
#如：content = input("请输入内容：") # 如abcassfsqfsafqsdzacsad
# content = input('请输入内容：')
# count = content.count('s')
# print ('您输入的内容包含%s个s'%(count))

#8.使用while循环分别正向和反向对字符串 message = "伤情最是晚凉天，憔悴厮人不堪言。" 进行打印。
# message = "伤情最是晚凉天，憔悴厮人不堪言。"
# count = 0
# while count< 16:
#     print(message[count],end="")
#     count += 1
# print()
# count = -1
# while count > -17:
#     print(message[count],end="")
#     count -= 1



#9.获取用户输入的内容，并计算前四位"a"出现几次,并输出结果。
# use_input = input("请输入内容")
# use_input1 = use_input[0:4]
# print(use_input1.count('a'))

#10.制作趣味模板程序需求：等待⽤户输⼊名字、地点、爱好，根据⽤户的名字和爱好进⾏任意现实 如：敬爱可亲的xxx，最喜欢在xxx地⽅⼲xxx (字符串格式化)
# name_input = input("请输入您的姓名：")
# address_input = input("请输入您的地址：")
# hobby_input = input("请输入您的爱好：")
# print("敬爱可亲的%s，最喜欢在%s地方干%s"%(name_input,address_input,hobby_input))

#11.判断⼀句话是否是回⽂. 回⽂: 正着念和反着念是⼀样的. 例如, 上海⾃来⽔来⾃海上
# use_input = input("请输入内容：")
# n = ""
# for i in use_input:
#     n = i + n
# if n == use_input:
#     print('是回文')
# else:
#     print('不是回文')

#12.输⼊⼀个字符串，要求判断在这个字符串中⼤写字⺟，⼩写字⺟，数字，其他共出现了多少次，并输出出来
# CAPITAL='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
# lower_ease_letters = CAPITAL.lower()
# NUM ='0123456789'
# use_input = input ("请输入内容：")
# countc = 0
# countl = 0
# countn = 0
# countothr = 0
# for i in use_input:
#     if i in CAPITAL:
#         countc += 1
#     elif i in lower_ease_letters:
#         countl += 1
#     elif i in NUM:
#         countn += 1
#     else:
#         countothr += 1
# print("大写字母%s个，小写字母%s个，数字%s个，其他%s个,合计%s个。"%(countc,countl,countn,countothr,countc+countl+countn+countothr))

'''
13.用户可持续输入（用while循环），用户使用的情况：
输入A，则显示走大路回家，然后在让用户进一步选择：
是选择公交车，还是步行？
选择公交车，显示10分钟到家，并退出整个程序。
选择步行，显示20分钟到家，并退出整个程序。
输入B，则显示走小路回家，并退出整个程序。
输入C，则显示绕道回家，然后在让用户进一步选择：
是选择游戏厅玩会，还是网吧？
选择游戏厅，则显示 ‘一个半小时到家，爸爸在家，拿棍等你。’并让其重新输入A，B,C选项。
选择网吧，则显示‘两个小时到家，妈妈已做好了战斗准备。’并让其重新输入A，B,C选项。
'''
while True:
    choice_road_input = input("请输入A选择大路回家，或输入B选择小路回家，或输入C选择绕路回家")
    if choice_road_input == "A":
        choice_vehicle_input = input("请输入A选择乘坐公交车，或输入B选择步行")
        if choice_vehicle_input == "A":
            print('十分钟到家')
            break
        elif choice_vehicle_input == "B":
            print('二十分钟到家')
            break
    elif choice_road_input == "B":
        print("走小路回家！")
        break
    elif choice_road_input == "C":
        choice_detour_input = input("请输入A选择去游戏厅，选择B去网吧。")
        if choice_detour_input == "A":
            print("一个半小时到家，爸爸在加，拿棍等你")
        elif choice_detour_input == "B":
            print("两个小时到家，妈妈已做好了战斗准备。")


