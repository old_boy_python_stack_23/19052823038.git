# 数据的存储方式 - - 存储引擎 engines
    # 使用不同的存储引擎,数据是以不同的方式存储的.
    # show engines
        # | Engine             | Support | Comment                                                        | Transactions | XA   | Savepoints |
        # +--------------------+---------+----------------------------------------------------------------+--------------+------+------------+
        # | FEDERATED          | NO      | Federated MySQL storage engine                                 | NULL         | NULL | NULL       |
        # | MRG_MYISAM         | YES     | Collection of identical MyISAM tables                          | NO           | NO   | NO         |
        ### | MyISAM             | YES     | MyISAM storage engine                                          | NO           | NO   | NO         |

        # | BLACKHOLE          | YES     | /dev/null storage engine (anything you write to it disappears) | NO           | NO   | NO         |
        # | CSV                | YES     | CSV storage engine                                             | NO           | NO   | NO         |
        ### | MEMORY             | YES     | Hash based, stored in memory, useful for temporary tables      | NO           | NO   | NO         |

        # | ARCHIVE            | YES     | Archive storage engine                                         | NO           | NO   | NO         |

        ##### | InnoDB             | DEFAULT | Supports transactions, row-level locking, and foreign keys     | YES          | YES  | YES        |
        # | PERFORMANCE_SCHEMA | YES     | Performance Schema                                             | NO           | NO   | NO
    # memory
        # hash
        # 存一个文件


    # myisam
        # 5.5以下版本默认.  table-level locking 表级锁
        # 树 tree
        # 存三个文件
            # staff_t.frm #结构
            # staff_t.MYD #data数据
            # staff_t.MYI #index 索引
    # innodb
        # 5.6版本以上默认.
        # Supports transactions, 事物
            # 当一次行为,一连串操作,有开头标志,有结束标志.开头和结束标志以及之间的批量操作,叫做事物
        # row-level locking 行级锁
            # 最细可以到行级别的锁,可以支持表级锁等.
        # foreign keys 外键
            # 两个关联表,其中一个可以做外键约束,当另一个表修改内容会改变关联时,不让改.
        # 树 tree 加速查询
        # 存两个文件
            #库文件里
            # staff.frm 表结构
            # staff.ibd InnoDB data


