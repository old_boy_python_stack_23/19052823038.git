#早晨默写
# 1.创建一个库day38
#create database day38;
# 2.查看有哪些表
#use day38
#show tables;
# 3.创建一个表
#create table staff(id int,name char(12),age int)
# 4.往这个表中插入三条数据
#insert into staff values(1,'a',10),(2,'b',15),(3,'c',20);
# 5.查看表结构
# desc staff;
# 6.查询表中的所有数据
# select * from staff;
# 7.常见的关系型数据库和非关系型数据库
#关系型 mysql oracle
#redis  mongodb






#数据库部分的概念
#数据库管理系统 DBMS 软件
#数据库DB文件库
#表 table 文件
#数据 data
#

# 关系型数据库 存取效率相对第,数据与数据之间的关系紧密
    # mysql  oracle  sqlserver
# 非关系型数据库 存取效率相对高 数据u数据之间的关联关系相对弱
    # redis mongodb memcache

# sql 操作数据库的时候使用的语言

    #DDL: CREATE ALTER DROP             (****)
    #DML: SELECT INSERT DELETE UPDATE   (*****)
    #DCL: GRANT REVOKE                  (*)

# sql语句
    # 库
        #show databases; #查看有哪些库
        #caetae database 库名;  #创建库
        #use 库名;  #进入库
    # 表
        #show tables;   #查询哪些表
        # create table 表名(字段1 类型(长度),字段2)   #创建表
        # desc 表名;    #查看表结构
        #show create table 表名;    #查看表属性
        #alter table 表名 rename 新表名  #修改表名
        #drop table 表名;  #删除表
    # 数据
        # insert into 表名 values(字段1,字段2,字段3),(字段1...)...
        # select * from 表名;#查看所有内容
        # select * from 表名 where 条件字段= 条件
        # update 表名 set 字段名=新值 where 条件字段=条件
        # delete from 表名where 条件字段= 条件