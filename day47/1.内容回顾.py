# js的引入.
    # 直接把语句写在script标签内.
    # 把语句写在js文件中,再在script内引入文件.
    # <script src='xxxx.js'></script>
# js的变成要求
    # 结束符;
    # 单行注释//
    # 多行注释/*多行注释*/
# 变量
    # 声明 var
    # 变量的命名: 数字 字母 下划线,$
# 输入输出
    # 输入\输出
        # alert('弹出警告框)
        # prompt('弹出警告框并有输入内容.有返回值可接受.
        # console.log() ('控制台里打印)
# 数据类型
    # number
        # int
        # float
        # toFixed(2),保留两位小数并四舍五入.
        # inf无穷大
    # string
        # 单引号和双引号都可以.
        # length
        # trim()去空白,concat('str')拼接,charAt(索引)索引取值,indexOf通过元素找索引,
        # slice(start,end)切片,split(sep,返回n个值)
        # toLowerCase(),toUpperCASE().
    # boolean
        # true  空数组[],空字典{}都是TRUE,因为里面有元素叫未定义元素
        # false  undefiend null NaN 0 ""(空字符串)

    # null
        # 设置为空
    # nudefined
        # 未定义 当一个变量只声明,不赋值的时候
#类型转换
    # parseInt('123') 字符串转整形
    # parseFloat('1.234') 字符串转浮点数
    # String(123) 数字转字符串
    # var a = 123
    # a.toString() 数字转字符串
    # Boolean() 转成布尔值
# 内置的对象类型
    # array 数组(列表)
    # var a = [1,2,3,4]
    # var b = new Array() 创建空字符串
    # 属性 length
    # push() pop() shift() unshift()
    # slice() reverse() join() sort()排序 splice(1,2,'新值')切片换新 concat()连接
# 自定义对象
    # 字典 var obj = {'alex':'name'}//对象
# json
    # obj = JSON.parse(json字符串)   # json对象变为自定义对象
    # json字符串 = JSON.stingfy(自定义对象,转换成JSON字符串)
# 运算符
    # 算术运算符 + - * / ** % ++a a++ --a a--
    # 赋值运算符 = += -= *= /= %=
    # 比较运算符 > < >= <= == === != !==
    # 逻辑运算符&&and ||or !非
# 流程控制
    # if(条件){代码块}
    # if(条件){代码块}else{代码块}
    # if(条件){代码块}else if(条件){代码块}else{代码块}
    # switch(参数){
        # case 值1:
            # 代码:
            # break;
        # case 值2:
            # 代码
            # break;
        # case 值3:
            # 代码
            # break
        # default:
            # 代码 以上都不成立执行这里.
# 循环
    # while(条件){循环体}
# for循环
    # for (var i = 0;i<10;i++){循环体}
    # for (i in arr){循环体 arr[i] 值出去}   i取的是索引
    # for (var i=0;i<arr.length;i++){i是索引,arr[i]是具体的值

# 三元运算符
    # var 值 = 条件 ? 条件为true返回的内容: 条件为False返回的内容

# 函数
    # function 函数名(参数){函数体
    # return 返回值 }   # 返回指定额个数必须是一个,如果返回多个值,放在数组中返回.
# 函数名()调用.   参数可以跟定义的对不上.

# arguments 函数中内置的动态参数,可以接受所有的参数.
# 匿名函数
    # var 变量名 = function(参数){函数体   返回值}
# 自调用函数
    # (function(形参){函数体  return 返回值})(实参)


