# 1. js
    # 正则用法
        # RegExp 正则表达式
        # 创建一个 var reg = RegExp('正则表达式') // 注意,写在字符串中所有带\的元字符都会被转译.
        # var reg2 = /\d/ 用两个反斜杠,替换括号和引号,直接定义正则表达式.
        # var exp = 'a1b2c3'
        # reg2.test(exp)   # true
        # ^1[2-9]\d{9}$验证是否是手机号格式

        # 在字符串中应用正则
            # var exp = 'alex3714'
            # exp.match(/\d/) 3
            # exp.match(/\d/g) 3714 g匹配所有
            # exp.match(/a/i)i表示不区分大小写
            # exp.search(/a/i)只找第一个符合值得索引位置.
            # exp.split(/正则表达式/i,n) 根据正则表达式切割并取前n个值
            # exp2.replace(/[aA]/,'sb')i不区分大小写,g表示替换所有.符合情况的直接换成新值
        # 小问题
            # 正则表达式在匹配多个值,多次test会有光标移动并循环的现象.
            # var reg3 = /\w{5,10}/


# Date 对象
    # var
    # var dt2 = new Date('2018/1/1 12:12:12'
    # Mon Jan 01 2018 12:12:12 GMT+0800
    # dt2.getFullYear
    # dt2.getFulleYear()
# math 对象 
    # 面向对象
# 2. BOM
    #操作的是浏览器
# 3. DOM
    # 操作的是网页上的标签