''''''
'''
1.依赖关系
2.组合关系(关联,组合,聚合)
3.继承关系

1依赖关系(主从关系)
大象进冰箱.
将一个类的类名或者对象,传给另一个类的方法种.叫做依赖关系.




class Elephant:
    def __init__(self,name):
        self.name = name

    def open_1(self,ref1):
        print(f'大象{self.name}默念三声:芝麻开门')
        ref1.open_door()
    def close_1(self,ref1):
        print(f'大象{self.name}默念三声:芝麻关门')
        ref1.open_door()

class Refrigerator:
    def __init__(self,name):
        self.name = name

    def open_door(self):
        print(f'{self.name}冰箱门被打开了')


    def close_door(self):
        print(f'{self.name}冰箱门被关上了')

ele = Elephant('奇奇')
ref = Refrigerator('美菱')
ele.open_1(Refrigerator(111))
ele.close_1(ref)
# ref.close_door()




2组合关系(关联,组合,聚合)
将一个对象封装成另一个类的属性.
'''
class Boy:
    def __init__(self,name):
        self.name = name
    def meet(self,girl_friend  = None):
        self.girl_friend = girl_friend

    def have_diner(self):
        if self.girl_friend:
            print(f'{self.name}请{self.girl_friend.age}岁的{self.girl_friend.name}一起吃13块钱的麻辣烫')
            self.girl_friend.shopping(self)
        else:
            print('单身狗也配吃饭?')
class Girl:
    def __init__(self,name,age):
        self.name = name
        self.age = age
    def shopping(self,boy = None):
        if boy == None:
            print(f'{self.name}只能自己去奥特莱斯扫货')
        else:
            print(f'{boy.name}和{self.name}一起去奥特莱斯扫货')
wu = Boy('吴超')
flower = Girl('如花',48)
wu.meet(flower)
wu.have_diner()



#__author:wolfs
#date: 2019/7/8