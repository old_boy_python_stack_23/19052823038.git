#相似功能的函数,可以放在一个类里面.  同样也可以放在一个模块当中.
#类的结构,类名的命名规则,首字母大写,尽量不要下划线,不要数字.最好用驼峰体
class zws:
    def __init__(self,a,b,c):
        self.name = a
        self.age = b
        self.sex = c
    s1 = '123'
    s2 = '345'
    def work(self):
        print(f'{self.name}今天很开心')
zm = zws('张蒙','3','男')
zm.work()
zws.country = '中国'
print(zws.__dict__)
