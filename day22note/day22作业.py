''''''
'''
暴力摩托程序（完成下列需求）：
创建三个游戏人物，分别是：
​	苍井井，女，18，攻击力ad为20，血量200
​	东尼木木，男，20，攻击力ad为30，血量150
​	波多多，女，19，攻击力ad为50，血量80
创建三个游戏武器，分别是：
​ 平底锅，ad为20
​	斧子，ad为50
​	双节棍，ad为65
创建三个游戏摩托车，分别是：
​	小踏板，速度60迈
​ 雅马哈，速度80迈
​ 宝马，速度120迈。
​ 完成下列需求（利用武器打人掉的血量为武器的ad + 人的ad）：
​	（1）苍井井骑着小踏板开着60迈的车行驶在赛道上。
​	（2）东尼木木骑着宝马开着120迈的车行驶在赛道上。
​	（3）波多多骑着雅马哈开着80迈的车行驶在赛道上。
​	（4）苍井井赤手空拳打了波多多20滴血，波多多还剩xx血。
​	（5）东尼木木赤手空拳打了波多多30滴血，波多多还剩xx血。
​	（6）波多多利用平底锅打了苍井井一平底锅，苍井井还剩xx血。
​	（7）波多多利用斧子打了东尼木木一斧子，东尼木木还剩xx血。
​	（8）苍井井骑着宝马打了骑着小踏板的东尼木木一双节棍，东尼木木哭了，还剩xx血。（选做）
​	（9）波多多骑着小踏板打了骑着雅马哈的东尼木木一斧子，东尼木木哭了，还剩xx血。（选做）
'''
class Role:
    def __init__(self,name,sex,age,ad,hp):
        self.name = name
        self.sex = sex
        self.age = age
        self.ad = ad
        self.hp = hp

    def attack(self,role):
        role.hp = role.hp - self.ad
        print(f'{self.name}赤手空拳打了{role.name}{self.ad}滴血,{role.name}还剩{role.hp}血')

class Arms:
    def __init__(self,name,ad,modes):
        self.name = name
        self.ad = ad
        self.modes = modes

    def arms_attack(self,role1,role2):
        role2.hp = role2.hp - role1.ad -self.ad
        print(f'{role1.name}利用{self.name}{self.modes}了{role2.name}一{self.name},{role2.name}还剩{role2.hp}血')

class Motorcycle:
    def __init__(self,name,speed):
        self.name = name
        self.speed = speed

    def ride(self,role):
        print(f'{role.name}骑着{self.name}开着{self.speed}行驶在赛道上')

    def ride_arms_attack(self,role1,arms,role2,moto2):
        role2.hp = role2.hp - role1.ad -arms.ad
        print(f'{role1.name}骑着{self.name}{arms.modes}了骑着{moto2.name}的'
              f'{role2.name}一{arms.name},{role2.name}哭了,还剩{role2.hp}血',sep='')
#苍井井骑着宝马打了骑着小踏板的东尼木木一双节棍，东尼木木哭了，还剩xx血

role1 = Role('苍井井','女',18,20,200)
role2 = Role('东尼木木','男',20,30,150)
role3 = Role('波多多','女',19,50,80)

arms1 = Arms('平底锅',20,'砸')
arms2 = Arms('斧子',50,'砍')
arms3 = Arms('双截棍',65,'抽')

moto1 = Motorcycle('小踏板',60)
moto2 = Motorcycle('雅马哈',80)
moto3 = Motorcycle('宝马',120)
#(1)苍井井骑着小踏板开着60迈的车行驶在赛道上。
moto1.ride(role1)
#(2)东尼木木骑着宝马开着120迈的车行驶在赛道上。
moto3.ride(role2)
#(3)波多多骑着雅马哈开着80迈的车行驶在赛道上。
moto2.ride(role3)
#(4)苍井井赤手空拳打了波多多20滴血，波多多还剩xx血。
role1.attack(role3)
#(5)东尼木木赤手空拳打了波多多30滴血，波多多还剩xx血。
role2.attack(role3)
#(6)波多多利用平底锅打了苍井井一平底锅，苍井井还剩xx血。
arms1.arms_attack(role3,role1)
#(7)波多多利用斧子打了东尼木木一斧子，东尼木木还剩xx血。
arms2.arms_attack(role3,role2)
#(8)苍井井骑着宝马打了骑着小踏板的东尼木木一双节棍，东尼木木哭了，还剩xx血。（选做）
moto3.ride_arms_attack(role1,arms3,role2,moto1)
#(9)波多多骑着小踏板打了骑着雅马哈的东尼木木一斧子，东尼木木哭了，还剩xx血。（选做）
moto1.ride_arms_attack(role3,arms2,role2,moto2)
'''
定义一个类，计算圆的周长和面积。
'''
# class circular:
#     Circumference = 3.1415926
#
#     def __init__(self,radius):
#         self.radius = radius
#
#     def Perimeter(self):
#         print(f'这个圆的周长是{circular.Circumference*2*self.radius}')
#
#     def area(self):
#         print(f'这个圆的面积是{circular.Circumference*self.radius**2}')
# l1 = circular(5)
# l1.Perimeter()
# l1.area()


'''
定义一个圆环类，计算圆环的周长和面积（升级题）。
'''
# class Ring:
#     circumference = 3.1415926
#
#     def __init__(self,within_radius,abroad_radius):
#         self.within_radius = within_radius
#         self.abroad_radius = abroad_radius
#
#     def Perimeter(self):
#         print(f'这个圆环的内圈周长是{Ring.circumference*2*self.within_radius},外圈周长是{Ring.circumference*2*self.abroad_radius}')
#
#     def area(self):
#         print(f'这个圆环的面积是{Ring.circumference*self.abroad_radius**2-Ring.circumference*self.within_radius**2}')
# ring1 = Ring(3,4)
# ring1.Perimeter()
# ring1.area()
