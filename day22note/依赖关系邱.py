class Person:
    def play(self,tools):
        tools.run()
        print('很开心,我能玩游戏了')
class Computer():
    def run(self):
        print('电脑开机,正常运行')
class Phone():
    def run(self):
        print('手机开机,正常运行')
c= Computer()
phone = Phone()
p= Person()
p.play(phone)


class Plant:
    def __init__(self,name,ad,hp):
        self.name = name
        self.ad = ad
        self.hp = hp
    def attack(self,js):
        print("植物攻击僵尸")
        js.hp -= self.ad
        print(f'僵尸掉了{self.ad}血,还剩{js.hp}血')

class Jiangshi:
    def __init__(self,name,ad,hp):
        self.name = name
        self.ad = ad
        self.hp = hp
    def attack(self,zw):
        print("僵尸咬植物")
        zw.hp -= self.ad
        print(f'植物掉了{self.ad}血,还剩{zw.hp}血')

wd = Plant('歪脖子豌豆',10,20)
js = Jiangshi('铁桶僵尸',1,200)
wd.attack(js)
wd.attack(js)
js.attack(wd)
js.attack(wd)
#依赖关系是类之间关系最轻的,一个类调用了另一个类的内容.