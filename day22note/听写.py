#实例化对象发生的三件事
#答,加载类到内存,将__init__里的属性封装到对象当中.对象开辟单独的空间,此空间可以单独添加自己的内容,此内容可以与类里面的不同
#定义一个老师类,两属性,两方法.方法1备课pass,方法2讲课.**老师*性别讲了*课  实例化一个老师,调用方法2.
class Teacher:
    def __init__(self,a,b,c):
        self.name = a
        self.sex = b
        self.kecheng = c
    sx1 = "首先得是个人"
    sx2 = '然后是个老师'
    def beike(self):
        pass
    def jiangke(self):
        print(f'{self.name}老师,性别{self.sex},讲了{self.kecheng}课')
taibai = Teacher('太白','男','python')
taibai.jiangke()