class Boy:
    def __init__(self,name,girlFriend=None):
        self.girlFriend = girlFriend
        self.name = name
    def eat(self):
        if self.girlFriend:
            print(f'{self.name}带着他的女友{self.girlFriend.name}去看电影le')
        else:
            print('单身狗,看什么看,滚去学习')

class Girl:
    def __init__(self,name):
        self.name = name
g = Girl('孙艺珍')
b = Boy('爆狼')
g2 = Girl('梁咏琪')
b.eat()
b.girlFriend = g   #一对一关联.可有可无?  不比依赖重啊.
b.eat()
b.girlFriend = g2
b.eat()
