''''''
'''
整理今天的笔记以及课上代码，完善昨天没有写完的作业，先把上午留的装饰器认证登录的完成。
将课上模拟博客园登录的装饰器的认证的代码完善好，整清楚。
看代码写结果：
def wrapper(f):
    def inner(*args,**kwargs):
        print(111)
        ret = f(*args,**kwargs)
        print(222)
        return ret
    return inner
def func():
    print(333)
print(444)
func()
print(555)
'''
# 444
# 333
# 555


'''编写装饰器,在每次执行被装饰函数之前打印一句’每次执行被装饰函数之前都得先经过这里,这里根据需求添加代码’。

'''
# def func():
#     pass
# def wrapper(f):
#     def inner():
#         print('每次执行装饰函数之前都得先经过这里,这里根据需求添加代码')
#         f()
#     return inner()
# func = wrapper(func)

'''
为函数写一个装饰器，把函数的返回值 +100 然后再返回。
def func():
    return 7
result = func()
print(result)
'''
# def func():
#     return 7
# result = func()
# print(result)
# def wrapper(f):
#     def inner():
#         f()
#         return f()+100
#     return inner
# func = wrapper(func)
# print(func())
'''
请实现一个装饰器，通过一次调用是函数重复执行5次。
'''
# def func():
#     print('吃一个包子')
# def wrapper(f):
#     def inner():
#         f()
#         f()
#         f()
#         f()
#         f()
#     return inner
# func = wrapper(func)
# func()
'''
请实现一个装饰器，每次调用函数时，将函数名以及调用此函数的时间节点写入文件中。
可用代码：
import time
struct_time = time.localtime()
print(time.strftime("%Y-%m-%d %H:%M:%S",struct_time)) # 当前时间节点
def wrapper():
    pass
def func1(f):
    print(f.__name__)
func1(wrapper)
函数名通过： 函数名.__name__获取。
'''

