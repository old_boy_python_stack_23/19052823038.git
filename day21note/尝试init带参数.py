class Student:
    study = '学习'
    cloth = '校服'
    examination = '考试'
    def __init__(self,name,age,edu):   #双下划线方法.特殊函数
        self.name = name
        self.sex = age
        self.education = edu
    def Roll_call(self,x):
        self.dianming = x
        print('类里面的函数,至少要有一个位置参数?')
    def use_name(self):
        self.add = '新增'
        print(self.name)
        print('第二个函数')

obj = Student('zws','男','本科')  #实例化,用Student类,实例化出一个对象,也叫一个实例.并复制给变量obj.
# obj1 = Student()  #实例化的时候,会执行类里面__init__函数,而且没有传参数.
# print(obj)
# print(obj1)
print(obj.__dict__)
obj.use_name()
print(obj.__dict__)
obj.Roll_call('请假')
print(obj.__dict__)
# a = obj.__dict__
# print(a)
# a.clear()
# print(a)
print(obj.__dict__)

