class Student:
    study = '学习'
    cloth = '校服'
    examination = '考试'
    def __init__(self):   #双下划线方法.特殊函数
        self.n = 'zws'
        self.sex = '男'
    def Roll_call(self):
        print('类里面的函数,至少要有一个位置参数?')
    def use_name(self):
        print('第二个函数')

obj = Student()  #实例化,用Student类,实例化出一个对象,也叫一个实力.并复制给变量obj.
# obj1 = Student()  #实例化的时候,会执行类里面__init__函数,而且没有传参数.
# print(obj)
# print(obj1)
print(obj.__dict__)
obj.xueli = '本科' #增
print(obj.__dict__)
obj.xueli = '硕士' #改
print(obj.__dict__)
del obj.xueli   #删
print(obj.__dict__) #查
print(obj.sex)  #查

