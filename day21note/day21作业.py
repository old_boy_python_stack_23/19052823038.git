''''''
'''
完成下列功能:
创建一个人类Person,再类中创建3个静态变量(静态字段)
animal = '高级动物'
soul = '有灵魂'
language = '语言'
在类中定义三个方法,吃饭,睡觉,工作.
在此类中的__init__方法中,给对象封装5个属性:国家,姓名,性别,年龄, 身高.
实例化四个人类对象:
第一个人类对象p1属性为:中国,alex,未知,42,175.
第二个人类对象p2属性为:美国,武大,男,35,160.
第三个人类对象p3属性为:你自己定义.
第四个人类对象p4属性为:p1的国籍,p2的名字,p3的性别,p2的年龄,p3 的身高.
通过p1对象执行吃饭方法,方法里面打印:alex在吃饭.
通过p2对象执行吃饭方法,方法里面打印:武大在吃饭.
通过p3对象执行吃饭方法,方法里面打印:(p3对象自己的名字)在吃饭.
通过p1对象找到Person的静态变量 animal
通过p2对象找到Person的静态变量 soul
通过p3对象找到Person的静态变量 language
# '''
# class Person:
#     animal = '高级动物'
#     soul = '有灵魂'
#     language = '语言'
#     def __init__(self,country_self,name_self,sex_self,age_self,height_self):
#         self.country = country_self
#         self.name = name_self
#         self.sex = sex_self
#         self.age = age_self
#         self.height = height_self
#     def eat(self):
#         print(f'{self.name}在吃饭')
#     def sleep(self):
#         print(f'{self.name}在睡觉')
#     def work(self):
#         print(f'{self.name}在工作')
#
# p1 = Person('中国','alex','未知',42,175)
# p2 = Person('美国','武大','男',35,160)
# p3 = Person('火星','太白','男亦可女亦可',88,150)
# p4 = Person('中国','武大','男亦可女亦可',35,150)
#
# p1.eat()
# p2.eat()
# p3.eat()
# print(p1.animal)
# print(p2.soul)
# print(p3.language)






'''

通过自己创建类,实例化对象
在终端输出如下信息
小明，10岁，男，上山去砍柴
小明，10岁，男，开车去东北
小明，10岁，男，最爱大保健
老李，90岁，男，上山去砍柴
老李，90岁，男，开车去东北
老李，90岁，男，最爱大保健
老张…
'''
# class Human:
#     def __init__(self,name_use,age_use,sex_use):
#         self.name = name_use
#         self.age = age_use
#         self.sex = sex_use
#     def hobby1(self):
#         print(f'{self.name},{self.age}岁,{self.sex},上山去砍柴')
#     def hobby2(self):
#         print(f'{self.name},{self.age}岁,{self.sex},开车去东北')
#     def hobby3(self):
#         print(f'{self.name},{self.age}岁,{self.sex},最爱大保健')
# xiaoming = Human('小明',10,'男')
# laoli = Human('老李',90,'男')
# laozhang = Human('老张',33,'男')
# xiaoming.hobby1()
# xiaoming.hobby2()
# xiaoming.hobby3()
# laoli.hobby1()
# laoli.hobby2()
# laoli.hobby3()
# laozhang.hobby1()
# laozhang.hobby2()
# laozhang.hobby3()



'''
设计一个汽车类。
要求：
汽车的公共属性为：动力驱动，具有四个或以上车轮，主要用途载运人员或货物。
汽车的功能：run,transfer.
具体的汽车的不同属性：颜色，车牌，类型（越野，轿车，货车等）。
'''
# class car:
#     power_drive = '四轮驱动'
#     purpose = '载人,拉货'
#     def transfer(self):
#         print('我是汽车的功能,转移...')
#     def run(self):
#         print('我是汽车的功能,跑')
#     def __init__(self,colour_car,brand_car,type_car):
#         self.colour = colour_car
#         self.buand = brand_car
#         self.car_type =type_car
# my_car = car('白色','福特','轿车')
# print(my_car.__dict__)

'''
模拟英雄联盟写一个游戏人物的类（升级题）.
要求:
创建一个 Game_role的类.
构造方法中给对象封装name,ad(攻击力),hp(血量).三个属性.
创建一个attack方法,此方法是实例化两个对象,互相攻击的功能:
例: 实例化一个对象 盖伦,ad为10, hp为100
实例化另个一个对象 剑豪 ad为20, hp为80
盖伦通过attack方法攻击剑豪,此方法要完成 '谁攻击谁,谁掉了多少血, 还剩多少血'的提示功能.
'''
# class Game_role:
#     def __init__(self,name_role,ad_role,hp_role):
#         self.name = name_role
#         self.ad = ad_role
#         self.hp = hp_role
#     def attack(self,self1):
#         self_hp = self1.hp - self.ad
#         if self_hp > 0:
#             print(f'{self.name}攻击了{self1.name},{self1.name}掉了{self.ad}血,还剩{self_hp}血')
#         else:
#             print(f'{self.name}攻击了{self1.name},{self1.name}掉了{self1.hp}血,{self1.name}死了')
#
# Galen = Game_role('盖伦',500,1000)
# Asia_rope = Game_role('剑豪',50,100)
# Galen.attack(Asia_rope)
# Asia_rope.attack(Galen)



