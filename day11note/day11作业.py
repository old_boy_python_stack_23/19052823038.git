''''''
'''day11作业
请写出下列代码的执行结果：﻿例一：
def func1():
	print('in func1')
def func2():
	print('in func2')
ret = func1
ret()#in func1 
ret1 = func2
ret1()#in func2
ret2 = ret
ret3 = ret2
ret2()in func1 
ret3()in func1 
执行结果：
​'''
# in func1
# in func2
# in func1
# in func1
'''	例二：
def func1():
	print('in func1')
def func2():
	print('in func2')
def func3(x,y):
	x()
	print('in func3')
	y()
print(111)
func3(func2,func1)
print(222)
执行结果：
'''
# 111
# in func2
# in func3
# in func1
# 222
'''
例三（选做题）：
def func1():
	print('in func1')
def func2(x):
	print('in func2')
	return x
def func3(y):
	print('in func3')
	return y
ret = func2(func1)
ret()
ret2 = func3(func2) 
ret3 = ret2(func1) #func3(func2(func1))
ret3()
执行结果：
'''
# in func2
# in ufnc1
# in func3
# in func2
# in func1

'''
看代码写结果：
def func(arg):
    return arg.replace('苍老师', '***')
def run():
    msg = "Alex的女朋友苍老师和大家都是好朋友"
    result = func(msg)
    print(result)
run()
def func(arg):
    return arg.replace('苍老师', '***')
def run():
    msg = "Alex的女朋友苍老师和大家都是好朋友"
    result = func(msg)
    print(result)
data = run()
print(data)
'''
# Alex的女朋友***和大家都是好朋友
# Alex的女朋友***和大家都是好朋友
# None
'''
看代码写结果：
DATA_LIST = []
def func(arg):
    return DATA_LIST.insert(0, arg)
data = func('绕不死你')
print(data)
print(DATA_LIST)
None # DATA_LIST.insert(0, arg)这句话是个操作,虽然操作了,但没有返回值,所以是None.  
['绕不死你']  #这个列表因为函数调用后,竟然添加了内容.这也是始料未及.函数内调用全局可变参数时,可变内容会发生编辑动作.

看代码写结果：
def func():
    print('你好呀')
    return '好你妹呀'
func_list = [func, func, func]
for item in func_list:
    val = item()
    print(val)
'''
# 你好呀
# 好你妹呀
# 你好呀
# 好你妹呀
# 你好呀
# 好你妹呀
'''
看代码写结果：
def func():
    print('你好呀')
    return '好你妹呀'
func_list = [func, func, func]
for i in range(len(func_list)):
    val = func_list[i]()
    print(val)
'''
# 你好呀
# 好你妹呀
# 你好呀
# 好你妹呀
# 你好呀
# 好你妹呀
'''
看代码写结果：
def func():
    return '烧饼'
def bar():
    return '豆饼'
def base(a1, a2):
    return a1() + a2()
result = base(func, bar)
print(result)
'''
#烧饼豆饼

'''
看代码写结果：
for item in range(10):
    print(item)
print(item)
0
1
2
3
4
5
6
7
8
9
9

看代码写结果：
def func():
    for item in range(10):
        pass
    print(item)
func()
'''
# 9
'''
看代码写结果：
item = '老男孩'
def func():
    item = 'alex'
    def inner():
        print(item)
    for item in range(10):
        pass
    inner()
func()
#9
看代码写结果：
l1 = []
def func(args):
    l1.append(args)
    return l1
print(func(1))
print(func(2))
print(func(3))
# [1]
# [1,2]
# [1,2,3]
看代码写结果：
name = '太白'
def func():
    global name
    name = '男神'
print(name)
func()
print(name)
# 太白
# 男神

看代码写结果：
name = '太白'
def func():
    print(name)
func()
# 太白
看代码写结果：
name = '太白'
def func():
    print(name)
    name = 'alex'
func()
报错,全局变量在函数里可以直接用,但用之后就不能再改了.   没用之前,可以自定义一个新的.   
看代码写结果：
def func():
    count = 1
    def inner():
        nonlocal count
        count += 1
        print(count)
    print(count)
    inner()
    print(count)
func()
# 1
# 2
# 2
看代码写结果：
def extendList(val,list=[]):
	list.append(val)
	return list

list1 = extendList(10)
[10]
list2 = extendList(123,[])
[123]
list3 = extendList('a')
['a']
print('list1=%s'%list1)
print('list2=%s'%list2)
print('list3=%s'%list3)
list1=[10, 'a']  #函数参数的默认值参数,如果是可变的.这个默认值是可编辑状态,
且每次变化都一直跟着变,切记.其他的重新赋值对它也不影响,
下次如果调用默认值,依然是调用过的状态.
list2=[123]
list3=[10, 'a']
看代码写结果：
def extendList(val,list=[]):
	list.append(val)
	return list
print('list1=%s'% extendList(10))
print('list2=%s'% extendList(123,[]))
print('list3=%s'% extendList('a'))
list1 = [10]
list2 = [123]
list3 = [10,a]





用你的理解解释一下什么是可迭代对象，什么是迭代器。
可迭代对象:这个对象内部的值,可以一个一个拿出来用.但迭代使用时,
是从头挨个用到尾.如果只用一部分,那没用到的部分也过了一遍,比较浪费内存.
迭代器:内部的值,可以一个一个拿来用,并且真的是一个一个用. 
 比较节约内存,但想用后面的,前面需要铺垫,比较麻烦.
如何判断该对象是否是可迭代对象或者迭代器？
iter in dir(对象),如果这个对象的dir里面有iter选项,就是可迭代对象.  有next就是迭代器.
写代码：用while循环模拟for内部的循环机制（面试题）。
'''
# li = [1,2,3,4,5]#用while循环模拟for循环打印这个列表的内容.
# count = 0
# while count < len(li):
#     i = li[count]  #这里相当用for  i  in  li  的第一次循环.
#     print(i)
#     count += 1





'''写函数，传入n个数，返回字典{‘max’:最大值,’min’:最小值}﻿
﻿例如:min_max(2,5,7,8,4) 返回:{‘max’:8,’min’:2}(此题用到max(),min()内置函数)
'''
# def max_num(*args):
#     return {'max':max(args),'min':min(args)}
# print(max_num(3,4,5,6,7))

'''
写函数，传入一个参数n，返回n的阶乘﻿
﻿例如:cal(7) 计算7654321
'''
# def jiecheng(n):
#     m = 1
#     while n > 1:
#         m *= n
#         n -= 1
#     return m
# s = jiecheng(9)
# print(s)
'''
写函数，返回一个扑克牌列表，里面有52项，每一项是一个元组(选做题)﻿
﻿例如：[(‘红心’，2),(‘草花’，2), …(‘黑桃’，‘A’)]
'''
def puke(a,b,li=[]):
    li.append((a,b))
    return li
for i in ["红心","草花","方块","黑桃"]:
    for j in ['A',2,3,4,5,6,7,8,9,10,'J','Q','K']:
        pu_ke = puke(i,j)
print(pu_ke)







'''
写代码完成99乘法表.(选做题，面试题)
1 * 1 = 1
2 * 1 = 2 2 * 2 = 4
3 * 1 = 3 3 * 2 = 6 3 * 3 = 9
......
9 * 1 = 9 9 * 2 = 18 9 * 3 = 27 9 * 4 = 36 9 * 5 = 45 9 * 6 = 54 9 * 7 = 63 9 * 8 = 72 9 * 9 = 81
'''
# m = 1
# n = 1
# while m<=9:
#     while n<=m:
#         print(str(m)+"*"+str(n)+"="+str(m*n),end="\t")
#         n+=1
#     m+=1
#     n=1
#     print()