# try :
#     # l1 = [1,2,3]
#     # print(l1[3])
#
#     # dic = {1:2,3:4}
#     # print(dic[5])
#
#     num = int(input('请输入序号'))
# except IndexError as e:
#     print(e)
# except KeyError as e:
#     print('没有此键')
# except ValueError as e:
#     print('出现了转化错误')
#
# print('程序虽然有错,但我跳过去执行了这里')
# dic ={
#     1:111,2:222,3:333
# }
# while 1:
#     try :
#         num = int(input('请输入序号'))
#         print(dic[num])
#         break
#     except IndexError as e:
#         print(e)
#     except KeyError as e:
#         print('超出可选范围')
#     except ValueError as e:
#         print('请输入数字')
# print('正常结束')
#需要引流的时候,要把错误类型写具体.

# try:
#     num = int(input('请输入序号'))
#     print(dic[num])
# except Exception as e:
#     print(e)
# # else:  #如果没有异常,就执行else语句后面的.
# #     print(345)
# # print('123')
# finally:   #发生错误之前,一定会先执行这里.然后报错.
#     print('我是finally')


# def func():
#     try:
#         a = 1
#         b = 2
#         return a+b
#     finally:
#         print(666)
# func()

# try:
#     s1 = 'a'
#     s2 = int(s1)
# finally:
#     exit()

# #自定义异常
# class Connection(BaseException):
#     def __init__(self,msg):
#         self.msg = msg
# raise Connection('触发了链接异常')