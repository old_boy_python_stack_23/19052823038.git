# #类方法
# class A:
#     num = 1
#     def func(self):
#         print('实例方法')
#     @classmethod
#     def a_func(cls):
#         print(cls.num)
#         cls.num = 2
#         print(cls.num)
# obj = A()
# obj.a_func()
# print(obj.num)#对象可以调用类方法,但cls参数依然是类名.
# class Student:
#     __count = 0
#     def __init__(self,name):
#         self.name = name
#         Student.count_A()
#     @classmethod
#     def count_S(cls):
#         print(f'一共通过{cls}实例化了{cls.__count}个对象')
#     @classmethod
#     def count_A(cls):
#         cls.__count += 1
# s1 = Student('李业')
# s2 = Student('骚强')
# s3 = Student('怼怼')
# Student.count_S()
import time
class A:
    @staticmethod
    def showTime():
        return time.strftime('%H:%M:%S',time.localtime())

showTime()