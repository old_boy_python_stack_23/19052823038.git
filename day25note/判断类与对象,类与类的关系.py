class A:
    pass
class B(A):
    pass
class C(B,A):
    pass
class D:
    pass
a = B()
print(isinstance(a,A))#判断对象和类的关系,是否有血缘关系
print(isinstance(a,B))
print(isinstance(a,C))
print(isinstance(a,D))
print(issubclass(D,A))#判断前面的类是否是后面的类的子孙
print(issubclass(B,A))
print(issubclass(C,A))

print(type(a))
s1 = 'sfdkjakl'

from collections import Iterable
from collections import Iterator

print(isinstance(s1,Iterable))#返回True ,Iterable这个类是各种数据类型类的基类.

print(type(s1)) #判断s1是从哪个类实例化出来的.  str是一个类

print(type(type))
print(type(B))
print(type(Iterable))
# print(type(abc.ABCMeta))
print(type(list))
print(type(object))
