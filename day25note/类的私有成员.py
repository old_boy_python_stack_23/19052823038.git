#类的私有属性
# class A:
#     __s1 = '我是A类的私有属性,只有内部可以调用'
#     def func1(self):
#         print(self.__s1)
#
# class B(A):
#     def func2(self):
#         print(self.__s1)
# aaa = A()
# print(aaa.func1())
# class A:
# #     def __init__(self):
# #         self.__s1 = '我是A类的对象的私有属性,只有内部可以调用'
# #     def func1(self):
# #         print(self.__s1)
# #
# # class B(A):
# #     def func2(self):
# #         print(self.__s1)
# # aaa = A()
# # bbb = B()
# # aaa.func1()
# # print(aaa.__s1)
# # # print(bbb.func2())

class A:
    def __func1(self):
        print('我是A类的私有方法')
    def func3(self):
        self.__func1()
class B(A):
    def func2(self):
        A._A__func1(self)
aaa = A()
bbb = B()
aaa.func3()
bbb.func2()
# print(bbb.func2())