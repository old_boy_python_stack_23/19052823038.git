# class Bmi:
#     def __init__(self,name,height,weight):
#         self.name = name
#         self.height = height
#         self.weight = weight
#     def bmi(self):
#         return self.weight/self.height**2
# tb = Bmi('太白',1.75,80)
# print(tb.bmi())

# class Bmi:
#     def __init__(self,name,height,weight):
#         self.name = name
#         self.height = height
#         self.weight = weight
#     @property
#     def bmi(self):
#         return self.weight/self.height**2   #把方法伪装成属性,调用属性直接执行.
# tb = Bmi('太白',1.75,80)
# print(tb.bmi)
#
# class A:
#     @property
#     def aaa(self):
#         print('aaa大爷我又出来了')
#     @aaa.setter
#     def aaa(self,v):
#         print(f'{v}你修改不了我')
#     @aaa.deleter
#     def aaa(self):
#         print('s.b,你也删除不了我')
# obj = A()
# obj.aaa
# obj.aaa = 'dsb'
# del obj.aaa
# obj.aaa
# obj.aaa = 'dsb'
# del obj.aaa


class B:
    def get_bbb(self):
        print('aaa大爷我又出来了')

    def set_bbb(self,v):
        print(f'{v}你修改不了我')

    def delete_bbb(self):
        print('s.b,你也删除不了我')

    BBB= property(get_bbb,set_bbb,delete_bbb)   #property三连一个效果.而且原函数还在.
f1 = B()
f1.BBB
f1.BBB='a'
del f1.BBB
