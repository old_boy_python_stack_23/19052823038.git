''''''
'''
看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
class Foo(object):
    a1 = 1
    def __init__(self,num):
        self.num = num
    def show_data(self):
        print(self.num+self.a1)
obj1 = Foo(666)
obj2 = Foo(999)
print(obj1.num)
print(obj1.a1)
obj1.num = 18
obj1.a1 = 99
print(obj1.num)
print(obj1.a1)
print(obj2.a1)
print(obj2.num)
print(obj2.num)
print(Foo.a1)
print(obj1.a1)
'''
# class Foo(object):
#     a1 = 1
#     def __init__(self,num):
#         self.num = num
#     def show_data(self):
#         print(self.num+self.a1)
# obj1 = Foo(666)
# obj2 = Foo(999)
# print(obj1.num) #666
# print(obj1.a1) #1
# obj1.num = 18
# obj1.a1 = 99
# print(obj1.num) #18
# print(obj1.a1) #99
# print(obj2.a1) #1
# print(obj2.num) #999
# print(obj2.num) #999
# print(Foo.a1) # 1
# print(obj1.a1) #99
'''
看代码写结果，注意返回值。
class Foo(object):
    def f1(self):
        return 999
    def f2(self):
        v = self.f1()
        print('f2')
        return v
    def f3(self):
        print('f3')
        return self.f2()
    def run(self):
        result = self.f3()
        print(result)
obj = Foo()
v1 = obj.run()
print(v1)
'''
# class Foo(object):
#     def f1(self):
#         return 999
#     def f2(self):
#         v = self.f1()
#         print('f2')
#         return v
#     def f3(self):
#         print('f3')
#         return self.f2()
#     def run(self):
#         result = self.f3()
#         print(result)
# obj = Foo()
# v1 = obj.run()
# #f3
# #f2
# #999
# print(v1)
# #None
'''
看代码写结果
class Foo(object):
    def __init__(self, num):
        self.num = num
v1 = [Foo for i in range(10)]
v2 = [Foo(5) for i in range(10)]
v3 = [Foo(i) for i in range(10)]
print(v1)
print(v2)
print(v3)
'''
# class Foo(object):
#     def __init__(self, num):
#         self.num = num
# v1 = [Foo for i in range(10)]
# v2 = [Foo(5) for i in range(10)]
# v3 = [Foo(i) for i in range(10)]
# print(v1)
# #10个类的地址
# print(v2)
# #10个实例化对象的地址
# print(v3)
# #10个实例化对象的地址
'''
看代码写结果
class StarkConfig(object):
    def __init__(self, num):
        self.num = num
    def changelist(self, request):
        print(self.num, request)
config_obj_list = [StarkConfig(1), StarkConfig(2), StarkConfig(3)]
for item in config_obj_list:
    print(item.num)    
'''
# class StarkConfig(object):
#     def __init__(self, num):
#         self.num = num
#     def changelist(self, request):
#         print(self.num, request)
# config_obj_list = [StarkConfig(1), StarkConfig(2), StarkConfig(3)]
# for item in config_obj_list:
#     print(item.num)
#1
#2
#3



'''
看代码写结果：
class StarkConfig(object):
    def __init__(self, num):
        self.num = num
    def changelist(self, request):
        print(self.num, request)
config_obj_list = [StarkConfig(1), StarkConfig(2), StarkConfig(3)]
for item in config_obj_list:
    item.changelist(666)
'''
# class StarkConfig(object):
#     def __init__(self, num):
#         self.num = num
#     def changelist(self, request):
#         print(self.num, request)
# config_obj_list = [StarkConfig(1), StarkConfig(2), StarkConfig(3)]
# for item in config_obj_list:
#     item.changelist(666)
#1 666
#2 666
#3 666


'''    
    
    
看代码写结果：
class Department(object):
    def __init__(self,title):
        self.title = title
class Person(object):
    def __init__(self,name,age,depart):
        self.name = name
        self.age = age 
        self.depart = depart
d1 = Department('人事部')
d2 = Department('销售部')
p1 = Person('武沛齐',18,d1)
p2 = Person('alex',18,d1)
p3 = Person('安安',19,d2)
print(p1.name)
print(p2.age)
print(p3.depart)
print(p3.depart.title)
'''
# class Department(object):
#     def __init__(self,title):
#         self.title = title
# class Person(object):
#     def __init__(self,name,age,depart):
#         self.name = name
#         self.age = age
#         self.depart = depart
# d1 = Department('人事部')
# d2 = Department('销售部')
# p1 = Person('武沛齐',18,d1)
# p2 = Person('alex',18,d1)
# p3 = Person('安安',19,d2)
# print(p1.name)#武沛齐
# print(p2.age)#18
# print(p3.depart)#d2对象的对象地址
# print(p3.depart.title)#销售部
'''
看代码写结果：
class Department(object):
    def __init__(self,title):
        self.title = title
class Person(object):
    def __init__(self,name,age,depart):
        self.name = name
        self.age = age 
        self.depart = depart
    def message(self):
        msg = "我是%s,年龄%s,属于%s" %(self.name,self.age,self.depart.title)
        print(msg)
d1 = Department('人事部')
d2 = Department('销售部')
p1 = Person('武沛齐',18,d1)
p2 = Person('alex',18,d1)
p1.message()
p2.message()
'''
# class Department(object):
#     def __init__(self,title):
#         self.title = title
# class Person(object):
#     def __init__(self,name,age,depart):
#         self.name = name
#         self.age = age
#         self.depart = depart
#     def message(self):
#         msg = "我是%s,年龄%s,属于%s" %(self.name,self.age,self.depart.title)
#         print(msg)
# d1 = Department('人事部')
# d2 = Department('销售部')
# p1 = Person('武沛齐',18,d1)
# p2 = Person('alex',18,d1)
# p1.message()
# #我是武佩奇,年龄18岁,属于人事部
# p2.message()
#我是alex,年龄18岁,属于人事部.


'''
看代码写结果：
class A:
    def f1(self):
        print('in A f1')
class B(A):
    def f1(self):
        print('in B f1')
class C(A):
    def f1(self):
        print('in C f1')
class D(B, C):
    def f1(self):
        super(B, self).f1()
        print('in D f1')
obj = D()
obj.f1()

'''
# class A:
#     def f1(self):
#         print('in A f1')
# class B(A):
#     def f1(self):
#         print('in B f1')
# class C(A):
#     def f1(self):
#         print('in C f1')
# class D(B, C):
#     def f1(self):
#         super(B, self).f1()
#         print('in D f1')
# obj = D() #D,B,C,A
# obj.f1()
#in C f1
#in D f1

'''
看代码写结果：
class A:
    def f1(self):
        print('in A f1')
class B(A):
    def f1(self):
        super().f1()
        print('in B f1')
class C(A):
    def f1(self):
        print('in C f1')
class D(B, C):
    def f1(self):
        super().f1()
        print('in D f1')
obj = D()
obj.f1()
'''
# class A:
#     def f1(self):
#         print('in A f1')
# class B(A):
#     def f1(self):
#         super().f1()
#         print('in B f1')
# class C(A):
#     def f1(self):
#         print('in C f1')
# class D(B, C):
#     def f1(self):
#         super().f1()
#         print('in D f1')
# obj = D()
# obj.f1()
#in C f1
#in B f1
#in D f1
'''
程序设计题：
运用类完成一个扑克牌类(无大小王)的小游戏：
用户需要输入用户名，以下为用户可选选项:
    1. 洗牌
    2. 随机抽取一张
    3. 指定抽取一张
    4. 从小到大排序
    5. 退出
1. 洗牌：每次执行的结果顺序随机。
2. 随机抽取一张：显示结果为：太白金星您随机抽取的牌为：黑桃K
3. 指定抽取一张：
    用户输入序号（1~52）
    比如输入5，显示结果为：太白金星您抽取的第5张牌为：黑桃A
4. 将此牌从小到大显示出来。A -> 2 -> 3 .......-> K
提供思路：
    52张牌可以放置一个容器中。
    用户名，以及盛放牌的容器可以封装到对象属性中。
'''
# import random
# class puke:
#
#     def __init__(self,name):
#         li = ['黑桃','红桃','梅花','方块']
#         li1 = ['A','2','3','4','5','6','7','8','9','10','J','Q','K']
#         li2 = []
#         li3 = []
#         for i in li1:
#             for j in li:
#                 li2.append(j+i)
#                 li3.append(j+i)
#         self.name = name
#         self.li2 = li2
#         self.li3 = li3
#     def xipai(self):
#         random.shuffle(self.li2)
#         print(self.li2)
#     def random_ex(self):
#         print(f'{self.name}您随机抽取的牌为{random.choices(self.li2)}')
#     def ex_zhiding(self,n):
#         print(f'{self.name}您抽取的第{n}张牌是{self.li2[n-1]}')
#
# def puke_play():
#     input_use = input('请输入您的用户名')
#     use_play = puke(input_use)
#     while 1:
#         input_chonse = input('''
#         1. 洗牌
#         2. 随机抽取一张
#         3. 指定抽取一张
#         4. 从小到大排序
#         5. 退出
#         ''')
#         if input_chonse == '1':
#             use_play.xipai()
#         if input_chonse == '2':
#             use_play.random_ex()
#         if input_chonse == '3':
#             while 1:
#                 n = input('请您选择抽取第几张牌?(1-52)')
#                 if n.isdecimal():
#                     if int(n) in range (53):
#                         break
#             use_play.ex_zhiding(int(n))
#         if input_chonse == '4':
#             print(use_play.li3)
#         if input_chonse == '5':
#             exit()
# puke_play()
