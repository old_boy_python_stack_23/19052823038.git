# class A:
#     def func1(self):
#         print('in A')
# class B(A):
#     def func1(self):
#         print('in B')
# class C(A):
#     def func1(self):
#         print('in C')
#         super(B,self).func1()
# class D(B,C):
#     def func1(self):
#         print('in D')
# class E(B,C):
#     def func1(self):
#         print('in E')
# class F(D,E):
#     def func1(self):
#         print('in F')
#         super(B,self).func1()
# # print(F.mro())#F,D,E,B,C,A
# ret = F()
# ret.func1()

#鸭子类型   不同类之间,长的比较像,就是鸭子类型.
# 因为鸭子类型的内容长的都一样,可以归一化设计用统一接口把各鸭子当参数放进去
# class QQpay:   #鸭子1
#     def pay(self,money):
#         print(f'利用QQ支付了{money}')
# class Alipay:   #鸭子2
#     def pay(self, money):
#         print(f'利用支付宝支付了{money}')
#
# def pay(obj,money):   #统一接口
#     obj.pay(money)
# obj1 = QQpay()
# obj2 = Alipay()
#
# pay(obj1,100)
# pay(obj2,200)
# raise Exception('你能看到这,说明我在找茬')  #主动抛出错误

from abc import ABCMeta,abstractmethod
class Payment(metaclass=ABCMeta):
    @abstractmethod
    def pay(self,money):
        pass
class QQpay(Payment):
    def pay(self,money):
        print(f'通过QQ支付了{money}')

class ALIpay(Payment):
    def pay(self,money):
        print(f'通过支付宝支付了{money}')

class Wechatpay(Payment):
    def pay(self,money):
        print(f'通过微信支付了{money}')

def pay(obj,money):
    obj.pay(money)

obj1 = QQpay()
obj2 = ALIpay()
obj3 = Wechatpay()
pay(obj1,300)
pay(obj2,300)
pay(obj3,300)




