# 数据库部分
# 数据库里的名词 : DBMS DB DBA table data
# 数据库的分类:
    # 关系型: msyql oracle sql server
    # 非关系型: redis mongodb
# sql 语句的种类
    # ddl : create drop alter
    # dml : select insert update delete
    # dcl : grant revoke

# sql 语句 :
    # 创建用户\给权限\设置密码
# 库的操作
    # 建库 create database
    # 查库 show databases;
    #
    # 删库
# 表的操作
    # 存储引擎 :
        # innodb (行级锁,表级锁,外键,事务
    # 查看库中有哪些表 showtables
    # 创建表
        # primary key /not null /unique/default/auto_increment/unsigned/foreign key
    # 查看表结构 desc
        #
    # 查看建表属性 show create tables 表明
    # 修改表结构 alter change
    # 删除表 drop
    # 表与表的关系   一对一,一对多,多对多.
# 数据的操作
    # insert
    # update
    # delete
    # select
        # 单表查
            # group by /where/having/ order by/limit/
            # sum/avg/min/max/count
        # 多表查
            # 连表
                # inner join /left join / right join/ union
            # 子查询
                # select 子查询 from 表 where 条件= 子查询 等.

# 索引原理
    # 创建索引
    # 删除索引
    # 索引种类
        # memory hash索引
        # (innodb/myisam) -b + tree (聚集索引 辅助索引)
    # 有哪些索引
        # primary key 主键索引,联合唯一索引
        # unique key 唯一索引\联合唯一索引
        # index key 普通索引\联合索引
    # 如何创建和使用索引
        # 创建: 尽量选定长,长度短,重复率低.
        # 使用:
            # 创建了索引,但没用上
            # 范围 大,不易命中
            # like %占位符.放前面的慢,放后面快.
            # 条件类参与计算或使用函数了
            # 条件中用到or的,且并不是所有列都有索引.则难以命中
            # 联合索引的时候,最左前缀原则.
# explain 执行计划
    # 试一下索引可能出现的情况.

