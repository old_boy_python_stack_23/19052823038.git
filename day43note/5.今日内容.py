# 浏览器怎么和server 端交互
# 前端的文件有一个后缀名: html
# web 端的组成:
    # html 一堆标签组成的内容 基础的排版    # 骨头
    # css 描述了标签的样式    #
    # js/jq 动态的效果  穿衣服,化妆

# 超文本标记语言
    # html就是一种超文本标记语言
    # 超文本:图片,音频,视频等.
    # 标记 : 所有内容都是包裹在标签中的
        # 位置,大小,颜色等
# 标记(标签)的分类
    # 双边标记
        #< body >
        #< / body > 双边标记,双闭合标记
        # 单边标记<meta> 单闭合
# 1.html标签
# <!doctype html> 声明我的语言是html哪个版本,当前默认html5.
#<html lang="en"> 除第一句外,所有内容要包含在html标签内.
# lang = "en"是语言.en为英文, zh-cn是中文.
    # head : 思想
        # 写在head标签中的内容,都不可见,是逻辑.
    #<head>
    #<title>这里是标签名</title>
    #meta 网页元信息,编码,浏览器版本,关键字,描述
        #<meta http-equiv="X-UA-Compatible" content="ie=edge">
        # 声明用最高版本来渲染
        #<meta name="viewport"
        #         content="width=device-width, user-scalable=no, initial-scale=1.0, " \
        #                  "maximum-scale=1.0, minimum-scale=1.0">
        # 这部分是给搜索引擎看的,对外宣传用.
    #</head>

    # body : 身体
    #<body>
        #标签的分类:
            # 内联标签(行内标签),不带换行功能的
                # b/strong 加粗
                # a/

                # 标签 img
                    # src 属性
                        # 一张图片的网络地址或本地路径
                    # width 属性
                        # 设置图片的尺寸
                        # height 高,width宽.二选一自适应.
                        # 200px
                    # alt 属性
                        # 在图片加载失败的时候显示的内容
                # 标签 a
                    # 超链接
                    # 锚 anchor
                    # href 属性
                        # 1. 网络资源
                        # 2. 邮件资源,"mailto:邮箱地址"
                        # 3.设置锚点
                            # href = "#"一个井号回到顶部
                            # 在想跳的位置设置道标.
                            # href = "#id或a标签name"跳到道标位置
                    # target 属性
                        # '_self'  (默认)在当前标签页打开
                        # '_blank' 在新标签页跳转
                    # title
                        # 鼠标悬浮时显示的内容.

            # 块级标签 带换行功能的
                # h1 -h6是字体



    #</body>
