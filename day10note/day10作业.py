'''
1写函数，函数可以支持接收任意数字（位置传参）并将所有数据相加并返回。
'''
# def sum_(a,b,c,d,e):
#     return a+b+c+d+e
# print (sum_(1,3,4,555,6))

'''
看代码写结果
def func():
    return 1, 2, 3
val = func()
print(type(val) == tuple)
print(type(val) == list)
'''
# True#返回值是一个元组(1,2,3)
# False

'''看代码写结果
def func(*args, **kwargs):
    pass
# a. 请将执行函数，并实现让args的值为 (1,2,3,4)
# b. 请将执行函数，并实现让args的值为 ([1,2,3,4],[11,22,33])
# c. 请将执行函数，并实现让args的值为 ([11,22],33) 且 kwargs的值为{'k1':'v1','k2':'v2'}
# d. 如执行 func(*{'武沛齐','金鑫','女神'})，请问 args和kwargs的值分别是？
# e. 如执行 func({'武沛齐','金鑫','女神'},[11,22,33])，请问 args和kwargs的值分别是？
# f. 如执行 func('武沛齐','金鑫','女神',[11,22,33],**{'k1':'栈'})，请问 args和kwargs的值分别是？
'''
# def func(*args, **kwargs):
#     print(args)
#     pass
# print(func(1,2,3,4))
# print(func(([11,22],33),{'k1':'v1','k2':'v2'}))


'''看代码写结果


def func(name, age=19, email='123@qq.com'):
    pass
# a. 执行 func('alex') ,判断是否可执行，如可以请问 name、age、email 的值分别是？
# b. 执行 func('alex',20) ,判断是否可执行，如可以请问 name、age、email 的值分别是？
# c. 执行 func('alex',20,30) ,判断是否可执行，如可以请问 name、age、email 的值分别是？
# d. 执行 func('alex',email='x@qq.com') ,判断是否可执行，如可以请问 name、age、email 的值分别是？
# e. 执行 func('alex',email='x@qq.com',age=99) ,判断是否可执行，如可以请问 name、age、email 的值分别是？
# f. 执行 func(name='alex',99) ,判断是否可执行，如可以请问 name、age、email 的值分别是？
# g. 执行 func(name='alex',99,'111@qq.com') ,判断是否可执行，如可以请问 name、age、email 的值分别是？
'''


'''看代码写结果


def func(users, name):
    users.append(name)
    return users

result = func(['武沛齐', '李杰'], 'alex')
print(result)
看代码写结果


def func(v1):
    return v1 * 2


def bar(arg):
    return "%s 是什么玩意？" % (arg,)


val = func('你')
data = bar(val)
print(data)
看代码写结果


def func(v1):
    return v1 * 2


def bar(arg):
    msg = "%s 是什么玩意？" % (arg,)
    print(msg)


val = func('你')
data = bar(val)
print(data)
看代码写结果

v1 = '武沛齐'


def func():
    print(v1)


func()
v1 = '老男人'
func()
看代码写结果
v1 = '武沛齐'
def func():
    v1 = '景女神'
    def inner():
        print(v1)
    v1 = '肖大侠'
    inner()
func()
v1 = '老男人'
func()
看代码写结果【可选】
def func():
    data = 2 * 2
    return data
new_name = func
val = new_name()
print(val)
# 注意：函数类似于变量，func代指一块代码的内存地址。
看代码写结果【可选】

def func():
    data = 2 * 2
    return data


data_list = [func, func, func]
for item in data_list:
    v = item()
    print(v)

# 注意：函数类似于变量，func代指一块代码的内存地址。
看代码写结果（函数可以做参数进行传递）【可选】

def func(arg):
    arg()
def show():
    print('show函数')
func(show)
#以上代码输出结果是show函数
def func(arg):
    pass
    # arg()
def show():
    print('show函数')
func(show)  #以上几行代码结果是什么都不发生.    func函数执行,show作为参数进去,
此时show仅仅是参数,是不执行的.当func函数体内用到这个参数时,show函数才运行.所以如果func只有pass,show函数不打印.
写函数，接收n个数字，求这些参数数字的和。（动态传参）
'''
# def num_input(*args):
#     num_1 = 0
#     for i in args:
#         num_1 += int(i)
#     return num_1
# print(num_input(3,4,6,8,93,34524,333,444))
'''
读代码，回答：代码中, 打印出来的值a, b, c分别是什么？为什么？
a = 10
b = 20
def test5(a, b):
    print(a, b)
c = test5(b, a)
print(c)
20 10
none
读代码，回答：代码中, 打印出来的值a, b, c分别是什么？为什么？
a = 10
b = 20
def test5(a, b):
    a = 3
    b = 5
    print(a, b)
c = test5(b, a)
print(c)
3 5
none
传入函数中多个列表和字典, 如何将每个列表的每个元素依次添加到函数的动态参数args里面？如何将每个字典的所有键值对依次添加到kwargs里面？
定义函数时,要有*args,**kwargs
调用函数时,直接用*列表1,*列表2...**字典1,**字典2.
写函数, 接收两个数字参数, 将较小的数字返回.
'''
# def compare(a,b):
#     if a > b:
#         return a
#     elif b>a:
#         return b
#     else:
#         return f"{a}={b}"
# print(compare(5,3))
'''写函数, 接收一个参数(此参数类型必须是可迭代对象), 将可迭代对象的每个元素以’_’相连接, 形成新的字符串, 并返回.
例如
传入的可迭代对象为[1, '老男孩', '武sir']
返回的结果为’1
_老男孩_武sir’
'''
# def li1_li2(li1):
#     str1 = ''
#     for i in li1:
#         str1 = str1 + '_' + str(i)
#     return str1[1:]
# print(li1_li2([1, '老男孩', '武sir']))
# def li1_li2(*args):
#     str1 = ''
#     for i in args:
#         str1 = str1 + '_' + str(i)
#     return str1[1:]
# print(li1_li2(*[1, '老男孩', '武sir']))


'''
19.
有如下函数:
def wrapper():
    def inner():
        print(666)
wrapper()
你可以任意添加代码, 执行inner函数.
'''
# def wrapper():
#     def inner():
#         print(666)
#     inner()
# wrapper()


'''相关面试题：
写出下列代码结果：
def foo(a, b, *args, c, sex=None, **kwargs):
    print(a, b)
    print(c)
    print(sex)
    print(args)
    print(kwargs)
\  # foo(1,2,3,4,c=6)
1 2
6
None
(3,4)
{}
\  # foo(1,2,sex='男',name='alex',hobby='old_woman')
报错c作为位置参数,被前面的*args拿不到位置参数值,如果不指定参数,就会报错.

\  # foo(1,2,3,4,name='alex',sex='男')
报错,同上
\  # foo(1,2,c=18)
1 2
18
None
()
{}
\  # foo(2, 3, [1, 2, 3],c=13,hobby='喝茶')
2 3
13
None
([1,2,3],)
{'hobby':'喝茶'}
\  # foo(*[1, 2, 3, 4],**{'name':'太白','c':12,'sex':'女'})
1 2
12
女
(3,4)
{'name':'太白'}


'''
def foo(a, b, *args, c, sex=None, **kwargs):
    print(a, b)
    print(c)
    print(sex)
    print(args)
    print(kwargs)
foo(*[1, 2, 3, 4],**{'name':'太白','c':12,'sex':'女'})
