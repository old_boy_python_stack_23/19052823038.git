'''
1.整理当天的笔记,将今天课上代码自己练习一下
'''
'''
2.用户输入一个数字，判断一个数是否是水仙花数。

水仙花数是一个三位数, 三位数的每一位的三次方的和还等于这个数. 那这个数就是一个水仙花数, 例如: 153 = 13 + 53 + 3**3
'''
# use_input = input("请输入一个三位数:")
# n = 0
# if use_input.isdecimal():
#     for i in use_input:
#         n += int(i) ** 3
#     if n == int(use_input):
#         print(use_input,"是水仙花数")
#     else:
#         print(use_input, "不是水仙花数")
#找一下都有哪些水仙花数:
# sxhs = []
# for i in range(100,1000):
#     n = 0
#     for j in str(i):
#         n += int(j) ** 3
#     if n == i:
#         sxhs.append(i)
# print(sxhs)
#运行结果 [153,370,371,407]  一共4个水仙花数.



'''
3.请说出下面a,b,c三个变量的数据类型。

a = ('太白金星')   元组括号如果只有一个元素而且没有逗号,数据类型是这个元素本身.  所以是字符串.

b = (1,)       元组

c = ({'name': 'barry'})   字典,道理同上

4.按照需求为列表排序：

l1 = [1, 3, 6, 7, 9, 8, 5, 4, 2]

从大到小排序

从小到大排序

反转l1列表
'''
# l1 = [1, 3, 6, 7, 9, 8, 5, 4, 2]
# l1.sort()
# l1.sort(reverse = True)
# l1.reverse()
# print(l1)



'''
5.看代码写结果：

dic = dict.fromkeys('abc',[])

dic['a'].append(666)

dic['b'].append(111)

print(dic)
{'a':[666,111],'b':[666,111],'c':[666,111]}

6.完成彩票36选7的功能. 从36个数中随机的产生7个数. 最终获取到7个不重复的数据作为最终的开奖结果.

随机数:

from random import randint

randint(0, 20) # 0 - 20 的随机数

'''
# from random import randint
# set1 = set()
# while len(set1) < 7:
#     n = randint(1,36)
#     set1.add(n)
# print(set1)






'''7.字符串和字节转换
s1 = '太白金星'
将s1转换成utf-8的bytes类型。
将s1转化成gbk的bytes类型。
b = b'\xe5\xa4\xaa\xe7\x99\xbd\xe6\x9c\x80\xe5\xb8\x85'
b为utf-8的bytes类型，请转换成gbk的bytes类型。
'''
# s1 = '太白金星'
# s2 = s1.encode("utf-8")
# print(s2)
# s3 = s1.encode('gbk')
# print(s3)
# b = b'\xe5\xa4\xaa\xe7\x99\xbd\xe6\x9c\x80\xe5\xb8\x85'
# b1 = b.decode("utf-8")
# b2 = b1.encode('gbk')
# print(b1)
# print(b2)

'''8.把列表中所有姓周的⼈的信息删掉(升级题：此题有坑, 请慎重):
lst = ['周⽼⼆', '周星星', '麻花藤', '周扒⽪']
结果: lst = ['麻花藤']
'''
# lst = ['周⽼⼆', '周星星', '麻花藤', '周扒⽪']
# lst1 = []
# for i in lst:
#     if i.startswith('周'):
#         lst1.append(i)
# for i in lst1:
#     lst.remove(i)
# print(lst)
'''9.⻋牌区域划分, 现给出以下⻋牌. 根据⻋牌的信息, 分析出各省的⻋牌持有量. (升级题)
cars = ['鲁A32444','鲁B12333','京B8989M','⿊C49678','⿊C46555','沪 B25041'.....]
locals = {'沪':'上海', '⿊':'⿊⻰江', '鲁':'⼭东', '鄂':'湖北', '湘':'湖南'.....}
结果: {'⿊⻰江':2, '⼭东': 2, '北京': 1,'上海':1}
'''
# cars = ['鲁A32444','鲁B12333','京B8989M','⿊C49678','⿊C46555','沪 B25041']
# locals = {'沪':'上海', '⿊':'⿊⻰江', '鲁':'⼭东', '鄂':'湖北', '湘':'湖南','京':'北京'}
# locals_cars = {}
# for i in cars:
#     if i[0] in locals:
#         locals_cars[i[0]] = locals[i[0]]
# # print(locals_cars)
# locals_cars1 = dict.fromkeys(locals_cars.values(),0)
# # print(locals_cars1)
# for i in cars:
#     locals_cars1[locals_cars[i[0]]] += 1
# print(locals_cars1)





'''10.⼲掉主播. 现有如下主播收益信息: 
zhubo = {'卢本伟':122000, '冯提莫':189999, '⾦⽼板': 99999, '吴⽼板': 25000000, 'alex': 126}
 1. 计算主播平均收益值 2. ⼲掉收益⼩于平均值的主播 3. ⼲掉卢本伟
'''
# zhubo = {'卢本伟':122000, '冯提莫':189999, '⾦⽼板': 99999, '吴⽼板': 25000000, 'alex': 126}
# n = 0
# gan_diao_zhu_bo = []
# for i in zhubo:
#     n += zhubo[i]
# for i in zhubo:
#     if zhubo[i] < n/len(zhubo):
#         gan_diao_zhu_bo.append(i)
# for i in gan_diao_zhu_bo:
#     del zhubo[i]
# if '卢本伟' in zhubo:
#     del zhubo['卢本伟']
# print(zhubo)
