# 1. 单表查询
# 2. 补遗
    # 表操作
        # 修改表
    # 数据操作
        # 增加
        # 删除
        # 修改
# 3. 小测试

# 1. 单表查询
# select post,group_concat(name) from employee where  group by having order by  limit

#select from where group by 分组  having 分组后筛选组.   order by排序 默认升,desc降  having过滤组
# limit m,n  取从m+1开始取n条.  m不写则默认为0.  取前几条. order by
# select * from 表 group by 字段名
#上面这句话,会按照字段名分成两组.并每组显示第一条信息.
    #分组 group
    # 根据某个重复率比较搞得字段进行
    # 字段有多少种可能就分读诵好组
        # 根据性别分组,男一组,女一组
        # 根据年龄分组,
    # 去重
    # 分组后,不能单独操作某条数据.而是考虑整组数据
# 聚合函数
    # 函数
        # count  计数
        # max    最大
        # min    最小
        # avg    均值
        # sum    求和
    # 用法 通常是跟分组一起用的.如果没有分组,默认整张表为一个组.

# select 想要的列 from 表 where 先从这张表中查询的人 group by 分组


# select post,avg(salary) from employee group by post;
#
#---+----------  ---------+------------+------------------------------+--------------+------------+------+---------+
#id | emp_name   sex  age | hire_date  | post     employee           | post_comment | salary   | office depart_id |
#---+----------  ---------+------------+------------------------------+--------------+------------+------+---------+
# 1 | egon       male  18 | 2017-03-01 | 老男孩驻沙河办事处外交大使    | NULL     |    7300.33 |  401 |       1 |
# 2 | alex       male  78 | 2015-03-02 | teacher                      | NULL         | 1000000.31 |  401 |       1 |
# 3 | wupeiqi    male  81 | 2013-03-05 | teacher                      | NULL         |    8300.00 |  401 |       1 |
# 4 | yuanhao    male  73 | 2014-07-01 | teacher                      | NULL         |    3500.00 |  401 |       1 |



#练习
# 拆线呢各岗位报韩的员工个属性i奥羽2的岗位名和岗位内的员工名字以及员工个数
# select post,emp_name,count(id) from employee group by post having count(id)<2;
# 查询岗位平均薪资大于1万的岗位名和平均工资
# select post,evg()
# 查询岗位平均薪资大于1万的小于2万的岗位名和平均工资

# 2. 补遗
    # 表操作
        # 修改表    (底层数据的重构,使用频率非常低)
            # alter table 表名 rename 新表名
            # alter table 表名 add 新字段 类型 约束
            # alter table 表名 add 新字段 类型 约束 插入位置(first,after 原字段)
            # alter table 表名 change 旧字段名 新字段名 类型 约束 等.
            # alter table 表名 drop 字段名;
            # alter table 表名 change name name 新类型 约束等,(等于没改名),位置
            # alter table 表名 modify 字段 类型 约束  (除名字外的内容),位置
            # alter table
    # 数据操作
        # id name age
        # 增加
            # insert into 表名 value 一条,values多条.
            # insert into 表名 (name,gender) valuse(***
            # issert into 表名 (name,gender) select (username,sex) from day38.表2

        # 删除
            # delete from 表名 where 条件
        # 修改
            # update 表名 set 字段= 值 where 条件
            # update 表名 set 字段1 = 值1 字段2 =值2 where 条件
# 3. 小测试