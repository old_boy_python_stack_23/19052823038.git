#sql注入,
import pymysql
# li1='学python从开始到放弃|alex|人民大学出版社|50|2018-7-1'.strip().split('|')
# li2 = '|学mysql从开始到放弃|egon|机械工业出版社|60|2018-6-3'.strip().split('|')
# li3='学html从开始到放弃|alex|机械工业出版社|20|2018-4-1'.strip().split('|')
# li4='学css从开始到放弃|wusir|机械工业出版社|120|2018-5-2'.strip().split('|')
# li5= '学js从开始到放弃|wusir|机械工业出版社|100|2018-7-30'.strip().split('|')

conn = pymysql.Connection(host = '127.0.0.1',user='root',password = '123456',database = 'day40')
cur = conn.cursor()   #游标 用游标来执行sql语句.
# cur.execute('select * from employee')
# sql = 'insert into book(name,author,press,price,date) values ("学python从开始到放弃","alex","机械工业出版社",50,20180701)'
sql = 'insert into book(name,author,press,price,date) values (' \
      '"学css从开始到放弃","wusir","机械工业出版社",50,20180701),' \
      '("学js从开始到放弃","wusir","机械工业出版社",60,20180603),' \
      '("学html从开始到放弃","alex","机械工业出版社",20,20180401),' \
      '("学mysql从开始到放弃","egon","机械工业出版社",120,20180502),' \
      '("学python从开始到放弃","alex","机械工业出版社",100,20180730)'
# sql = 'insert into book(name,author,press,price,date) values ("学js从开始到放弃","wusir","机械工业出版社",50,20180701)'
cur.execute(sql)
conn.commit()
# ret1= cur.fetchone()
# ret2= cur.fetchmany(4)
# ret3= cur.fetchall()
# print(ret1)
# print(ret2)
# print(ret3)
cur.close()
conn.close()
# | id | name            | author | press                          | price | date       |
# +----+-----------------+--------+--------------------------------+-------+------------+
# |  1 | 倚天屠龙记      | egon   | 北京工业地雷出版社             |    50 | 2019-07-01 |
# |  4 | 九阴白骨爪      | jinxin | 人民音乐不好听出版社           |    40 | 2019-08-07 |
# |  6 | 降龙十巴掌      | egon   | 知识产权没有用出版社           |    20 | 2019-07-05 |
# |  7 | 葵花宝典        | yuan   | 知识产权没有用出版社           |    33 | 2019-08-02 |
# +----+-----------------+--------+--------------------------------+-------+------------


