import os
from multiprocessing import  Queue
from multiprocessing import  Process

def task(q):
    try :
        q.put(f'{os.getpid()}',block=False)
    except Exception:
        return

if __name__ == '__main__':
    q = Queue(10)
    for i in range(100):
        p = Process(target=task,args=(q,))
        p.start()
    for i in range(1,11):
        print(f'排名第{i}的用户:{q.get()}',)
