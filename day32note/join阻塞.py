from multiprocessing import Process
import time

def task(name,sec):
    time.sleep(sec)
    print(f'{name}子进程,is running')
if __name__ == '__main__':
    p1 = Process(target=task,args=('李业',3))
    p2 = Process(target=task,args=('怼怼',2))
    p3 = Process(target=task,args=('骚Q',1))
    start_time = time.time()
    p1.start()
    p2.start()
    p3.start()
    p1.join()
    p2.join()
    p3.join()
    print(f'===主进程:{time.time()-start_time}之后,zhixing ')