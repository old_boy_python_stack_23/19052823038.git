from multiprocessing import Process
import time

x = 100

def task():
    global x
    x= 2
    print(f'我是子进程调用的函数打印的x={x}')
if __name__ == '__main__':
    p1 = Process(target=task,)
    p1.start()
    time.sleep(1)
    print(f'我是主进程调用的函数打印的x={x}')