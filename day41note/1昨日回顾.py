# 单表查询
    # group by 分组

    # 聚合函数
        # count
        # max
        # min
        # avg
        # sum
    #having 筛选组 过滤条件
        # having 可以用聚合函数,这一点儿跟where不同

    # order by 排序
        # 默认asc  升序排列
        # desc 降序排列

    # limit
        # n 取1-n行
        # m,n 从m+1开始取n行
        # n offset m.

# select * from 表
    # where  条件
    # group by  分组
    # having  过滤组条件
    # order by 排序 asc升序  desc降序
    # limit 显示区间.区间前的内容也会加载.只是不显示.

# 表操作
    # 修改表操作
        # alter table 表名 rename 改名
        # alter table 表名 add 增加字段
        # alter table 表名 drop 删除字段
        # alter table 表名 modify 修改字段 除字段名
        # alter table 表名 change 修改字段包括字段名

# 数据操作
    # 增 insert into 表名(字段,字段) values (值1,值2)
    # 删 delete from 表名 where 条件;
    # update 表名 set 字段= 值 where条件;

# 数据库的查看 创建
# 表结构的创建 查看 删除 修改    #查看是重点
# 数据的 增删改查(insert delete update select)    #数据的增删改查是重点.