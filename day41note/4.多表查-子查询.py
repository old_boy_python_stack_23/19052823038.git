# 子查询
    # 年龄大于25岁的员工及员工所在部门
    # select dep_id from employee where age>25;
    # select * from department where id in (select dep_id from employee where age>25);
    # 有两个select 的,就算是子查询.  用括号嵌套.
# 查询平均你那领再25以上的部门名.
#select d.name,avg_age from department d inner join
# (select dep_id,avg(age) as avg_age from employee
# group by dep_id having avg(age)>25 ) e on d.id = e.dep_id;

#select d.name,e.avg(age) from department d inner join
# (select dep_id,avg(age) from employee group by dep_id
# having avg(age)>25 ) e on d.id = e.dep_id;

#select * from department d inner join
# (select dep_id,avg(age) from employee group by
# dep_id having avg(age)>25 ) e on d.id = e.dep_id;

#select * from department d inner join
# (select avg(age) from employee group by dep_id
# having avg(age)>25 ) e on d.id = e.dep_id;
# 查看技术不员工姓名
# select e.name from employee e inner join department d on d.id = dep_id where d.name ='技术';
# 查看不足1人的部门名
# select d.name from employee e inner join department d on d.id = dep_id group by d.name having count(e.id) =1;
# 查看年龄大于平均年龄的员工姓名和年龄
# select name,age from employee where age > (select avg(age) from employee);
# 查询大于部门内平均年龄的员工名,年龄
# select t.name,age from
# (select e.name,age,avg(age) as avg_age from employee e inner join department d on d.id = e.dep_id group by e.dep_id) t
#    where age > avg_age;

# 每个部门最新入职的员工
# select emp_name,hire_date from employee e inner join
# (select post,max(hire_date) as max_date from employee group by post) n

#select name as n,(select age from employee where name = n) from emp;  再这个位置嵌套的子查询,必须是单行单列的单个值.并能作为关键字的.