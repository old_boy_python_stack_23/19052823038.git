# import random
# for i in range(1,17):
#     for j in range(5,8):
#         print('(%d,%d,%d),'%(i,j,random.randint(1,100)))
#准备工作 添加数据
# insert into teacher(tname) values('叶平'),('杨艳');
# insert into course(cname,teacher_id) values('数学',6),('语文',7),('英语',7);
# insert into score(student_id,course_id,num) values(1,5,77),
# (1,6,21),
# (1,7,76),
# (2,5,30),
# (2,6,6),
# (2,7,6),
# (3,5,37),
# (3,6,67),
# (3,7,100),
# (4,5,1),
# (4,6,62),
# (4,7,94),
# (5,5,26),
# (5,6,85),
# (5,7,57),
# (6,5,43),
# (6,6,37),
# (6,7,25),
# (7,5,60),
# (7,6,25),
# (7,7,89),
# (8,5,14),
# (8,6,79),
# (8,7,60),
# (9,5,71),
# (9,6,75),
# (9,7,20),
# (10,5,71),
# (10,6,79),
# (10,7,61),
# (11,5,5),
# (11,6,20),
# (11,7,55),
# (12,5,93),
# (12,6,30),
# (12,7,28),
# (13,5,72),
# (13,6,4),
# (13,7,47),
# (14,5,10),
# (14,6,17),
# (14,7,73),
# (15,5,40),
# (15,6,88),
# (15,7,38),
# (16,5,89),
# (16,6,76),
# (16,7,31);

#1、查询没有学全所有课的同学的学号、姓名；
# select sid,sname from student
# where sid in
# (select student_id from score group by student_id having count(num) <
#     (select count(cid) from course)
# )
# +-----+--------+
# | sid | sname  |
# +-----+--------+
# |   1 | 理解   |
# |   2 | 钢蛋   |
# |  13 | 刘三   |
# |  14 | 刘一   |
# |  15 | 刘二   |
# |  16 | 刘四   |
# +-----+--------+
# 2、查询和3号的同学学习的课程完全相同的其他同学学号和姓名；
# select sid,sname from student where sid in (
# select s2.student_id from
# (select student_id,group_concat(course_id) as g1 from score where student_id = 3 ) as s1
# inner join
# (select student_id,group_concat(course_id) as g2 from score where student_id != 3 group by student_id) as s2
# on g1=g2
# )
# +-----+--------+
# | sid | sname  |
# +-----+--------+
# |   4 | 张一   |
# |   5 | 张二   |
# |   6 | 张四   |
# |   7 | 铁锤   |
# |   8 | 李三   |
# |   9 | 李一   |
# |  10 | 李二   |
# |  11 | 李四   |
# |  12 | 如花   |
# +-----+--------+
# 3、删除学习“叶平”老师课的SC表记录；此题与22题有冲突,放在22题后面执行.

# 4、向SC表中插入一些记录，这些记录要求符合以下条件：①没有上过编号“002”课程的同学学号；②插入“002”号课程的平均成绩；
# insert into score(student_id,course_id,num)
# select * from
# ((
#     (
#         select sid from student where sid not in
#         (select student_id from score where course_id = 2)
#     )
# as no2
# inner join
# (select cid from course where cid = 2) as c2
# )
# inner join
# (select avg(num) from score where course_id = 2) as sc2)
# 5、按平均成绩从低到高显示所有学生的“语文”、“数学”、“英语”三门的课程成绩，按如下形式显示： 学生ID,语文,数学,英语,有效课程数,有效平均分；

# 6、查询各科成绩最高和最低的分：以如下形式显示：课程ID，最高分，最低分；
# 7、按各科平均成绩从低到高和及格率的百分数从高到低顺序；
# 8、查询各科成绩前三名的记录:(不考虑成绩并列情况)
# 9、查询每门课程被选修的学生数；
# 10、查询同名同姓学生名单，并统计同名人数；
# 11、查询每门课程的平均成绩，结果按平均成绩升序排列，平均成绩相同时，按课程号降序排列；
# 12、查询平均成绩大于85的所有学生的学号、姓名和平均成绩；
# 13、查询课程名称为“数学”，且分数低于60的学生姓名和分数；
# 14、查询课程编号为003且课程成绩在80分以上的学生的学号和姓名；
# 15、求选了课程的学生人数
# 16、查询选修“杨艳”老师所授课程的学生中，成绩最高的学生姓名及其成绩；
# 17、查询各个课程及相应的选修人数；
# 18、查询不同课程但成绩相同的学生的学号、课程号、学生成绩；
# 19、查询每门课程成绩最好的前两名；
# 20、检索至少选修两门课程的学生学号；
# 21、查询全部学生都选修的课程的课程号和课程名；
# 22、查询没学过“叶平”老师讲授的任一门课程的学生姓名；

# 23、查询两门以上不及格课程的同学的学号及其平均成绩；
# 24、检索“004”课程分数小于60，按分数降序排列的同学学号；
# 25、删除“002”同学的“001”课程的成绩；