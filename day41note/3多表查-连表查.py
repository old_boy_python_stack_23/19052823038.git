# 联表查询
    # select * from employee,department where dep_id = department.id
    # 两张表联查,先把两表做笛卡尔积.
    # 然后根据用户给的条件进行筛选.

     # 内连接 inner join
        # select* from 表1 inner join 表2 on 条件
        # select * from employee inner join department on dep_id = department.id
            # 表1和表2能对应的数据显示. 不能对应的数据都不显示.

    # 外连接
        # 左外连接
            # select * from 表1 left join 表2 on 条件
        # 右外连接
            # select * from 表1 right join 表2 on 条件
        # 全外连接,   左外连,右外连,并表去重. 实现 full join
        #     select * from employee left join department on dep_id = department.id
        #     union
        #     select * from employee right join department on dep_id = department.id

    # 连表查的语法顺序,d和e是表的简写.
    # select e.name,d.name from employee e inner join department d on d.id = dep_id where age>25;
    # 年龄大于25的员工及员工部门
    # select * from employee e inner join department d on d.id = dep_id order by age;
    #

