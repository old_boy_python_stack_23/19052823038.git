# 1、查询男生、女生的人数；
# select gender,count(sid) from student group by gender;
# 女     |          6 |
# | 男     |         10
# 2、查询姓“张”的学生名单；
# select sname from student where sname like '张%';
# 张三   |
# | 张一   |
# | 张二   |
# | 张四   |
# 3、课程平均分从高到低显示
#select cname,avg(num) from course c inner join score s on course_id=cid group by cid order by avg(num) desc;
#+--------+----------+
# | cname  | avg(num) |
# +--------+----------+
# | 美术   |  85.2500 |
# | 物理   |  65.0909 |
# | 体育   |  64.4167 |
# | 生物   |  53.4167 |
# +--------+----------+
# 4、查询有课程成绩小于60分的同学的学号、姓名；
# select t.sid,sname from (
# select student.sid,sname,min(num) as min_num from
# student inner join score on student_id = student.sid group by student.sid) as t where min_num <60;
# +-----+--------+
# | sid | sname  |
# +-----+--------+
# |   1 | 理解   |
# |   2 | 钢蛋   |
# |   4 | 张一   |
# |   5 | 张二   |
# |   6 | 张四   |
# |   7 | 铁锤   |
# |   8 | 李三   |
# |   9 | 李一   |
# |  10 | 李二   |
# |  11 | 李四   |
# |  12 | 如花   |
# +-----+--------+
# 5、查询至少有一门课与学号为1的同学所学课程相同的同学的学号和姓名；
# select sid,sname from student where sid in
# (select student_id from score where course_id in
# (select course_id from score where student_id =1)) order by sid
# | sid | sname  |
# +-----+--------+
# |   1 | 理解   |
# |   2 | 钢蛋   |
# |   3 | 张三   |
# |   4 | 张一   |
# |   5 | 张二   |
# |   6 | 张四   |
# |   7 | 铁锤   |
# |   8 | 李三   |
# |   9 | 李一   |
# |  10 | 李二   |
# |  11 | 李四   |
# |  12 | 如花   |
# 6、查询出只选修了一门课程的全部学生的学号和姓名；
# select s.student_id,sname from student st inner join score s on s.student_id = st.sid group by sname
# having count(num) = 3;
# sid | sname  |
# +-----+--------+
# |  13 | 刘三
# 7、查询各科成绩最高和最低的分：以如下形式显示：课程ID，最高分，最低分；
# select cid,max(num),min(num) from course inner join score on course_id = cid group by cid
# +-----+----------+----------+
# | cid | max(num) | min(num) |
# +-----+----------+----------+
# |   1 |       91 |        8 |
# |   2 |      100 |        9 |
# |   3 |       87 |       43 |
# |   4 |      100 |       22 |
# +-----+----------+----------+
# 8、查询课程编号“2”的成绩比课程编号“1”课程低的所有同学的学号、姓名；
# select sid,sname from student where sid in (
# select t1.student_id from
# (select * from score where course_id = 1) as t1 inner join
# (select * from score where course_id = 2) as t2 on t1.student_id =t2.student_id
# where t1.num > t2.num
# )
# +-----+--------+
# | sid | sname  |
# +-----+--------+
# |   1 | 理解   |
# |   3 | 张三   |
# |   4 | 张一   |
# |   5 | 张二   |
# |   9 | 李一   |
# |  10 | 李二   |
# |  11 | 李四   |
# |  12 | 如花   |
# +-----+--------+
# 9、查询“生物”课程比“物理”课程成绩高的所有学生的学号；
# select sid,sname from student where sid in (
# select t1.student_id from
# (select * from score where course_id = (select cid from course where cname ='生物')) as t1 inner join
# (select * from score where course_id = (select cid from course where cname ='物理')) as t2
# on t1.student_id =t2.student_id
# where t1.num > t2.num
# )
# +-----+--------+
# | sid | sname  |
# +-----+--------+
# |   1 | 理解   |
# |   3 | 张三   |
# |   4 | 张一   |
# |   5 | 张二   |
# |   9 | 李一   |
# |  10 | 李二   |
# |  11 | 李四   |
# |  12 | 如花   |
# +-----+--------+

# 10、查询平均成绩大于60分的同学的学号和平均成绩;
# select student_id,avg(num) as avg_num from score group by student_id having avg(num) > 60 order by avg(num) desc
# +------------+---------+
# | student_id | avg_num |
# +------------+---------+
# |         13 | 87.0000 |
# |          3 | 82.2500 |
# |         10 | 74.2500 |
# |         11 | 74.2500 |
# |         12 | 74.2500 |
# |          6 | 69.0000 |
# |          9 | 67.0000 |
# |          7 | 66.0000 |
# |          8 | 66.0000 |
# |          5 | 64.2500 |
# |          4 | 64.2500 |
# +------------+---------+
# 11、查询所有同学的学号、姓名、选课数、总成绩；
# select s.sid,sname,count(num),sum(num) from student s
# inner join score on student_id = s.sid group by s.sid;
# +-----+--------+------------+----------+
# | sid | sname  | count(num) | sum(num) |
# +-----+--------+------------+----------+
# |   1 | 理解   |          3 |       85 |
# |   2 | 钢蛋   |          3 |      175 |
# |   3 | 张三   |          4 |      329 |
# |   4 | 张一   |          4 |      257 |
# |   5 | 张二   |          4 |      257 |
# |   6 | 张四   |          4 |      276 |
# |   7 | 铁锤   |          4 |      264 |
# |   8 | 李三   |          4 |      264 |
# |   9 | 李一   |          4 |      268 |
# |  10 | 李二   |          4 |      297 |
# |  11 | 李四   |          4 |      297 |
# |  12 | 如花   |          4 |      297 |
# |  13 | 刘三   |          1 |       87 |
# +-----+--------+------------+----------+
# 12、查询姓“李”的老师的个数；
# select count(tid) from teacher where tname like '李%';
# +------------+
# | count(tid) |
# +------------+
# |          2 |
# +------------+
# 13、查询没学过“张磊老师”课的同学的学号、姓名；
# select sid,sname from student
# where sid not in (select student_id from score where course_id =
# (select cid from course inner join teacher on tid = teacher_id where tname = '张磊老师' group by cid)
# )
# +-----+--------+
# | sid | sname  |
# +-----+--------+
# |  13 | 刘三   |
# |  14 | 刘一   |
# |  15 | 刘二   |
# |  16 | 刘四   |
# +-----+--------+
# 14、查询学过“1”并且也学过编号“2”课程的同学的学号、姓名；
# select sid,sname from student
# where sid in
# (select student_id from score
# where student_id in
# (select student_id from score where course_id = 1) and course_id = 2)
# +-----+--------+
# | sid | sname  |
# +-----+--------+
# |   1 | 理解   |
# |   3 | 张三   |
# |   4 | 张一   |
# |   5 | 张二   |
# |   6 | 张四   |
# |   7 | 铁锤   |
# |   8 | 李三   |
# |   9 | 李一   |
# |  10 | 李二   |
# |  11 | 李四   |
# |  12 | 如花   |
# +-----+--------+
# 15、查询学过“李平老师”所教的所有课的同学的学号、姓名
# select sid,sname from student
# where sid in (select student_id from score where course_id in
# (select cid from course inner join teacher on tid = teacher_id where tname = '李平老师' group by cid)
# )
# +-----+--------+
# | sid | sname  |
# +-----+--------+
# |   1 | 理解   |
# |   2 | 钢蛋   |
# |   3 | 张三   |
# |   4 | 张一   |
# |   5 | 张二   |
# |   6 | 张四   |
# |   7 | 铁锤   |
# |   8 | 李三   |
# |   9 | 李一   |
# |  10 | 李二   |
# |  11 | 李四   |
# |  12 | 如花   |
# +-----+--------+