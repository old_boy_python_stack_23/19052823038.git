# 索引的种类
    # priamry key 的创建自带索引效果     非空 + 唯一 + 聚集索引
    # unique 唯一约束的创建也自带索引效果 唯一+ 辅助索引
    # index 普通的索引    辅助索引

# 创建索引
    # create index ind_name on 表
# 删除索引
    # drop index ind_name on 表

# 索引的优缺点
    # 优点 : 查找速度快
    # 缺点 : 浪费空间
        # 树形结构的整理,不要创建无用的索引.

# 创建了索引之后的效率大幅度提高
# 文件所占的硬盘资源也大幅度提高


# 正确使用索引
    # 1. 所查询的列不是创建了索引的列,就会慢
    # 2. 在条件中不能带运算或者函数,=号左边不能带东西.  右边可以.
    # 3. 如果创建索引的列的内容重复率高也不能有效利用索引
        # 重复率不超过10%的列比较适合做索引
    # 4. 数据对应的范围如果太大的话,也不能有效利用索引
        # between and > >= <= != not in.尝试这些范围时,需要注意.
    # 5. like 的%越靠前,索引越难
    # 6. 多条件的情况
        # and 只要有一个条件列是索引列就可以命中
        # or 只有所有条件列都是索引才能命中索引.
    # 7. 联合索引
        # 如果两个索引都是单独创建的,索引时,只有一个能索引到,并且是索引效率高,结构更简洁的.
        # 在多个条件相连的情况下,使用联合索引的效率要高于使用单字段的索引.
        # where a = xx  and  b = xxx
        # 对a 和b 都创建索引- 联合索引
        # create index ind_mix on s1(id,email)

        # 1. 创建索引的顺序id,email 条件中从哪一个字段开始出现了范围类索引,就失效了.

        # 2. 联合索引在使用中,遵从最左前缀原则

        # 3. 联合索引中只有使用and 能生效,使用or失效.

    # 8 .查询索引可能的情况
        # explain select * from s1 where id = 1000000

    # 9 .覆盖索引,索引合并
        # 覆盖索引
            # explan select id from s1 where id = 100000;
                # Using index 如果查询一个数据,不需要回表就是覆盖索引.
                # 如果需要回表就不是覆盖索引.
        # 索引合并
            # 创建的时候是分开创建的
            # 用的时候临时合在一起用
            # using union 表示索引合并.


# 数据表\库的导入导出
    # 备份单表
    # mysqldump - uroot -p123456 homework > D:\s23\day42\a.sql
    # 备份多表
    # mysqldump - uroot -p123456 homework course > D:\s23\day42\a.sql
    # 恢复单表
    # 进入mysql 切换到要恢复数据的库下面
    # sourse D:\s23\day42\a.sql

    # 备份库:
        # mysqldump - uroot -p123456 --databases homework >D:\s23\day42\a.sql

# 开启事务,给数据加锁
    # begin;
    # select id from t1 where name = 'alex';
    # update t1 set id = 2 where name = 'alex';
    # commit;