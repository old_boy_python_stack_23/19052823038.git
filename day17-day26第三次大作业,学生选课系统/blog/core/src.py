# 创建课程(需要记录日志)。
# 创建学生账号(需要记录日志)。
import sys
import hashlib
import json
import os
import pickle
import logging.config
BASE_PATH = os.path.dirname(os.path.dirname(__file__))
sys.path.append(BASE_PATH)
Login_status_adm= False
Login_status_stu= False
from conf import settings
from lib import common
usename = ['匿名']
class Student:
    Student_list_main = [('show_courses', '查看可选课程'),
                         ('select_course', '选择课程'),
                         ('show_selected_course', '查看已选课程'),
                         ('exit', '退出程序')]
    def __init__(self,name):
        self.name = name
        self.courses = []  #这里是学生选择的课程列表
    def show_courses(self):
        with open(settings.class_dic_pickle, mode='rb') as f1:     #读取源文件
            class_dic_pickle = pickle.load(f1)
            for i in class_dic_pickle:
                print(class_dic_pickle[i].name,class_dic_pickle[i].price,class_dic_pickle[i].period)
    #查看可选课程
    def select_course(self):
        with open(settings.class_dic_pickle, mode='rb') as f1:     #读取源文件
            class_dic_pickle = pickle.load(f1)
            for i in class_dic_pickle:
                print(class_dic_pickle[i].name,class_dic_pickle[i].price,class_dic_pickle[i].period)
        while 1:
            student_select =input('请输入课程名称来选课,输入Q退出').strip()
            if student_select in class_dic_pickle:
                print(f'成功选课{student_select}')
                self.courses.append(student_select)
            elif student_select == 'q' or student_select == 'Q':
                break
            else:
                print('您的输入有误')


    def show_selected_course(self):
        print(self.courses)
    def exit(self):
        exit()
        pass
class Manager:
    manager_list_main = [('create_course', '创建课程')
        , ('create_student', '学生信息录入')
        , ('show_courses', '查看课程')
        , ('show_students', '查看学生')
        , ('show_students_courses', '查看学生选课情况')
        , ('exit', '退出程序')]
    def __init__(self,name):
        self.name = name

    def create_course(self):
        class_list_build = input('请创建课程,格式为:课程名|价格|周期|老师').strip().split('|')
        if os.path.isfile(settings.class_dic_pickle) == False:   #检查课程实例化字典是否存在,不存在则创建一个空的.
            dic_pickle = {}
            with open(settings.class_dic_pickle,mode='wb') as f:
                pickle.dump(dic_pickle,f)
        with open(settings.class_dic_pickle, mode='rb') as f1:     #读取源文件
            dic_pickle = pickle.load(f1)
            dic_pickle[class_list_build[0]] = Course(class_list_build[0],class_list_build[1],class_list_build[2])
        with open(settings.class_dic_pickle, mode='wb') as f2:    #加入新注册的并录入.
            pickle.dump(dic_pickle,f2)
        build_class_print(usename[-1],class_list_build[0])
    #创建课程


    def create_student(self):
        with open(settings.use_name_json, encoding='utf-8', mode='r+') as f:
            dic = json.load(f)
            while 1:
                use_name = input('请输入用户名,只能由字母或数字组成').strip().replace(' ', '')
                if use_name.isalnum():
                    if len(ascii(use_name)) - 2 != len(use_name):
                        print('您输入的内容包含特殊字符,请重新输入')
                        continue
                else:
                    print('您输入的内容包含特殊字符,请重新输入')
                    continue
                if use_name in dic:
                    print('用户名已存在,请重新输入')
                    continue
                else:
                    break
            while 1:
                use_password = input('请输入密码,6到14位').strip().replace(' ', '')
                if 6 < len(use_password) < 14:
                    use_password1 = input('请在次输入密码以便确认,6到14位').strip().replace(' ', '')
                    if use_password == use_password1:
                        break
                    else:
                        print('两次密码不一致,请重试')
                else:
                    print('您输入的密码长度不对,请重试')
            ret = hashlib.md5()
            ret.update(use_password.encode('utf-8'))
            s1 = ret.hexdigest()
            dic[use_name] = [s1,'stu']
            f.seek(0)
            json.dump(dic, f)
        #管理员注册学生,并把学生用户名密码写入文件中.
        if os.path.isfile(settings.student_dic_pickle) == False:   #检查学生实例化字典是否存在,不存在则创建一个空的.
            dic_pickle = {}
            with open(settings.student_dic_pickle,mode='wb') as f:
                pickle.dump(dic_pickle,f)
        with open(settings.student_dic_pickle, mode='rb') as f1:     #读取源文件
            dic_pickle = pickle.load(f1)
            dic_pickle[use_name] = Student(use_name)
        with open(settings.student_dic_pickle, mode='wb') as f2:    #加入新注册的并录入.
            pickle.dump(dic_pickle,f2)
        build_student_print(usename[-1],use_name)

    #管理员注册学生

    def show_courses(self):
        with open(settings.class_dic_pickle, mode='rb') as f1:     #读取源文件
            class_dic_pickle = pickle.load(f1)
            for i in class_dic_pickle:
                print(class_dic_pickle[i].name,class_dic_pickle[i].price,class_dic_pickle[i].period)

    def show_students(self):
        student_li = []
        with open(settings.student_dic_pickle, mode='rb') as f1:     #读取源文件
            student_dic_pickle = pickle.load(f1)
            for i in student_dic_pickle:
                student_li.append(i)
            print(student_li)

    def show_students_courses(self):
        with open(settings.student_dic_pickle, mode='rb') as f1:     #读取源文件
            student_dic_pickle = pickle.load(f1)
            for i in student_dic_pickle:
                print(student_dic_pickle[i].name,student_dic_pickle[i].courses)

    def exit(self):
        exit()
def use_load():
    count = 1
    while count < 4:
        use_load_name = input('请输入您的用户名').strip()
        use_load_psd = input('请输入您的密码').strip()
        ret = hashlib.md5()
        ret.update(use_load_psd.encode('utf-8'))
        s1 = ret.hexdigest()
        with open(settings.use_name_json, encoding='utf-8', mode='r') as f:
            dic = json.load(f)
            if use_load_name in dic:
                if dic[use_load_name][0] == s1:
                    usename.append(use_load_name)
                    if dic[use_load_name][1] =='adm':
                        print(f'{usename[-1]}管理员登录成功')
                        global Login_status_adm
                        Login_status_adm = True
                    else:
                        print(f'{usename[-1]}同学登录成功')
                        global Login_status_stu
                        Login_status_stu = True
                    break
                else:
                    print('您输入的用户名或密码错误,请重试')
                    count += 1
            else:
                print('您输入的用户名或密码错误,请重试')
                count += 1
    else:
        print('您输入的错误次数超过三次,程序自动退出.')
#登录函数,
@common.wrapper
def register():
    if os.path.isfile(settings.use_name_json) == False:
        dic = {}
        with open(settings.use_name_json, encoding='utf-8', mode='w') as f:
            json.dump(dic, f)
    with open(settings.use_name_json,encoding='utf-8',mode='r+') as f:
        dic = json.load(f)
        while 1:
            use_name = input('请输入用户名,只能由字母或数字组成').strip().replace(' ','')
            if use_name.isalnum():
                if len(ascii(use_name))-2 != len(use_name):
                    print('您输入的内容包含特殊字符,请重新输入')
                    continue
            else:
                print('您输入的内容包含特殊字符,请重新输入')
                continue
            if use_name in dic:
                print('用户名已存在,请重新输入')
                continue
            else:
                break
        while 1:
            use_password = input('请输入密码,6到14位').strip().replace(' ','')
            if 6 < len(use_password) < 14:
                use_password1 = input('请在次输入密码以便确认,6到14位').strip().replace(' ','')
                if use_password == use_password1:
                    break
                else:
                    print('两次密码不一致,请重试')
            else:
                print('您输入的密码长度不对,请重试')
        ret = hashlib.md5()
        ret.update(use_password.encode('utf-8'))
        s1 = ret.hexdigest()
        dic[use_name]=[s1,'adm']
        f.seek(0)
        json.dump(dic, f)
        if os.path.isfile(settings.manager_dic_pickle) == False:   #检查学生实例化字典是否存在,不存在则创建一个空的.
            manager_dic_pickle = {'': ''}
            with open(settings.manager_dic_pickle,mode='wb') as f:
                pickle.dump(manager_dic_pickle,f)
        with open(settings.manager_dic_pickle, mode='rb') as f1:     #读取源文件
            manager_dic_pickle = pickle.load(f1)
            manager_dic_pickle[use_name] = Manager(use_name)
        with open(settings.manager_dic_pickle, mode='wb') as f2:    #加入新注册的并录入.
            pickle.dump(manager_dic_pickle,f2)
#注册管理员  OK了
class Course:
    def __init__(self,name,price,period):
        self.name = name
        self.price = price
        self.period = period
        self.teacher = None
def login():
    pass
def main():
    # use_load()#登录函数
    # register()#管理员注册
    load_register = [('use_load','登录'),('register','注册管理员')]

    while 1:
        if Login_status_adm:
            with open(settings.manager_dic_pickle, mode='rb') as f1:     #读取源文件
                manager_dic_pickle = pickle.load(f1)
            for num,option in enumerate(manager_dic_pickle[usename[-1]].manager_list_main,1):
                print(num,option[1])
            while 1:
                # try:
                choice_num = int(input('请输入序号').strip())
                if hasattr(manager_dic_pickle[usename[-1]],
                           manager_dic_pickle[usename[-1]].manager_list_main[choice_num-1][0]):
                    getattr(manager_dic_pickle[usename[-1]],
                            manager_dic_pickle[usename[-1]].manager_list_main[choice_num-1][0])()
                # except Exception:
                    # print('您输入的内容有误')


        elif Login_status_stu:
            with open(settings.student_dic_pickle, mode='rb') as f1:  # 读取源文件
                student_dic_pickle = pickle.load(f1)
            for num, option in enumerate(student_dic_pickle[usename[-1]].Student_list_main,1):
                print(num, option[1])
            print('10 保存')
            while 1:
                choice_num = int(input('请输入序号').strip())
                if choice_num == 10:
                    with open(settings.student_dic_pickle, mode='wb') as f1:  # 读取源文件
                        pickle.dump(student_dic_pickle,f1)
                elif hasattr(student_dic_pickle[usename[-1]],
                           student_dic_pickle[usename[-1]].Student_list_main[choice_num - 1][0]):
                    getattr(student_dic_pickle[usename[-1]],
                            student_dic_pickle[usename[-1]].Student_list_main[choice_num - 1][0])()

        else:
            for num, option in enumerate(load_register,1):
                print(num, option[1])
            while 1:
                    choice_num = input('请输入序号').strip()
                    if choice_num == '1':
                        use_load()
                        break
                    elif choice_num == '2':
                        register()
                        break


standard_format = '[%(asctime)s][%(threadName)s:%(thread)d][task_id:%(name)s][%(filename)s:%(lineno)d]' \
                  '[%(levelname)s][%(message)s]' #其中name为getlogger指定的名字      复杂的日志格式
simple_format = '[%(levelname)s][%(asctime)s][%(filename)s:%(lineno)d]%(message)s'   #适中的日志格式
id_simple_format = '[%(levelname)s][%(asctime)s] %(message)s'#简易的日志格式
# 定义日志输出格式 结束
# logfile_dir = os.path.dirname(os.path.abspath(__file__)) #log文件的目录
logfile_name = 'login.log'  # log文件名
logfile_path = r'D:\python\Source code\第三次大作业,学生选课系统\blog\log\log1'
logfile_path_boss = r'D:\python\Source code\第三次大作业,学生选课系统\blog\log\admlog1'
#log配置字典
LOGGING_DIC = {
    'version':1,#版本号
    'disable_existing_loggers':False, #固定写法,大概意思是,我生成的子日志都沿用这个字典
    'formatters':{   #这个顾名思义是格式.
        'standard':{#这里是格式的第一个,是供调用者选择的格式选项.
            'format':standard_format #这里是第一个选项里的内容,两层字典
        },
        'simple':{#这个是适中版,
            'format':simple_format
        },
        'id_simple':{
            'format':id_simple_format
        }
    },
    'filters':{},#这里是过滤器,目前没讲,空字典,先不管
    'handlers':{#句柄文件,配置日志输出目标,可以定义多个屏幕以及多个文件,灵活选择.
        'sh':{#日志名,里面是配置,打印到终端需要配置的内容相对较少
            'level':'DEBUG',
            'class':'logging.StreamHandler',#输出目标,是输出到终端.可以修改输出目标到文件等
            'formatter':'simple'
        },
        'fh':{#输出到文件中,名称可修改,内容配置.对文件的操作比较多
            'level':'DEBUG',#同上
            'class':'logging.handlers.RotatingFileHandler',#保存到文件
            'formatter':'standard',
            'filename':logfile_path,#日志文件.我们上面定义过这个变量就是写一个这样的文件
            'maxBytes':500, #日志大小,5000个字节.可写成1024*1024*5这种格式,这个是5M,
            'backupCount':5,#滚动备份文件数量,当前日志文件不占用这个数量,所以一共是5个满的和当前这个没满的
            'encoding':'utf-8',#日志文件的编码,在这里可以指定,不用担心乱码的问题了
        },
        'boss':{
            'level':'DEBUG',#同上
            'class':'logging.handlers.RotatingFileHandler',#保存到文件
            'formatter':'id_simple',
            'filename':logfile_path_boss,#日志文件.我们上面定义过这个变量就是写一个这样的文件
            'maxBytes':300, #日志大小,5000个字节.可写成1024*1024*5这种格式,这个是5M,
            'backupCount':2,#滚动备份文件数量,当前日志文件不占用这个数量,所以一共是5个满的和当前这个没满的
            'encoding':'utf-8',#日志文
        },
    },
    'loggers':{#logging.getlogger(__name__)拿到logger配置
        '': {
            'handlers':['sh','fh','boss'], #这里把上面定义的两个handler都加上,即log数据既写入文件又打印到屏幕
            'level':'DEBUG',#这里是log级别的总开关?没细听,不知道讲没讲,可尝试
            'propagate':True, #向上(更高的level的logger)传递
        },
    },
}
def build_class():
    logging.config.dictConfig(LOGGING_DIC)
    logger = logging.getLogger('学生选课系统')  #生成一个log实例,调用这个实例logger.后面是级别.
    # logger.debug('It works!')   #括号内填写任意信息,都会在日志最后输出.可利用后面的函数来添加用户信息等等.
    return logger
def build_student():
    logging.config.dictConfig(LOGGING_DIC)
    logger = logging.getLogger('学生选课系统')  #生成一个log实例,调用这个实例logger.后面是级别.
    # logger.debug('It works!')   #括号内填写任意信息,都会在日志最后输出.可利用后面的函数来添加用户信息等等.
    return logger
dic = {
    'username':'小黑'
}
def build_class_print(name1,name2):
    build_class().info(f"管理员{name1}添加了{name2}课程")
def build_student_print(name1,name2):
    build_class().info(f"管理员{name1}注册了学生{name2}")

def run():
    main()



