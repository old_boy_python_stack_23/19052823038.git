import sys
import os
BASE_PATH = os.path.dirname(os.path.dirname(__file__))
sys.path.append(BASE_PATH)
from core import src
from conf import settings
if __name__ == '__main__':
    src.run()
