import hashlib
def wrapper(f):
    def inner():
        invitation = input('管理员注册需要邀请码,请跟老男孩教学部联系,如果您有邀请码,请直接输入').strip()
        ret = hashlib.md5()
        ret.update(invitation.encode('utf-8'))
        s1 = ret.hexdigest()
        if s1 =='4b89f409f8450a2133fb3d0153e395ca':
            f()
        else:
            print('您输入的验证码不对,请重试,或联系老男孩教学部')
            exit()
    return inner
#验证码装饰器


