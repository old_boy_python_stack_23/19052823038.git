# from multiprocessing import Process
# import time
# def task(name):
#     print(f'{name} 开始了')
#     time.sleep(3)
#     print(f'{name} 完事了')
# if __name__ == '__main__':  #windows环境下,开启多进程一定放在这个下面
#     p = Process(target=task,args=('***',))  #args一定是一个元组形式
#     p.start()
#     # 通知操作系统,你给我在内存中开辟一个空间,将p这个进程放进去,然后让cpu执行
#     print('===主进程')


from multiprocessing import Process
import time
def task(name):
    print(f'{name} 开始了')
    time.sleep(1)
    print(f'{name} 完事了')

def task1(name):
    print(f'{name} 开始了')
    time.sleep(2)
    print(f'{name} 完事了')

def task2(name):
    print(f'{name} 开始了')
    time.sleep(3)
    print(f'{name} 完事了')
def task3(name):
    print(f'{name} 开始了')
    time.sleep(3)
    print(f'{name} 完事了')
if __name__ == '__main__':  #windows环境下,开启多进程一定放在这个下面
    p = Process(target=task,args=('函数1秒',))  #args一定是一个元组形式
    p1 = Process(target=task1,args=('函数2秒',))  #多个子进程随机顺序瞬间先后开启
    p2 = Process(target=task2,args=('函数3秒',))
    p3 = Process(target=task3,args=('第二个函数3秒',))
    p.start()
    p1.start()
    p2.start()
    p3.start()
    # 通知操作系统,你给我在内存中开辟一个空间,将p这个进程放进去,然后让cpu执行
    print('===主进程')
    time.sleep(1)
    print('主进程睡完1秒出来')
    time.sleep(1)
    print('主进程睡完2秒出来')
    time.sleep(1)
    print('主进程睡完3秒出来')
    time.sleep(1)
    print('主进程睡完4秒出来')
