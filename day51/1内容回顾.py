# 标签的文本操作
    # jq对象.text('想添加的内容')只能识别文本
    # jq对象.html('标签')jq对象和dom对象都可以识别.
# 标签的操作
    # 增
        # 夫子关系
            # 父.append(子)父末尾追加子
            # 子.appendTo(父)子追加在父末尾
            # 父.prepend(子)父头追加子
            # (子).prependTo(父)子追加在父头

            # 兄.after(要添加的弟)兄后面添加弟
            # (要添加的弟).insertAfter(兄)
            # 弟.befone(要添加的兄)弟前面添加兄
            #
    # 删
        # remove:删除所有,返回被删除的标签,删除返回后,事件被清除
        # detach:删除标签,返回值保留标签及事件
        # emtpy: 清空标签
    # 改
        # 被改.replaceWith(改后)
        # 改后.replaceAll(被改)
    # 克隆
        # clone()浅拷贝
        # clone(True)深拷贝
# 标签的属性操作
    # 通用属性
        # attr
            # attr('属性名')获取属性值
            # attr('属性名','属性值')设置一个属性
            # attr({'属性名':'属性值1','属性值2'...})设置多个属性

        # removeAttr('属性名')删除某个属性

        # prop
            # 针对true/false两种值的
            # checkbox redio
# css
    # dom.style.color = 'red'
    # $(obj).css('color','red')
    # $(obj).css({'color':'red'})
# 类操作
    # addClass('box c1')
    # removeClass('box')
    # toggleClass('box')
# val
    # $ 对象.val() 获取值 针对表单内元素
    # $ val('内容') 设置值 针对表单内非选择框
    # val ([1,2,3]) 其中1,2,3是选项的value
# 盒子样式
    # width 内容 # height
    # innerWidth # innerHeight 内容加padding宽高
    # outerWidth # outerHeight 内容加padding 加border宽高
    # outerWidth(True) # outerHeight(True) 内容加padding 加border宽高加margin
# 滚动条
    # scrollTop  距离顶部的  窗口滚动条
    # scrollLeft 水平滚动条距离左侧的距离
# 表单
    # 给submit 绑定事件
    # 如果给对应的函数返回值是false,表单就不提交了.
