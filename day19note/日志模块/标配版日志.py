import logging
#创建一个logging对象
logger = logging.getLogger()
#创建一个文件对象
fh = logging.FileHandler('标配版.log',encoding = 'utf-8')
#创建一个屏幕对象
sh = logging.StreamHandler()
#配置显示格式,可配置多种,随时调用.
formatter = logging.Formatter( '%(asctime)s  %(filename)s  [line:%(lineno)d] %(levelname)s  %(message)s')
formatter1 = logging.Formatter('%(asctime)s  [line:%(lineno)d]  %(message)s')

#格式绑定到文件和屏幕上
fh.setFormatter(formatter)
sh.setFormatter(formatter1)
#把句柄绑定到两个logger里.
logger.addHandler(fh)
logger.addHandler(sh)
#报错级别总开关,确定报错可选范围
logger.setLevel(10)
#屏幕报错和文件报错级别设定
fh.setLevel(10)
sh.setLevel(10)

logging.debug('debug message')
logging.info('info message')
logging.warning('warning message')
logging.error('error message')
logging.critical('critical message')
