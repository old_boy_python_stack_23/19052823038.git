import logging
logging.basicConfig(
    level = 10,
    format = '%(asctime)s%(filename)s[line:%(lineno)d]%(levelname)s%(message)s',
    filename=r'test.log'
)
logging.debug('debug message')
logging.info('info message')
logging.warning('warning message')
logging.error('error message')
logging.critical('critical message')

# try:
#     i = input('请输入内容')
#     int(i)
# except Exception as e:
#     logging.error(e)