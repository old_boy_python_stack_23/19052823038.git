import logging.config
standard_format = '[%(asctime)s][%(threadName)s:%(thread)d][task_id:%(name)s][%(filename)s:%(lineno)d]' \
                  '[%(levelname)s][%(message)s]' #其中name为getlogger指定的名字      复杂的日志格式
simple_format = '[%(levelname)s][%(asctime)s][%(filename)s:%(lineno)d]%(message)s'   #适中的日志格式
id_simple_format = '[%(levelname)s][%(asctime)s] %(message)s'#简易的日志格式
# 定义日志输出格式 结束
# logfile_dir = os.path.dirname(os.path.abspath(__file__)) #log文件的目录
logfile_name = 'login.log'  # log文件名
logfile_path = r'D:\python\Source code\day19\日志模块\旗舰版日志方法\旗舰版日志文件夹\log1'
logfile_path_boss = r'D:\python\Source code\day19\日志模块\旗舰版日志方法\旗舰版日志文件夹\boss\bosslog1'
#log配置字典
LOGGING_DIC = {
    'version':1,#版本号
    'disable_existing_loggers':False, #固定写法,大概意思是,我生成的子日志都沿用这个字典
    'formatters':{   #这个顾名思义是格式.
        'standard':{#这里是格式的第一个,是供调用者选择的格式选项.
            'format':standard_format #这里是第一个选项里的内容,两层字典
        },
        'simple':{#这个是适中版,
            'format':simple_format
        },
        'id_simple':{
            'format':id_simple_format
        }
    },
    'filters':{},#这里是过滤器,目前没讲,空字典,先不管
    'handlers':{#句柄文件,配置日志输出目标,可以定义多个屏幕以及多个文件,灵活选择.
        'sh':{#日志名,里面是配置,打印到终端需要配置的内容相对较少
            'level':'DEBUG',
            'class':'logging.StreamHandler',#输出目标,是输出到终端.可以修改输出目标到文件等
            'formatter':'simple'
        },
        'fh':{#输出到文件中,名称可修改,内容配置.对文件的操作比较多
            'level':'DEBUG',#同上
            'class':'logging.handlers.RotatingFileHandler',#保存到文件
            'formatter':'standard',
            'filename':logfile_path,#日志文件.我们上面定义过这个变量就是写一个这样的文件
            'maxBytes':500, #日志大小,5000个字节.可写成1024*1024*5这种格式,这个是5M,
            'backupCount':5,#滚动备份文件数量,当前日志文件不占用这个数量,所以一共是5个满的和当前这个没满的
            'encoding':'utf-8',#日志文件的编码,在这里可以指定,不用担心乱码的问题了
        },
        'boss':{
            'level':'DEBUG',#同上
            'class':'logging.handlers.RotatingFileHandler',#保存到文件
            'formatter':'id_simple',
            'filename':logfile_path_boss,#日志文件.我们上面定义过这个变量就是写一个这样的文件
            'maxBytes':300, #日志大小,5000个字节.可写成1024*1024*5这种格式,这个是5M,
            'backupCount':2,#滚动备份文件数量,当前日志文件不占用这个数量,所以一共是5个满的和当前这个没满的
            'encoding':'utf-8',#日志文
        },
    },
    'loggers':{#logging.getlogger(__name__)拿到logger配置
        '': {
            'handlers':['sh','fh','boss'], #这里把上面定义的两个handler都加上,即log数据既写入文件又打印到屏幕
            'level':'DEBUG',#这里是log级别的总开关?没细听,不知道讲没讲,可尝试
            'propagate':True, #向上(更高的level的logger)传递
        },
    },
}
def md_logger():
    logging.config.dictConfig(LOGGING_DIC)
    logger = logging.getLogger('购物车日志')  #生成一个log实例,调用这个实例logger.后面是级别.
    # logger.debug('It works!')   #括号内填写任意信息,都会在日志最后输出.可利用后面的函数来添加用户信息等等.
    return logger
dic = {
    'username':'小黑'
}
def login():
    md_logger().info(f"{dic['username']}登录成功")
# login()
md_logger().debug('It works')