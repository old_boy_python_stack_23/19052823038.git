''''''
#第一类:执行文件 通过import 导入包及包内的功能
#创建一个aaa的包,创建包的同时,包里会自动创建一个__init__.py的文件
#会议:创建一个tbjx模块发生的三件事
'''
    1.将该tbjx文件加载到内存
    2创建一个以tbjx明明的名称空间
    3通过tbjx.的方式引用tbjx模块的所有的名字.
'''
#创建一个包,发生的三件事.:
'''
    1.将该aaa的包内的__init__.py的文件加载到内存.
    2创建一个以aaa命名的名称空间
    3.通过aaa.的方式,引用__init__的所有的名字.



    问:引用执行文件目录所在的aaa包里的bbb包里的b1文件里的funcb1函数.
    1在执行文件写入import aaa
    2在aaa的init文件里写from aaa import bbb
    3在bbb的init文件里写from aaa.bbb import b1
    4然后通过 aaa.bbb.b1.funcb1()调用funcb1这个函数.

无论从哪引用模块,import或者from...import..
最开始的模块或者包名一定要是sys.path能找到的.


'''
#from    两个中间的a.b.c,这个c前面一定是个包.c可以是包可以不是包
# import后面不能再有.必须直接用东西,可以是包也可以是文件,也可以是函数
from aaa.bbb.b1 import func
# aaa.m1.func1()
# aaa.m2
func()