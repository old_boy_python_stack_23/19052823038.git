''''''
'''
写函数，让用户输入用户名密码，将密码转化成密文，然后构建一个字典，字典的键为用户名，
值为其对应的密码，将这个字典以json字符串的形式写入文件中。

利用递归寻找文件夹中所有的文件，并将这些文件的绝对路径添加到一个列表中返回（面试题，有点难，可先做其他）。

写函数：用户输入某年某月，判断这是这一年的第几天（需要用Python的结构化时间）。

结构化时间可以通过这样取值：

import time
ret = time.localtime()
print(ret)  # time.struct_time(tm_year=2019, tm_mon=6, tm_mday=28, tm_hour=15, tm_min=50, tm_sec=47, tm_wday=4, tm_yday=179, tm_isdst=0)
2019
print(ret.tm_year)  # 2019
写函数，生成一个4位随机验证码（包含数字大小写字母）

'''