## 内容回顾

1. django的命令

   1. 下载

      pip3 install django==1.11.23 -i

   2. 创建项目

      切入路径，python django-admin startproject 项目名

   3. 启动项目

      cd 到项目的根目录

      python manage.py runserver 0.0.0.0:80 

   4. 创建app

      python manage.py startapp app名称

   5. 数据库迁移

      python manage.py makemigrations #扫描变化并创建迁移文件

      python manage.py migrate

2. django配置

   1. 注册app

      直接写app的文件名

      app01.apps.App01Config

   2. DATABASES

      1. 引擎 mysql
      2. name:数据库名
      3. HOST:127.0.0.1
      4. PORT:3306
      5. USER:root
      6. PASSWORD:123

   3. 静态文件

      1. STATIC_URL='/static/'

      2. STATICFILES_DIRS=[

         os.path.join(BASE_DIR,'static')]

   4. 中间件

      注释掉csrf,提交POST请求时,不用校验了.

   5. 模板

      TEMPLATES

      DIRS=[os.path.join(BASE_DIR,templates)

3. django使用mysql数据库的流程

   1. init文件中,通过pymysql指定数据库

   2. 在数据库中建库,

   3. 通过上面的settings文件的DATABASE,name指定新库.

   4. models.py中写类

      class publisher(models.Model):

      ​	name=models.charfile(max_length=32)

   5. 迁移命令

      python manage.py makemigrations

      python manage.py migrate

4. get和post区别

   ​	默认发送的请求都是get

   ​	a标签

5. orm

   面向对象和关系型数据库的一种映射关系

   对应关系

   ​		类 ---表

   ​		对象---行数据

   ​		属性---字段

   查询:

   from app01 import models导入.

   models.类.objects.get 有且唯一,不然报错

   models.类.objects.filter 查询满足条件的所有内容 对象列表queryset

   .all() 查询所有数据.

   新增:

   ​	.create(name=xxxx) 返回值是新建的对像

   ​	obj= models.publisher(name='xxxx')

   删除

   ​	models.Publisher.objects.filter(pk=1).delete()

   ​	obj = models.Publisher.objecs.filter(pk=1).first() obj.delete()

   修改:

   ​	obj=models.publisher.objects.filter(pk=1).first()

   ​	obj.name='xxx'

   ​	obj.save()

6. 模板的语法

   render(request,'页面地址',{'k1':v1}

   1. 变量

      {{  k1 }}

      for 循环 

      {%  for i in  k1 %}

      ​	{{  forloop.counter }}

      ​	{{ i }}

      {% endfor %}

      

      外键

      一对多 

      图书管理系统

      出版社

      书

      外键的设计

      ```python
      class Book(models.Model):
          title = models.CharField(max_length=32)
          pid = models.ForeignKey('Publisher', on_delete=models.CASCADE) # 外键 
          #  on_delete  2.0 必填 
         
      ```

   查询：

   ```
   all_books = models.Book.objects.all()
   print(all_books)
   for book in all_books:
       print(book)
       print(book.pk)
       print(book.title)
       print(book.pub,type(book.pub))  # 所关联的对象 
       print(book.pub_id,type(book.pub_id))  # 所关联的对象的pk
       print('*' * 32)
   ```

   新增

   ```
   models.Book.objects.create(title=title,pub=models.Publisher.objects.get(pk=pub_id))
   models.Book.objects.create(title=title, pub_id=pub_id)
   
   ```

   删除：

   ```
   pk = request.GET.get('pk')
   models.Book.objects.filter(pk=pk).delete()
   ```

   编辑：

   ```
   book_obj.title = title
   book_obj.pub_id = pub_id
   # book_obj.pub = models.Publisher.objects.get(pk=pub_id)
   book_obj.save()
   ```

   