from multiprocessing import  Queue
from multiprocessing import  Process
import time
def func1(q):
    # time.sleep(1)
    ret = q.get()
    q.put('我是func1加塞进去的,如果我进去了.')
    print(ret)
    # time.sleep(1)

if __name__ == '__main__':
    q = Queue(3)
    q.put('alex')
    q.put({'count': 1})
    q.put('taibai')
    print(222)
    p = Process(target=func1,args=(q,))
    p.start()
    # time.sleep(1)
    q.put(666)
    print(111)
    print(q.get())
    print(q.get())
    print(q.get())
    print(q.get())

# 1. maxsize()  数据量不宜过大,精简的重要信息
# 2. put block  默认为True 当你插入的数据超过最大限度,默认阻塞.
# 3. put timeout() 参数,  延时一定时间再次put,不进数据就会报错.
