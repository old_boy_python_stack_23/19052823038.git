from multiprocessing import Process
from multiprocessing import Lock
import time
import random

def task1(lock):
    # print(('task1'))
    lock.acquire()
    print('task1:开始打印')
    # time.sleep(random.randint(1,3))
    time.sleep(1)
    print('task1:打印完成')
    lock.release()

def task2(lock):
    # print(('task2'))
    lock.acquire()
    print('task2:开始打印')
    # time.sleep(random.randint(1,3))
    time.sleep(2)
    print('task2:打印完成')
    lock.release()

def task3(lock):
    # print(('task3'))
    lock.acquire()
    print('task3:开始打印')
    # time.sleep(random.randint(1,3))
    time.sleep(3)
    print('task3:打印完成')
    lock.release()

if __name__ == '__main__':

    lock = Lock()
    p1 = Process(target=task1,args=(lock,))
    p2 = Process(target=task2,args=(lock,))
    p3 = Process(target=task3,args=(lock,))

    p1.start()
    p2.start()
    p3.start()
    print('遇到锁,非锁的代码执行情况?开始')
    time.sleep(1)
    print('遇到锁,非锁的代码执行情况?睡1秒出来')
    time.sleep(1)
    print('遇到锁,非锁的代码执行情况?睡2秒出来')
    time.sleep(1)
    print('遇到锁,非锁的代码执行情况?睡3秒出来')
