from multiprocessing import Process
from multiprocessing import Lock
import time
import json
import os
import random

def search():
    time.sleep(random.random())
    with open('db.json',encoding='utf-8') as f:
        dic = json.load(f)
    print(f'剩余票数{dic["count"]}')

def get():
    with open('db.json',encoding='utf-8') as f1:
        dic = json.load(f1)
    time.sleep(random.randint(1,3))
    if dic['count'] > 0:
        dic['count'] -=1
        with open('db.json',encoding='utf-8',mode = 'w')as f1:
            json.dump(dic,f1)
        print(f'{os.getpid()}用户购买成功')
    else:
        print(f'没票了...{os.getpid()}用户没买着票')
def task(lock):
    search()
    lock.acquire()
    get()
    lock.release()

def refresh():
    n = int(input('您是上帝,准备放几张票给大家抢?'))
    dic = {'count':n}
    with open('db.json', encoding='utf-8', mode='w')as f1:
        json.dump(dic, f1)

if __name__ == '__main__':
    lock = Lock()
    for i in range(5):
        p = Process(target=task,args=(lock,))
        p.start()
    refresh()



