# select * from 表名;
# select 字段名 from 表名;
# select 字段1,字段2,字段3 from 表名;
# select user()
# select database()
# select now()
# select distinct 字段 from 表名; 去重.
# select distinct 字段1,字段2 from 表名;   #联合去重.
# select 字段1,字段2*5 from  字段2如果是数字,可以用四则运算.
# select concat('姓名:',字段1,'自定义',字段2) from 表名;  把表里面的字段和自己定义的东西拼接起来显示.
# select concat_ws ('|','信息',emp_name

# select(
#     case
#     when emp_name = 'alex' then
#         concat(emp_name,'BIGSB')
#     when emp_name = 'jingliyang'
#         emp_name
#     else
# )

#单表查询
    # select 某一个东西
        #调用函数 :now() user() database()
        # 进行四则运算
        # 可以去重 distinct
        # 可以进行条件判断  case when else

# where 筛选行
# select * from 表 where 条件
    #范围查询
        # > < >= <= != /< />
        # between a and b
        # in(1,2,3,4) n选1
    # 模糊查询
        # like
            # %    #任意长度任意字符
                # 'a%'
                # '%ing'
                # '%a%
            # _    #一个任意字符
                #'a_'

        # regexp
            # '^a'
            # '\d+'
    # is is not
        # is null
        # is not null
    # 逻辑运算
        # and
        # or
        # not

#  id | emp_name   | sex    | age | hire_date  | post                employee             | post_comment | salary     | office | depart_id |
# +----+------------+--------+-----+------------+-----------------------------------------+--------------+------------+--------+-----------+
# |  1 | egon       | male   |  18 | 2017-03-01 | 老男孩驻沙河办事处外交大使              | NULL         |    7300.33 |    401