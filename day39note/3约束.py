# 联合唯一
# create table t2(
#         id int primary key,
#         servername char(12) not null,
#         ip char (15),
#         port int,
#         unique(ip,port)    #两个关键字,一起唯一,互相不能重复.不能同时一样.
#         )

# insert into t2 values(1,'输入法皮肤','10.10.3.1',8800)
# insert into t2 values(2,'mysql','10.10.2.4',3306)
# insert into t2 values(3,'mysql','10.10.2.5',3306)
# insert into t2 values(4,'输入法皮肤','10.10.3.1',8802)

# 自增的
    # 只能操作数字
    # 自带非空属性
    # 只能对unique 字段进行设置
    # 不受普通删除影响
# create





# 联合主键
# create table t5(
#     family_name char(4),
#     name char(12),
#     primary key(family_name,name)
# );


#外键约束
#foreign key(cid) references class2(id)

# create table student3(
#   id int primary key auto_increment,
#   name char(12) not null,
#   gender enum('male','female'),
#   cid int,
#   foreign key(cid) references class2(id) on delete cascade on update cascade
# );

#级联删除
#on delete cascade

#级联更新
#on update cascade

# create table class3 (
#     id int primary key auto_increment,
# cname char(12) not null unique,
# start_date date,
# preiod char(12),
# course char(12),
# teacher char(12)
# );


#数据的插入操作
# insert into 表1 select * from 表2;
# 从表2把值复制到表1中的对应列.



#表与表的关系
    # 校区表 班级表 一对多
        # 校区表 一个校区可以有多个班级
        # 班级表 一个班级对应一个校区
        # 校区表 校区id 校区名称 校区城市 校区地址
        # 班级表 班级id 班级名称 开班日期 班主任 校区
        # 多(foreign key) 关联一张表
        # 班级表创建foreign key 关联校区表的校区id字段
    # 学生表 班级表 多对多
        # 学生表 学生id 学生姓名
        # 班级表 班级id 班级名称
        # 多对多表
            # 会产生第三张表,放两个关键字.foreign key  级联更新?
