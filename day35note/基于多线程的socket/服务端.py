import socket
phone = socket.socket()
phone.bind(('127.0.0.1',8080))  #本地回环地址
phone.listen(5)
print('start')
conn,addr = phone.accept()
print(conn,addr)
while 1:
    from_client_data = conn.recv(1024)  #至多接受1024字节.
    if from_client_data.decode("utf-8").upper()== 'Q':
        print(f'来自客户端{addr}的消息:{from_client_data.decode("utf-8")}')
        break
    print(f'来自客户端{addr}的消息:{from_client_data.decode("utf-8")}')
    to_client = input('>>>')
    conn.send(to_client.encode('utf-8'))
conn.close()
phone.close()