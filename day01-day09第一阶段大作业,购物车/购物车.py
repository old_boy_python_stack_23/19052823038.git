''''''
'''
2.完成一个商城购物车的程序。
商品信息在shopping.txt文件中存储的，存储形式：
name price
电脑 1999
鼠标 10
游艇 20
美女 998
要求:
1，用户先给自己的账户充钱：比如先充3000元。
2，读取商品信息文件将文件中的数据转化成下面的格式：
goods = [{"name": "电脑", "price": 1999},
{"name": "鼠标", "price": 10},
{"name": "游艇", "price": 20},
{"name": "美女", "price": 998},]
3，页面显示 序号 + 商品名称 + 商品价格，如：
1 电脑 1999
2 鼠标 10
…
4，用户输入选择的商品序号，然后打印商品名称及商品价格,并将此商品，添加到购物车(自己定义购物车)，用户还可继续添加商品。
5，如果用户输入的商品序号有误，则提示输入有误，并重新输入。
6，用户输入n为购物车结算，依次显示用户购物车里面的商品，数量及单价，
若充值的钱数不足，则让用户删除某商品，直至可以购买，
若充值的钱数充足，则可以直接购买。
7，用户输入Q或者q退出程序。
8，退出程序之后，依次显示用户购买的商品，数量，单价，以及此次共消费多少钱，账户余额多少，并将购买信息写入文件。
'''
Recharge_welcome = ''''''
while 1:
    money = input('请输入您的充值金额:(不想充值请输入Q退出程序)')#第一步,确保用户充值成功.
    if money == "Q" or money =='q':
        print("穷B,等赚够了钱再来,谢谢")
        exit()
    if money.isdecimal():
        if int(money)>0:
            money = int(money)
            break
print("你的账户余额是%d"%(money))
li_goods = []
with open('shopping.txt',mode='r',encoding='utf-8') as f:#第二步读取文件信息拿到商品字典.
    for i in f:
        dict_goods = {}
        if i[0] == 'n':
            li = i.strip().split()
        else:
            dict_goods[li[0]] = i.strip().split()[0]
            dict_goods[li[1]] = i.strip().split()[1]
            li_goods.append(dict_goods)
# print(li_goods)
import copy
shoping_car = copy.deepcopy(li_goods)
for i in shoping_car:
    i['num'] = 0
# print(shoping_car)#第四步其中一项:定义购物车
input_welcome ='''
输入商品序号将商品加入购物车.
输入N去节算购物车.
输入Q退出程序
>>>'''
del_shoping = '''
输入商品序号将商品从购物车中删除.
删除后余额充足便直接结算,余额任然不够可能还要继续删除
输入Q退出程序
>>>
'''
# for i in range(len(li_goods)):
#     print(i+1,li_goods[i]["name"],li_goods[i]["price"])#第三步打印商品列表
money_sum = 0
while 1:
    for i in range(len(li_goods)):
        print(i + 1, li_goods[i]["name"], li_goods[i]["price"])  # 第三步打印商品列表
    use_choice = input(input_welcome)
    if use_choice == 'Q' or use_choice =='q':
        for i in shoping_car:
            i['num'] = 0
        print("您的商品未结算,已经为您的购物车清零")
        break
    elif use_choice == "N" or use_choice =="n":
        for i in shoping_car:
            if i['num']> 0:
                print("您的购物车里有%s,数量%s,单价%s"%(i['name'],i['num'],i['price']))
            money_sum += int(i['price']) * i['num']

        while money_sum > money:
            print("您的账户余额不足,请删减购物车商品.以下是商品列表以及您购物车里的商品清单.输入对应序号删减")
            for i in range(len(li_goods)):
                print(i + 1, li_goods[i]["name"], li_goods[i]["price"])
            print("您需要%d元,但您的余额只有%d元,您需要删掉至少%d元价值的东西."%(money_sum,money,money_sum-money))
            for i in shoping_car:
                if i['num'] > 0:
                    print("您的购物车里有%s,数量%s,单价%s" % (i['name'], i['num'], i['price']))
            del_input = input(del_shoping)
            if del_input == 'Q' or del_input =='q':
                break
            elif del_input.isdecimal():
                if int(del_input) in range(1,5):
                    if shoping_car[int(del_input)-1]["num"] > 0:
                        shoping_car[int(del_input)-1]["num"] -= 1
                        money_sum = 0
                        for i in shoping_car:
                            money_sum += int(i['price']) * i['num']
        else:
            break
        #退出程序之后，依次显示用户购买的商品，数量，单价，以及此次共消费多少钱，账户余额多少
    elif use_choice.isdecimal():
        if int(use_choice) in range(1,5):
            shoping_car[int(use_choice)-1]["num"] += 1
    else:
        print('您的输入错误,请重新输入')

# if money > 0:
for i in shoping_car:
    if i['num']> 0:
        print("您本次购买了%s,数量%s,单价%s."%(i['name'],i['num'],i['price']))
print("共消费了%d元,账户余额还有%s元"%(money_sum,money-money_sum))
# else:
#     print("您本次消费0元,账户余额%s"%(money))
with open("shopping_record.txt",mode='w',encoding=('utf-8')) as f1:
    for i in shoping_car:
        if i['num']> 0:
            f1.write("%s,数量%s,单价%s.\n"%(i['name'],i['num'],i['price']))
    f1.write("您共购买了以上商品,共消费了%d元,账户余额还有%s元" % (money_sum, money - money_sum))



