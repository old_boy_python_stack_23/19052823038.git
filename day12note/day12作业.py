''''''


'''
整理今天笔记，课上代码最少敲3遍。
用列表推导式做下列小题
过滤掉长度小于3的字符串列表，并将剩下的转换成大写字母
'''
# li = ['sadf','sdsdfsdfa','dsfdase','a','b']
# li1 = [i.upper() for i in li if len(i)>3 ]
# print(li1)
'''
求(x,y)其中x是0-5之间的偶数，y是0-5之间的奇数组成的元祖列表
'''
# li = [tuple(i for i in range(5) if i%2 == 0),tuple(j for j in range(5) if j%2 == 1)]
# print(li)
'''
求M中3,6,9组成的列表M = [[1,2,3],[4,5,6],[7,8,9]]
'''
M = [[1,2,3],[4,5,6],[7,8,9]]
li = [i for i in M ]
#搞不清需求,不知道如何着手....
'''
求出50以内能被3整除的数的平方，并放入到一个列表中。
'''
# li1 = [i**2 for i in range(50) if i%3 == 0]
# print(li1)

'''
构建一个列表：['python1期', 'python2期', 'python3期', 'python4期', 'python6期', 'python7期', 'python8期', 'python9期', 'python10期']
'''
# li1 = ['python%d期'%(i) for i in range(1,11)]
# print(li1)
'''li
构建一个列表：[(0, 1), (1, 2), (2, 3), (3, 4), (4, 5), (5, 6)]
'''
# li = [(i,i+1) for i in range(6)]
# print(li)
'''
构建一个列表：[0, 2, 4, 6, 8, 10, 12, 14, 16, 18]
'''
# li = [i for i in range(19) if i%2 == 0]
# print(li)


'''
有一个列表l1 = ['alex', 'WuSir', '老男孩', '太白']将其构造成这种列表['alex0', 'WuSir1', '老男孩2', '太白3']
# '''
# l1 = ['alex', 'WuSir', '老男孩', '太白']
# li2 =[l1[i]+str(i) for i in range(4)]
# print(li2)
'''有以下数据类型：
x = {'name':'alex',
     'Values':[{'timestamp':1517991992.94,'values':100,},
               {'timestamp': 1517992000.94,'values': 200,},
            {'timestamp': 1517992014.94,'values': 300,},
            {'timestamp': 1517992744.94,'values': 350},
            {'timestamp': 1517992800.94,'values': 280}],}
将上面的数据通过列表推导式转换成下面的类型：[[1517991992.94, 100], [1517992000.94, 200], [1517992014.94, 300], [1517992744.94, 350], [1517992800.94, 280]]
'''
# x = {'name':'alex',
#      'Values':[{'timestamp':1517991992.94,'values':100,},
#                {'timestamp': 1517992000.94,'values': 200,},
#             {'timestamp': 1517992014.94,'values': 300,},
#             {'timestamp': 1517992744.94,'values': 350},
#             {'timestamp': 1517992800.94,'values': 280}],}
# li = [[x["Values"][i]['timestamp'],x["Values"][i]['values']] for i in range(5)]
# print(li)
'''
用列表完成笛卡尔积
什么是笛卡尔积？ 笛卡尔积就是一个列表，列表里面的元素是由输入的可迭代类型的元素对构成的元组，因此笛卡尔积列表的长度等于输入变量的长度的乘积。
'''
# li1=[1,2,3]
# li2=[4,5,6]
# li3=[i*j for i in li1 for j in li2]
# print(li3)
'''
​a. 构建一个列表，列表里面是三种不同尺寸的T恤衫，每个尺寸都有两个颜色（列表里面的元素为元组类型)。
colors = ['black', 'white']
sizes = ['S', 'M', 'L']
'''
# colors = ['black', 'white']
# sizes = ['S', 'M', 'L']
# li_T_shirt =[(i,j) for i in colors for j in sizes]
# print(li_T_shirt)


'''
​b. 构建一个列表,列表里面的元素是扑克牌除去大小王以后，所有的牌类（列表里面的元素为元组类型）。
l1 = [('A','spades'),('A','diamonds'), ('A','clubs'), ('A','hearts')......('K','spades'),('K','diamonds'), ('K','clubs'), ('K','hearts') ]
'''
# li1 = ['A',2,3,4,5,6,7,8,9,10,'J','Q','K']
# li2 = ['spades','diamonds','clubs','hearts']
# li3 = [(i,j) for i in li1 for j in li2]
# print(li3)
'''
简述一下yield 与yield from的区别。
yield是函数中中断挂起函数,并给一个返回值.   与yield from的区别是,yield是在后面书写并指定返回值,
加上from是迭代挂起后面的迭代对象,一下一批.
看下面代码，能否对其简化？说说你简化后的优点？
def chain(*iterables):
	for it in iterables:
		for i in it:
			yield i
g = chain('abc',(0,1,2))
print(list(g))  # 将迭代器转化成列表
'''
def chain(*iterables):
	yield (i for it in iterables for i in it)
g = chain('abc',(0,1,2))
print(list(g.__next__()))  # 将迭代器转化成列表
#简化后少了两行代码,没什么优点.
'''
看代码求结果（面试题）：
v = [i % 2 for i in range(10)]
print(v)
v = (i % 2 for i in range(10))
print(v)
for i in range(5):
	print(i)
print(i)
'''
# [0,1,0,1,0,1,0,1,0,1]
# 内存地址,指向内容是个生成器(0,1,0,1,0,1,0,1,0,1)
# 0
# 1
# 2
# 3
# 4
# 4
'''
看代码求结果：（面试题）
def demo():
    for i in range(4):
        yield i
g=demo() # 0 1 2 3
g1=(i for i in g) 内存地址 指向 0 1 2 3
g2=(i for i in g1) None
print(list(g1)) 0 1 2 3
print(list(g2)) 空列表
'''
# [0,1,2,3]
# []



'''看代码求结果：（面试题）
def add(n,i):
    return n+i
def test():
    for i in range(4):
        yield i
g=test()
for n in [1,10]:
    g=(add(n,i) for i in g)
print(list(g))
'''
# def add(n,i):
#     return n+i
# def test():
#     for i in range(4):
#         yield i
# g=test()# 0 1 2 3迭代器出来但不用,
# for n in [1,10]:
#     g=(add(n,i) for i in g) #第一次n为1时,循环一次add和g,
# print(list(g))
# # [20,21,22,23]
'''
第一次g是个内存地址
指向的内容是
yield = 0
yield = 1
yield = 2
yield = 3
def add(n,i):这个函数给的返回值是n+i这个表达式.
    return n+i
for n in [1,10]
    g = (add(n,i) for i in g)先运行=后面的内容,n = 1,i=0  n+i 放入列表中,求值是1,不求值是n+i. 新的g是n+i n+i n+i n+i
    第二次循环,n=10,i=n+i 循环结束后g是n+n+i n+n+i n+n+i n+n+i .
    不调用时,是以这种方式存储的.
    调用时,n的值最后停留在10上,i的值任然是初始的0,1,2,3'''