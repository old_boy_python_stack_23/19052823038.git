from threading import Thread

# def task(name):
#     print(f'{name} is running')
#
# if __name__=='__main__':
#     t = Thread(target=task,args=('mcsaoQ',))
#     t.start()
#
#     print('主线程')

class MyThread(Thread):

    def run(self):
        print(f'{self.name} is running')

if __name__=='__main__':
    t = MyThread()
    t.start()
    print('主线程')
