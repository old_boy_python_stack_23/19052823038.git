# 1.web框架了解,认识并写个大概
# 2.django的下载安装使用
# 内容回顾
# python基础
    # 面向对象
        # 继承
        # 封装
    # 网络
        # OSI七层协议
            # 应用层
            # 表示层
            # 会话层
            # 传输层
            # 网络层
            # 数据链路层
            # 物理层
        # TCP/ip五层
            # 应用层
            # 传输层  tcp
            # 网络层  ip
            # 数据链路层  erp
            # 物理层
    # 并发
# 数据库
# 前端
# socket套接字,位于应用层和传输层中的一个抽象层,一个接口.调用接口实现网络通信.
# C/S   B/S
# web框架浏览器已经做好了B,我们写S.
# 百度的服务器(socket服务端)
    # 1.创建socket对象
    # 2.绑定ip和端口
    # 3.启动监听
    # 6.接收数据
    # 7.处理请求并发回数据
    # 8.断开连接
# 浏览器(socket客户端)
    # 1.创建socket对象
    # 4.连接百度的ip和端口
    # 5.发送数据或请求
    # 6.接收数据
    # 8.断开连接
import socket
# 这里对应socket服务端简写.

# 连接时出现响应无效
# 引出http协议的概念.  在博客里,这里需要补博客的笔记.
#  https://www.cnblogs.com/maple-shaw/articles/9060408.html
# http协议的八个请求方法
# 其中get
#向指定的资源发出“显示”请求。使用GET方法应该只用在读取数据，
# 而不应当被用于产生“副作用”的操作中，例如在Web Application中。
# 原因是GET可能会被网络蜘蛛等随意访问。
# 和post常用.
# 向指定资源提交数据，请求服务器进行处理（例如提交表单或者上传文件）。
# 数据被包含在请求本文中。这个请求可能会创建新的资源或修改现有资源，或二者皆有。
# http默认端口80,https默认端口443




