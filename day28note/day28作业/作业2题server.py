import socket
import hashlib
sk = socket.socket()
sk.bind(('127.0.0.1',8080))
sk.listen()

conn,addr = sk.accept()
print(addr,':连接进来了')
conn.send('请输入用户名'.encode('utf-8'))
use_name = conn.recv(1024)
conn.send('请输入密码'.encode('utf-8'))
use_password = conn.recv(1024)
with open('password',encoding='utf-8') as f:
    for i in f.readlines():
        if use_name.decode('utf-8') == i.strip().split('|')[0].strip() and use_password.decode('utf-8') == i.strip().split('|')[1].strip():
            conn.send('登录成功'.encode('utf-8'))
        else:
            conn.send('用户名或密码不对,登录失败'.encode('utf-8'))
conn.close()
sk.close()
