import socket
sk = socket.socket()
sk.bind(('127.0.0.1',8080))
sk.listen(5)
while 1:
    print('等待连接:')
    conn,addr = sk.accept()
    print(f'用户{addr}进入连接')
    flag = input('选择恢复模式'
                 '1自动回复'
                 '2手动回复').strip()
    while 1:
        s1 = conn.recv(1024)
        if s1.decode('utf-8').upper() == 'Q':
            print(f'用户{addr}退出连接')
            break
        print(f"来自{addr}的消息>>>{s1.decode('utf_8')}")
        pc_send_message = "您好，我现在有事不在，请稍后联系"
        if flag == '1':
            conn.send(pc_send_message.encode("utf-8"))
            print(f'我方自动回复内容:{pc_send_message}')
        elif flag == '2':
            h_send_message = input('>>>')
            conn.send(h_send_message.encode('utf-8'))
    conn.close()
sk.close()

