import socket
import subprocess
#怎么交给操作系统.
#socket是内置模块,负责将pycharm里写好的东西交给Socket.又它交给操作系统.

phone = socket.socket()
#1创建socket对象,可以默认不写

phone.bind(('127.0.0.1',8848))  #本地回环地址
#2:绑定ip地址和端口(办卡)

#3,监听:(开机状态)
phone.listen(5)

#4. 等电话,接受连接
while 1:
    print('start')
    conn,addr = phone.accept()
    print(conn,addr)
    while 1:
        from_client_data = conn.recv(1024)  #至多接受1024字节.
        # print(f'来自客户端{addr}的消息:{from_client_data.decode("utf-8")}')
        obj = subprocess.Popen(from_client_data.decode("gbk"),
                               shell=True,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               )

        # print(obj.stdout.read().decode('gbk'))  # 正确命令
        # print(obj.stderr.read().decode('gbk'))  # 错误命令
        conn.send(obj.stdout.read())
    conn.close()
phone.close()
