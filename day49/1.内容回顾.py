# 内容回顾

# 事件
    # 方式一 行内绑定
        # <a onclick="函数名"()></a>
        # function 函数名(可以有参数){函数体}
    # 匿名函数绑定
        # 某对象.onclick = function 匿名函数(){函数体}
    # 函数名绑定
        # 某对象.onclick = 函数名
        # function 函数名(){函数体}
# 常见事件
    # onclick()点击
    # onmouseover()鼠标悬浮
    # onmouseout() 鼠标离开
    # onscroll() 滚轴滚动
    # onChange() 文本框内容变化
    # onfocus() 获取焦点
    # onblur() 失去焦点
# 应用示例
    # 红绿灯
    # 随机颜色,字体
    # 回顶部
    # 关广告
    # select框联动
# BOM
    # window对象
        # 定时器方法
            # setInterval("fn()",n) 每隔n毫秒就执行fn函数一次
            # setTimeout(fn,n)   n毫秒后执行fn函数一次

            # innerHeight  窗口高度.
            # innerWidth   窗口宽度
    # location对象
        # 属性: href
        # 查看页面的url 连接,href = '新的url',跳转到新的页面

        # 方法:reload()
        # 刷新页面
    # history对象
        # history.back()   go(-1)  返回上一页
        # history.forward()      刷新当前页
        # history.forward()  go(1)  前进