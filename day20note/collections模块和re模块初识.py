# from collections import  namedtuple
# Point = namedtuple('point',['x','y'])#命名元组
# print(type(Point))
# p = Point(1,2)
# print(type(p))
# print(p)
# print(p.x)
# print(p.y)
# import time
# struct_time = time.strptime('2019-7-2','%Y-%m-%d')
# print(struct_time)
# from collections import deque   #比列表多一个appendleft和popleft,并且从开头和末尾增删元素的效率比列表快
# liq = deque([1,2,3,4,5])
# print(liq)
# liq.append(6)
# print(liq)
# liq.appendleft(7)
# print(liq)
# liq.pop()
# print(liq)
# li = liq.popleft()
# print(liq)
# print(li)
# liq.insert(3,'a')
# print(liq)
# from collections import OrderedDict
# #有序字典.  py3.6版本的字典已经是默认创建时顺序保持不变的情况了.
# od = OrderedDict([('a',1),('b',2)])
# print(od['a'])
from collections import defaultdict
#默认值字典
# li = [11,22,33,44,55,77,88,99]
# dic = {}
# for i in li:
#     if i < 66:
#         if 'key1' not in dic:
#             dic['key1'] = []
#         dic['key1'].append(i)
#     else:
#         if 'key2' not in dic:
#             dic['key2'] = []
#         dic['key2'].append(i)
# print(dic)
# dic = defaultdict()#括号内需要一个可回调的内容,比如list,函数等等.
# from collections import Counter
# #计数器 ,后面是个可迭代对象. 返回一个字典
# c = Counter('dskfjaljfdkajslfjlcccsdkjjfkslccafdsfc')
# print(c)
#re模块 正则表达式,从一大堆字符串中,找出你想要的字符串.
#难度在于对你想要得这个字符串进行一个精确的描述,描述的越精确找的越准确
import re
#re.findall()
#单个字符匹配
#\W与\w

# \w数字字母下划线,中文.
# print(re.findall('\w','太白jx 12*() _'))
# \W除了数字字母下划线,中文.
# print(re.findall('\W','太白jx 12*() _'))
#\s与\S

# \s #空格,制表符,换行符等等.
# \S #除空格,制表符,换行符以外的.
# print(re.findall('\s','太白barry*(_ \t \n'))
# print(re.findall('\S','太白barry*(_ \t \n'))
#\d与\D
#d数字,D非数字

# print(re.findall('\d\d','太白123123b1a2r3r4y*(_ \t \n'))
# print(re.findall('\D\D','太白123123b1a2r3r4y*(_ \t \n'))
#\A匹配开头是否是这个,如果是则拿出来,不是则返回空列表.
#\^这个上尖叫号和大A一样的用法.
# print(re.findall('\Ahello','太白12312hello hell o3b1a2r3r4y*(_ \t \n'))
#\Z,$   匹配结尾

# print(re.findall('干净\Z','太白12312hello hell o3b1a2r3r4y*(_ \t \n擦不干净'))
# print(re.findall('干$','太白12312hello hell o3b1a2r3r4y*(_ \t \n擦不干净'))
#\n匹配换行符 \t匹配制表符

# print(re.findall('\n','太白12\n312hello hell o3b1a2r3r4y*(_ \t \n擦不干净'))
# print(re.findall('\t','太白12\n312hello hell o3b1a2r3r4y*(_ \t \n擦不干净'))

#元字符匹配
#. ?  *  +   {m,n}   .*    .*?

#   . 代替任意字符 ,除了换行符以外.
#满足条件则返回,并且光标直接移动到返回内容的后面.  如果匹配不成功,则光标挪动1位.
# print(re.findall('a.b','太白ab,a,ba babbaabaaab12\n312hello hell o3b1a2r3r4y*(_ \t \n擦不干净'))

#?问号左边的字符,可有1个或可没有,其他字符必须有.
# print(re.findall('aa?a?a?b','太白ab,a,ba babbaabaaab12\n312hello hell o3b1a2r3r4y*(_ \t \n擦不干净'))

#*,*号左边的字符,可有多个或没有.其他字符必须有
# print(re.findall('a*b','太白ab,a,ba babbaabaaab12\n312hello hell o3b1a2r3r4y*(_ \t \n擦不干净'))

#+匹配1个或多个左边字符的表达式.
# print(re.findall('a+b','太白ab,a,ba babbaabaaab12\n312hello hell o3b1a2r3r4y*(_ \t \n擦不干净'))

#{m,n}匹配m个至n个左边的字符表达式,m和n都能取到. 可以只有一个m参数.
# print(re.findall('a{2}b','太白ab,a,ba babbaabaaab12\n312hello hell o3b1a2r3r4y*(_ \t \n擦不干净'))

# .*    a.*b是两个换行符之间的从第一个a到最后一个b之间的所有内容.贪婪模式,拿最长的
# print(re.findall('a.*b','太白aboa蛋basbabb aabaaab12\n312h\tello hell o3b1a2r3r4y*(_ \t \n擦不干净'))

# .*?  问号对.*的模式遵循非贪婪模式. 从开头,匹配到能结尾就结尾.
# print(re.findall('a.*?b','太白aboa蛋basbabb aabaaab12\n312h\tello hell o3b1a2r3r4y*(_ \t \n擦不干净'))
#空白取到空白,相当于取len()+1个空字符串.
# print(re.findall('.*?','b'))

# [abc]括号内的字符,出现一个就算.并且只要一个.[0-9][a-z][A-Z][a-ce-g]想用-时,放在最前面或最后面就可以了.
# print(re.findall('a[a-ce-g]b','太白aabboa蛋basbafbb aadteb1a2r3r4y*(_ \t \n擦不干净'))
#^上尖角号在括号里表取反
# print(re.findall('a[a-ce-g]b','太白aabboa蛋basbafbb aadteb1a2r3r4y*(_ \t \n擦不干净'))

# ()分组(abc)de  选出来abcde但只把abc返回
# s = 'alex_sb wusir_sb ritian_sb 太白_nb yuanbao_sb dab_sb'
# print(re.findall('(\w+)_sb',s))

#左边|右边,两边有一边就行.ab|bc  有ab或bc都返回

#ab(c|d)ab后面跟c或d 返回c或d
#ab(?:c|d)  ab后i按跟c或d,返回abc或abd


# ret = re.search('sb|alex','alex sb sb barry 日天')
# #找到第一个符合条件的就返回.返回一个对象,长成下面那样
# print(ret)
# #<_sre.SRE_Match object; span=(0, 4), match='alex'>
# print(ret.group())
# #通过对象.group找到返回的内容.

#match 检查字符串是否以条件开头,开头则又返回,不是则返回None
# ret = re.match('alex','alexsgdsb')
# print(ret)

# split  切割[批量切割符]
# print(re.split('[;,]','sfdf;sdkfjk,kdsjfl,dsfj'))


#print(re.sub('被替换的内容','替换后的内容','被替换的内容,替换后的内容需要替换的对象'))

# obj = re.compile('\d{3}')  #定制了一个规则,3个数字.

# ret = re.finditer('\d','43sdfkj542lkjsdfjkals3342')
# print(list(ret))

s1 = '''
时间就是1995-04-27,2005-04-27
1999-04-27 老男孩教育创始人
老男孩老师 alex 1980-04-27:1980-04-27
2018-12-08
'''
print(re.findall('[1-2]\d{3}-\d{2}-\d{2}',s1))

obj = re.compile('[1-9][0-9]{4,9}')
print(re.findall(obj,'129380,29849385,09485098,493085,09348,958304985,0384953'))
